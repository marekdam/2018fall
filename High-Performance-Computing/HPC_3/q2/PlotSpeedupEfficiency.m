clear;
close all;

outp1 = importfile('outp1.txt', [2,8], [6,12]);
outp2 = importfile('outp2.txt', [2,8], [6,12]);
outp4 = importfile('outp4.txt', [2,8], [6,12]);
outp8 = importfile('outp8.txt', [2,8], [6,12]);
outp16 = importfile('outp16.txt', [2,8], [6,12]);

n = outp1.Wall(1:5);
p = [1 2 4 8 16];
alt1_times = zeros(5,5);
alt2_times = zeros(5,5);

for i = 1:5
    alt1_times(i,1) = outp1.times(i);
    alt1_times(i,2) = outp2.times(i);
    alt1_times(i,3) = outp4.times(i);
    alt1_times(i,4) = outp8.times(i);
    alt1_times(i,5) = outp16.times(i);
    
    alt2_times(i,1) = outp1.times(i+5);
    alt2_times(i,2) = outp2.times(i+5);
    alt2_times(i,3) = outp4.times(i+5);
    alt2_times(i,4) = outp8.times(i+5);
    alt2_times(i,5) = outp16.times(i+5);
end

for i = 1:5
figure(i)
plot(p(2:end), alt1_times(i,1)./alt1_times(i,2:end),':', p(2:end), alt2_times(i,1)./alt2_times(i,2:end));
titlename = "Speedup for problem size n = " + int2str(n(i));
title(titlename)
legend('Dist. Transpose', "Parallel Solves")
xlabel('P - Number of Processors')
ylabel('Speedup')
end

for i = 1:5
figure(i)
plot(p(2:end), alt1_times(i,1)./alt1_times(i,2:end)./p(2:end),':', p(2:end), alt2_times(i,1)./alt2_times(i,2:end)./p(2:end));
titlename = "Efficiency for problem size n = " + int2str(n(i));
title(titlename)
legend('Dist. Transpose', "Parallel Solves")
xlabel('P - Number of Processors')
ylabel('Efficiency')
end

