#ifndef LINEAR_ALGEBRA_H
#define LINEAR_ALGEBRA_H
#include <vector>

struct _TriDiagMat
{
	double *D; //Main diagonal
	double *DL; //Lower diagonal
	double *DU;	//Upper diagonal
	int n;	// Dimension of matrix
};
typedef struct _TriDiagMat TriDiagMat;

TriDiagMat *TriDiagMat_initialize(int n);

void TriDiagMat_destroy(TriDiagMat *A);

void fill_vector(double * src, int n, double value);

//Assuming a tridiagonal matrix given by the main diagonal D,
//the lower diagonal DL and the upper diagonal DU, the LU
//factor calculation can be optimized
void LU_factor_tridiagonal(TriDiagMat *A);

//Assuming a lower triagonal matrix with a main diagonal of ones,
//and a lower diagonal DL the forward substitution algorithm takes
//this optimized form
void fwd_sub_tri(TriDiagMat *A, double *b, double *y);

//Assuming an upper triagonal matrix with a main diagonal of D,
//and an upper diagonal DU the backward substitution algorithm takes
//this optimized form
void bwd_sub_tri(TriDiagMat *A, double *c, double *x);

//A should be in LU form
void tridiagonal_matrix_solve(TriDiagMat *A_LU, double *x, double *b);

//Performs parallel matrix solving with optimizations for multiple
//right hand sides
std::vector<double> partition2_tridiagonal(int rank, int size, TriDiagMat *A_p1, double A_p2, double A_p3,
							   double A_p4, double B_p1, double C_p1, double *b_p1, int num_rhs);

double *partition2_tridiagonal_og(int rank, int size, TriDiagMat *A_p1, double A_p2, double A_p3, double A_p4,
							   double B_p1, double C_p1, double *b_p1, double b_p2);

//Matrix is distributed to each processor by a subset of columns, but contains all rows of the total matrix
void transpose_distributed_matrix(std::vector<double>& A, int rows, int num_owned_columns, int num_procs);

std::vector<double> convert_storage_order(const std::vector<double>& V, int rows, int cols);

void transpose_block_row_major(double *v, int dim);

//Debug functions to check matrix components
void print_tridiagonal_matrix(const TriDiagMat* A);
void print_vector(const double *x, int n);
void print_stdvector(const std::vector<double>& to_print);

#endif