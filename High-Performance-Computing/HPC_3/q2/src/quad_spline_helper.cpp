#include "quad_spline_helper.h"
#include "mpi.h"
#include <cmath>

std::vector<double> linspace(double start, double end, int size)
{
	double delta = (end - start) / (size - 1);
	std::vector<double> result(size);
	for (int i = 0; i < size; i++)
	{
		result[i] = start + i * delta;
	}
	return result;
}

//Populates the U vector for x^2y^2 case
std::vector<double> populate_u_vector_x2y2(const RectDomain &global_domain, int n, int m, int rank, int num_procs)
{
	//In parallel version each processor contains all the rows of "g"
	//for some subset of the columns
	//m and n correspond to the number of y segments and number of x segments respectively
	const int num_owned_columns = (n + 2) / num_procs;
	const int first_col = rank * num_owned_columns;

	//Also a section of g in DeBoor decomposition
	std::vector<double> u_ij(num_owned_columns * (m + 2));

	const double dx = global_domain.delta_x();
	const double dy = global_domain.delta_y();

	for (int i = first_col; i < first_col + num_owned_columns; i++)
	{
		double x = i * dx + global_domain.min_x;
		if (i > 0 && i < n + 1)
		{
			x = x - dx / 2;
		}
		else if (i == n + 1)
		{
			x = x - dx;
		}

		for (int j = 0; j < m + 2; j++)
		{

			double y = j * dy + global_domain.min_y;
			if (j > 0 && j < m + 1)
			{
				y = y - dy / 2;
			}
			else if (j == m + 1)
			{
				y = y - dy;
			}

			u_ij[(i - first_col) * (m + 2) + j] = x * x * y * y;
		}
	}
	return u_ij;
}

std::vector<double> populate_u_vector_sinxexpy(const RectDomain& global_domain, int n, int m, int rank, int num_procs)
{
	//In parallel version each processor contains all the rows of "g"
	//for some subset of the columns
	//m and n correspond to the number of y segments and number of x segments respectively
	const int num_owned_columns = (n + 2) / num_procs;
	const int first_col = rank * num_owned_columns;

	//Also a section of g in DeBoor decomposition
	std::vector<double> u_ij(num_owned_columns * (m + 2));

	const double dx = global_domain.delta_x();
	const double dy = global_domain.delta_y();

	for (int i = first_col; i < first_col + num_owned_columns; i++)
	{
		double x = i * dx + global_domain.min_x;
		if (i > 0 && i < n + 1)
		{
			x = x - dx / 2;
		}
		else if (i == n + 1)
		{
			x = x - dx;
		}

		for (int j = 0; j < m + 2; j++)
		{

			double y = j * dy + global_domain.min_y;
			if (j > 0 && j < m + 1)
			{
				y = y - dy / 2;
			}
			else if (j == m + 1)
			{
				y = y - dy;
			}

			u_ij[(i - first_col) * (m + 2) + j] = sin(x) * exp(y);
		}
	}
	return u_ij;	
}

//Populate the Matrix used to solve quadratic spline problems
void populate_sequential_spline_matrix(TriDiagMat *matrix, int N)
{
	matrix->D[0] = 0.5;
	matrix->DU[0] = 0.5;

	for (int i = 0; i < N - 2; i++)
	{
		matrix->D[i + 1] = 0.75;
		matrix->DU[i + 1] = 1.0 / 8.0;
		matrix->DL[i] = 1.0 / 8.0;
	}

	matrix->D[N - 1] = 0.5;
	matrix->DL[N - 2] = 0.5;
}

//Populate the S or T Matrix used to solve quadratic spline problems, but in the case
//where the matrix is distributed amongst processors
//In fact this is only A_p1, the other values are populated
//by user with call to partition 2
void populate_distributed_spline_matrix(TriDiagMat *matrix, int N, int rank, int num_procs)
{
	int q = N / num_procs;
	assert(matrix->n == q - 1);

	if (rank == 0)
	{
		matrix->D[0] = 0.5;
	}
	else
	{
		matrix->D[0] = 0.75;
	}

	for (int i = 0; i < (q - 2); i++)
	{
		matrix->D[i + 1] = 0.75;
		matrix->DU[i] = 1.0 / 8.0;
		matrix->DL[i] = 1.0 / 8.0;
	}

	if (rank == 0)
	{
		matrix->DU[0] = 0.5;
	}
}

double model_function(double x)
{
	if (x >= 0 && x <= 1)
	{
		return x * x / 2.;
	}
	else if (x >= 1 && x <= 2)
	{
		double x1 = x - 1;
		return 0.5 * (-2. * x1 * x1 + 2. * x1 + 1);
	}
	else if (x >= 2. && x <= 3.)
	{
		return 0.5 * (3. - x) * (3. - x);
	}
	else
	{
		return 0.0;
	}
}

std::vector<double> calc_2D_spline_values( const RectDomain& global_domain, const std::vector<double>& C, const std::vector<std::array<double, 2>>& xy_points,
 int m, int n, int rank, int num_procs)
{
	//To calculate spline values each processor needs to share its boundary values of C with its neighbors
	std::vector<double> cp_minus(m+2);
	std::vector<double> cp_plus(m+2);
	int num_cols = (n+2)/num_procs;
	if (num_procs > 1)
	{
		if (rank == 0)
		{
			MPI_Request plus_request;
			MPI_Isend(&C[(m+2)*(num_cols-1)], m+2, MPI_DOUBLE, rank + 1, rank, MPI_COMM_WORLD, &plus_request);
			MPI_Status recv_status_plus;
			MPI_Recv(&cp_plus[0], m+2, MPI_DOUBLE, rank + 1, rank + 1, MPI_COMM_WORLD, &recv_status_plus);
			MPI_Status plus_status;
			MPI_Wait(&plus_request, &plus_status);
		}
		else if (rank == num_procs - 1)
		{
			MPI_Request minus_request;
			MPI_Isend(&C[0], m+2, MPI_DOUBLE, rank - 1, rank, MPI_COMM_WORLD, &minus_request);
			MPI_Status recv_status_minus;
			MPI_Recv(&cp_minus[0], m+2, MPI_DOUBLE, rank - 1, rank - 1, MPI_COMM_WORLD, &recv_status_minus);
			MPI_Status minus_status;
			MPI_Wait(&minus_request, &minus_status);
		}
		else
		{
			MPI_Request plus_request;
			MPI_Isend(&C[(m+2)*(num_cols-1)], m+2, MPI_DOUBLE, rank + 1, rank, MPI_COMM_WORLD, &plus_request);
			MPI_Status recv_status_plus;
			MPI_Recv(&cp_plus[0], m+2, MPI_DOUBLE, rank + 1, rank + 1, MPI_COMM_WORLD, &recv_status_plus);
			MPI_Request minus_request;
			MPI_Status recv_status_minus;
			MPI_Recv(&cp_minus[0], m+2, MPI_DOUBLE, rank - 1, rank - 1, MPI_COMM_WORLD, &recv_status_minus);
			MPI_Status plus_status;
			MPI_Wait(&plus_request, &plus_status);

			MPI_Isend(&C[0], m+2, MPI_DOUBLE, rank - 1, rank, MPI_COMM_WORLD, &minus_request);
			MPI_Status minus_status;
			MPI_Wait(&minus_request, &minus_status);
		}
	}

	std::vector<double> val(xy_points.size());
	//Now interpolations can be calculated
	for (int idx = 0; idx < xy_points.size(); idx++)
	{
		const double x = xy_points[idx][0];
		const double y = xy_points[idx][1];

		int x_knot = global_domain.get_x_interval(x);
		int y_knot = global_domain.get_y_interval(y);

		int local_idx_x = x_knot%num_cols;
		
		val[idx] = 0.0;
		for(int i = -1; i < 2; i++ )
		{
			double scaled_x_i = (x - global_domain.min_x)/global_domain.delta_x() - (x_knot + i) + 2;
			int i_rela = local_idx_x + i;
			const double* start_j = &C[0];

			if(i_rela == -1)
			{
				start_j = &cp_minus[0];
			}else if(i_rela == num_cols)
			{
				start_j = &cp_plus[0];
			}else{
				start_j = &C[i_rela*(m+2)];
			}

			for(int j = -1; j < 2; j++ )
			{
				int j_rela = y_knot + j;
				if(j_rela >= 0.0 && j_rela < m+2){
					const double scaled_y_i = (y - global_domain.min_y)/global_domain.delta_y() - (y_knot + j) + 2;
					const double model_x = model_function(scaled_x_i);
					const double model_y = model_function(scaled_y_i);
					val[idx] += start_j[j_rela]*model_x*model_y;
				}

			}
		}

	}
	return val;
}