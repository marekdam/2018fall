#ifndef QUAD_SPLINE_HELPER_H
#define QUAD_SPLINE_HELPER_H

#include <array>
#include <cassert>
#include <vector>
#include "linear_algebra.h"

//Represents a 2D rectangular domain
//and is used to quickly calculate knot positions
//in quadratic spline operations
struct RectDomain
{
	double min_x;
	double max_x;
	double min_y;
	double max_y;
	int num_x;
	int num_y;

	double delta_x() const
	{
		return (max_x - min_x)/(num_x - 1);
	}

	double delta_y() const
	{
		return (max_y - min_y)/(num_y - 1);
	}

	//Here interval means the first i th knot that is larger
	//than the test_point
	int get_x_interval(double test_point) const
	{
		assert(test_point >= min_x);
		double distance = test_point - min_x;
		return distance/delta_x() + 1;
	}
	//Similar to above but in y dimension
	int get_y_interval(double test_point) const
	{
		assert(test_point >= min_y);
		double distance = test_point - min_y;
		return distance/delta_y() + 1;
	}
	std::array<double, 2> get_knot(int i, int j) const
	{
		return std::array<double, 2>{min_x + i*delta_x(), min_y + j*delta_y()};
	}
};

std::vector<double> linspace(double start, double end, int size);
//Populates the U vector for x^2y^2 case
std::vector<double> populate_u_vector_x2y2(const RectDomain& global_domain, int n, int m, int rank, int num_procs);
std::vector<double> populate_u_vector_sinxexpy(const RectDomain& global_domain, int n, int m, int rank, int num_procs);

void populate_sequential_spline_matrix(TriDiagMat *matrix, int N);
void populate_distributed_spline_matrix(TriDiagMat *matrix, int N, int rank, int num_procs);

double model_function(double x);

std::vector<double> calc_2D_spline_values( const RectDomain& global_domain, const std::vector<double>& C, const std::vector<std::array<double, 2>>& xy_points,
 int m, int n, int rank, int num_procs);

#endif