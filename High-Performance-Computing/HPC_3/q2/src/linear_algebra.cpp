#include "linear_algebra.h"
#include <cassert>
#include <cstring>
#include "mpi.h"

TriDiagMat *TriDiagMat_initialize(int n)
{
	assert(n > 0);

	double *D = (double *)malloc(sizeof(double) * n);
	double *DL = (double *)malloc(sizeof(double) * (n - 1));
	double *DU = (double *)malloc(sizeof(double) * (n - 1));

	if (D && DL && DU)
	{
		TriDiagMat *A = (TriDiagMat *)malloc(sizeof(TriDiagMat));
		if (A)
		{
			A->n = n;
			A->D = D;
			A->DL = DL;
			A->DU = DU;
			return A;
		}
	}

	free(D);
	free(DL);
	free(DU);
	return NULL;
}

void TriDiagMat_destroy(TriDiagMat *A)
{
	free(A->D);
	free(A->DL);
	free(A->DU);
	free(A);
}

void fill_vector(double *src, int n, double value)
{
	for (int i = 0; i < n; i++)
	{
		src[i] = value;
	}
}

//Assuming a tridiagonal matrix given by the main diagonal D,
//the lower diagonal DL and the upper diagonal DU, the LU
//factor calculation can be optimized
void LU_factor_tridiagonal(TriDiagMat *A)
{
	double *DL = A->DL;
	double *D = A->D;
	double *DU = A->DU;
	int size = A->n;
	for (int k = 0; k < size - 1; k++)
	{
		DL[k] = DL[k] / D[k];
		D[k + 1] = D[k + 1] - DL[k] * DU[k];
	}
}

//Assuming a lower triagonal matrix with a main diagonal of ones,
//and a lower diagonal DL the forward substitution algorithm takes
//this optimized form
void fwd_sub_tri(TriDiagMat *A, double *b, double *y)
{
	y[0] = b[0];
	double *DL = A->DL;
	int size = A->n;
	for (int i = 1; i < size; i++)
	{
		y[i] = b[i] - DL[i - 1] * y[i - 1];
	}
}

//Assuming an upper triagonal matrix with a main diagonal of D,
//and an upper diagonal DU the backward substitution algorithm takes
//this optimized form
void bwd_sub_tri(TriDiagMat *A, double *c, double *x)
{
	double *D = A->D;
	double *DU = A->DU;
	int size = A->n;
	x[size - 1] = c[size - 1] / D[size - 1];

	for (int i = size - 2; i >= 0; i--)
	{
		x[i] = c[i] - DU[i] * x[i + 1];
		x[i] = x[i] / D[i];
	}
}

//A should be in LU form
void tridiagonal_matrix_solve(TriDiagMat *A_LU, double *x, double *b)
{
	fwd_sub_tri(A_LU, b, x);
	bwd_sub_tri(A_LU, x, x);
}

//Assume Beta is 1 so many "matrix" blocks are actually single values or vectors
//Optimized for multiple righthand sides
std::vector<double> partition2_tridiagonal(int rank, int size, TriDiagMat *A_p1, double A_p2, double A_p3, double A_p4,
							   double B_p1, double C_p1, double *b_p1, int num_rhs)
{
	int q = A_p1->n + 1;

	//msg contains A_p2_prime, C_p1 prime, and all of the bp1 primes
	const int size_pre_red_msg = num_rhs + 2;
	double *pre_reduced_msg = (double *)malloc(sizeof(double) * size_pre_red_msg);

	
	//Step 1
	LU_factor_tridiagonal(A_p1);

	//Step 2
	double *rhs = (double *)malloc(sizeof(double) * (q - 1));
	double *A_p2_prime = (double *)malloc(sizeof(double) * (q - 1));
	fill_vector(rhs, q - 1, 0.0);
	rhs[q - 2] = A_p2;
	tridiagonal_matrix_solve(A_p1, A_p2_prime, rhs);
	pre_reduced_msg[0] = A_p2_prime[0];

	//Step 3
	double *C_p1_prime = NULL;
	if (rank != 0)
	{
		C_p1_prime = (double *)malloc(sizeof(double) * (q - 1));
		fill_vector(rhs, q - 1, 0.0);
		rhs[0] = C_p1;
		tridiagonal_matrix_solve(A_p1, C_p1_prime, rhs);
		pre_reduced_msg[1] = C_p1_prime[0];
	}
	free(rhs);

	double *b_p1_prime = (double *)malloc(sizeof(double) * num_rhs*(q - 1));
	//Step 4 optimized for multiple rhs vectors
	for (int it = 0; it < num_rhs; it++)
	{
		tridiagonal_matrix_solve(A_p1, &b_p1_prime[it * (q-1)], &b_p1[it * q]);
		pre_reduced_msg[it + 2] = b_p1_prime[it * (q-1)];
	}

	//Need to send the first components of the 3 primed values to the previous processor which they will require
	MPI_Request pplus_request;
	if (rank != 0)
	{
		MPI_Isend(&pre_reduced_msg[0], size_pre_red_msg, MPI_DOUBLE, rank - 1, rank, MPI_COMM_WORLD, &pplus_request);
	}

	//Step 7
	//C_p2 is 0 for tridiagonal matrices if we assume q > 2
	//Confirming sent C_p1_prime before using
	double C_p2_prime = 0.0;
	if (rank != 0)
	{
		C_p2_prime = -A_p3 * C_p1_prime[q - 2];
	}

	double *recv_reduced_msg = (double *)malloc(sizeof(double) * size_pre_red_msg);

	if (rank != size - 1)
	{
		MPI_Status recv_status;
		MPI_Recv(&recv_reduced_msg[0], size_pre_red_msg, MPI_DOUBLE, rank + 1, rank + 1, MPI_COMM_WORLD, &recv_status);
	}

	//Step 5
	//The second term is a matrix multiplication
	//optimized to a single multiplication since A_p3 is all zero
	//except for the last element
	double A_p4_prime = A_p4 - A_p3 * A_p2_prime[q - 2];
	if (rank != size - 1)
	{
		double C_plus11_prime = recv_reduced_msg[1];
		A_p4_prime = A_p4_prime - B_p1 * C_plus11_prime;
	}

	//Step 6
	//For a tridiagonal matrix B_p2 is 0 if q > 2B
	double B_p2_prime = 0.0;
	if (rank != size - 1)
	{
		double A_plus12_prime = recv_reduced_msg[0];
		B_p2_prime = -B_p1 * A_plus12_prime;
	}

	//Make sure to finish sending by now
	if (rank != 0)
	{
		MPI_Status send_status;
		MPI_Wait(&pplus_request, &send_status);
	}

	double *b_p2_prime = (double *)malloc(sizeof(double) * num_rhs);
	//Step 4 optimized for multiple rhs vectors
	for (int it = 0; it < num_rhs; it++)
	{
		double b_p2 = b_p1[it*q + (q-1)];
		//Step 8
		b_p2_prime[it] = b_p2 - A_p3 * b_p1_prime[it*(q-1) + q-2];
		if (rank != size - 1)
		{
			double b_plus11_prime = recv_reduced_msg[it+2];
			b_p2_prime[it] = b_p2_prime[it] - B_p1 * b_plus11_prime;
		}
	}

	//This array contains both A, B, C, b_p2 entries interleaved
	//to minimize communication time.
	int size_one_proc = 3 + num_rhs;
	double *reduced_problem = (double *)malloc(sizeof(double) *(size * size_one_proc));
	{
		double *own_reduced_problem = (double *)malloc(sizeof(double) * (size_one_proc));
		own_reduced_problem[0] = A_p4_prime;
		own_reduced_problem[1] = B_p2_prime;
		own_reduced_problem[2] = C_p2_prime;
		for(int i = 0; i < num_rhs; i++){
			own_reduced_problem[i + 3] = b_p2_prime[i];
		}
		MPI_Allgather(&own_reduced_problem[0], size_one_proc, MPI_DOUBLE, reduced_problem, size_one_proc, MPI_DOUBLE, MPI_COMM_WORLD);
		free(own_reduced_problem);
	}

	//Form TriDiagonal problem
	TriDiagMat *reduced_tridiagonal = TriDiagMat_initialize(size);
	reduced_tridiagonal->D[0] = reduced_problem[0];
	for (int i = 1; i < size; i++)
	{
		reduced_tridiagonal->D[i] = reduced_problem[size_one_proc * i];
		reduced_tridiagonal->DU[i - 1] = reduced_problem[size_one_proc * (i - 1) + 1];
		reduced_tridiagonal->DL[i - 1] = reduced_problem[size_one_proc * i + 2];
	}

	double *reduced_prob_vector = (double *)malloc(sizeof(double) * size * num_rhs);
	//Unpack interleaved problem vector
	for(int iter = 0; iter < num_rhs; iter++)
	{
		for (int i = 0; i < size; i++)
		{
			reduced_prob_vector[iter*size + i] = reduced_problem[i*size_one_proc + iter + 3];
		}
	}

	//Factor reduced problem
	LU_factor_tridiagonal(reduced_tridiagonal);

	double *reduced_solution = (double *)malloc(sizeof(double) * size);
	std::vector<double> x_p(q * num_rhs);
	for (int it = 0; it < num_rhs; it++)
	{
		//Step 9 - Solve Reduced system
		//First substep is to share the reduced system and b vector
		//among all processors. Each processor
		//needs to share 4 doubles. The First and Last only need to share 3
		//but we will not optimize that.

		//Solve reduced diagonal problem
		tridiagonal_matrix_solve(reduced_tridiagonal, reduced_solution, &reduced_prob_vector[it*size]);

		//Step 10 - Calculate solution vector
		for (int i = 0; i < (q - 1); i++)
		{
			x_p[it * q + i] = b_p1_prime[it*(q-1) + i] - A_p2_prime[i] * reduced_solution[rank];
		}

		if (rank != 0)
		{
			for (int i = 0; i < (q - 1); i++)
			{
				x_p[it * q + i] = x_p[it * q + i] - C_p1_prime[i] * reduced_solution[rank - 1];
			}
		}

		x_p[it * q  + q - 1] = reduced_solution[rank];
	}

	free(A_p2_prime);
	free(C_p1_prime);
	free(b_p1_prime);
	free(b_p2_prime);
	TriDiagMat_destroy(reduced_tridiagonal);
	free(reduced_solution);
	free(reduced_prob_vector);
	free(pre_reduced_msg);
	free(recv_reduced_msg);
	free(reduced_problem);
	return x_p;
}

void transpose_distributed_matrix(std::vector<double> &A, int rows, int num_owned_columns, int num_procs)
{

	int size_submatrix = rows * num_owned_columns;
	std::vector<double> reordered = convert_storage_order(A, rows, num_owned_columns);

	for (int i = 0; i < num_procs; i++)
	{
		transpose_block_row_major(&reordered[i * num_owned_columns * num_owned_columns], num_owned_columns);
	}
	A = reordered;

	std::vector<double> d_i_prime(size_submatrix);

	MPI_Alltoall(&A[0], size_submatrix / num_procs, MPI_DOUBLE, &d_i_prime[0], size_submatrix / num_procs, MPI_DOUBLE, MPI_COMM_WORLD);

	std::vector<double> reordered_recv = convert_storage_order(d_i_prime, num_owned_columns, rows);
	A = reordered_recv;
}

std::vector<double> convert_storage_order(const std::vector<double> &V, int rows, int cols)
{
	assert(V.size() == rows * cols);
	size_t sz = V.size();
	std::vector<double> reordered(sz);

	for (int idx = 0; idx < sz; idx++)
	{
		int i = idx / cols;
		int j = idx % cols;
		reordered[idx] = V[rows * j + i];
	}
	return reordered;
}

void transpose_block_row_major(double *v, int dim)
{
	for (int j = 0; j < dim; j++)
	{
		for (int i = j + 1; i < dim; i++)
		{
			std::swap(v[i * dim + j], v[j * dim + i]);
		}
	}
}

//Debug function to check matrix components
void print_tridiagonal_matrix(const TriDiagMat *A)
{
	int size = A->n;
	double *DL = A->DL;
	double *D = A->D;
	double *DU = A->DU;
	for (int i = 0; i < size; i++)
	{
		for (int j = 0; j < size; j++)
		{
			if (i == j + 1)
			{
				printf("%.4g\t", DL[j]);
			}
			else if (i == j)
			{
				printf("%.4g\t", D[i]);
			}
			else if (i + 1 == j)
				printf("%.4g\t", DU[i]);
			else
			{
				printf("--\t");
			}
		}
		printf("\n");
	}
}

void print_vector(const double *x, int n)
{
	for (int i = 0; i < n; i++)
	{
		printf("%.4g ", x[i]);
	}
	printf("\n");
}

void print_stdvector(const std::vector<double> &to_print)
{
	print_vector(&to_print[0], to_print.size());
}