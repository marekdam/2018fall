#include "mpi.h"
#include <cassert>
#include <cmath>
#include <vector>
#include <ctime>
#include <iostream>

#include "linear_algebra.h"
#include "quad_spline_helper.h"

struct error_timing_alt1
{
	double max_error = 0.0;
	double first_linear_solves_c = 0.0;
	double first_linear_solves_w = 0.0;
	double dist_matrix_transpose_c = 0.0;
	double dist_matrix_transpose_w = 0.0;
	double second_linear_solves_c = 0.0;
	double second_linear_solves_w = 0.0;
	double total_clock_time = 0.0;
	double total_wall_time = 0.0;
};

struct error_timing_alt2
{
	double max_error = 0.0;
	double first_linear_solves_c = 0.0;
	double first_linear_solves_w = 0.0;
	double partition2_c = 0.0;
	double partition2_w = 0.0;
	double total_clock_time = 0.0;
	double total_wall_time = 0.0;
};

error_timing_alt1 max_error_2d_spline_alt1(int n, int m, int function_switch, int rank, int num_procs, int num_test_points);
error_timing_alt2 max_error_2d_spline_alt2(int n, int m, int function_switch, int rank, int num_procs, int num_test_points);

//Returns the clock time in seconds
double get_clock_time(struct timespec *tm)
{
	int id = clock_gettime(CLOCK_PROCESS_CPUTIME_ID, tm);
	double ts = (double)(tm->tv_sec);
	double tns = (double)(tm->tv_nsec) * 1e-9;
	return ts + tns;
}

int main(int argc, char **argv)
{
	int rank, num_procs;
	MPI_Init(&argc, &argv);
	MPI_Comm_rank(MPI_COMM_WORLD, &rank);
	MPI_Comm_size(MPI_COMM_WORLD, &num_procs);
	std::cout.precision(5);
	std::cout << std::scientific;
	//test_transpose();
	//test_fast_partition();
	int n_array[5] = {30, 62, 126, 254, 510};
	//int n = 6;
	if (rank == 0)
	{
		printf("\nMaximum errors for data from sin(x)exp(y) in domain (0, 4*pi)(0, pi)\n\n");
		printf("Errors for alternative 1\n");
	}

	std::vector<error_timing_alt1> alt1_timings;
	for (int i = 0; i < 5; i++)
	{
		int n = n_array[i];
		int m = n;
		error_timing_alt1 knots_results = max_error_2d_spline_alt1(n, m, 1, rank, num_procs, m + 1);
		error_timing_alt1 p38_results = max_error_2d_spline_alt1(n, m, 1, rank, num_procs, 38);
		alt1_timings.push_back(p38_results);
		if (rank == 0)
		{
			std::cout << "n=" << n << "\tMax Error on Knots = \t\t" << knots_results.max_error<< std::endl;
			std::cout << "n=" << n << "\tMax Error on 38 points = \t" << p38_results.max_error<< std::endl;
		}
	}
	if (rank == 0)
	{
		printf("Errors for alternative 2\n");
	}

	std::vector<error_timing_alt2> alt2_timings;
	for (int i = 0; i < 5; i++)
	{
		int n = n_array[i];
		int m = n;
		error_timing_alt2 knots_results = max_error_2d_spline_alt2(n, m, 1, rank, num_procs, m + 1);
		error_timing_alt2 p38_results = max_error_2d_spline_alt2(n, m, 1, rank, num_procs, 38);
		alt2_timings.push_back(p38_results);
		if (rank == 0)
		{
			std::cout << "n=" << n << "\tMax Error on Knots = \t\t" << knots_results.max_error<< std::endl;
			std::cout << "n=" << n << "\tMax Error on 38 points = \t" << p38_results.max_error<< std::endl;
		}
	}

	if (rank == 0)
	{
		printf("\nTimings\n\n");
		printf("Clock times alternative 1\n");
		printf("n\t1st Lin. Sol.\tDist. Transpose\t2nd Lin. Sol.\tTotal Time\n");

		for (int i = 0; i < alt1_timings.size(); i++)
		{
			std::cout << n_array[i] <<
			 "\t" <<alt1_timings[i].first_linear_solves_c <<
			 "\t" <<alt1_timings[i].dist_matrix_transpose_c <<
			 "\t" <<alt1_timings[i].second_linear_solves_c <<
			 "\t" << alt1_timings[i].total_clock_time << std::endl;
		}
	}
	if (rank == 0)
	{
		printf("Clock times alternative 2\n");
		printf("n\t1st Lin. Sol.\tDist. Lin. Sol.\tTotal Time\n");

		for (int i = 0; i < alt2_timings.size(); i++)
		{
			std::cout << n_array[i] <<
			 "\t" <<alt2_timings[i].first_linear_solves_c <<
			 "\t" <<alt2_timings[i].partition2_c <<
			 "\t" << alt2_timings[i].total_clock_time << std::endl;
		}
	}

	if (rank == 0)
	{
		printf("\n\n");
		printf("Wall times alternative 1\n");
		printf("n\t1st Lin. Sol.\tDist. Transpose\t2nd Lin. Sol.\tTotal Time\n");

		for (int i = 0; i < alt1_timings.size(); i++)
		{
			std::cout << n_array[i] <<
			 "\t" <<alt1_timings[i].first_linear_solves_w <<
			 "\t" <<alt1_timings[i].dist_matrix_transpose_w <<
			 "\t" <<alt1_timings[i].second_linear_solves_w <<
			 "\t" << alt1_timings[i].total_wall_time << std::endl;
		}
	}
	if (rank == 0)
	{
		printf("Wall times alternative 2\n");
		printf("n\t1st Lin. Sol.\tDist. Lin. Sol.\tTotal Time\n");

		for (int i = 0; i < alt2_timings.size(); i++)
		{
			std::cout << n_array[i] <<
			 "\t" <<alt2_timings[i].first_linear_solves_w <<
			 "\t" <<alt2_timings[i].partition2_w <<
			 "\t" << alt2_timings[i].total_wall_time << std::endl;
		}
	}

	MPI_Finalize();
	return EXIT_SUCCESS;
}

error_timing_alt1 max_error_2d_spline_alt1(int n, int m, int function_switch, int rank, int num_procs, int num_test_points)
{
	assert(function_switch == 0 || function_switch == 1);
	struct timespec tm;
	int T_size = m + 2; //Dimension of matrix that will need to be solved
	int S_size = n + 2;
	error_timing_alt1 result;
	RectDomain global_domain;
	if (function_switch == 0)
	{
		global_domain.min_x = 0.0;
		global_domain.max_x = 1.0;
		global_domain.min_y = 0.0;
		global_domain.max_y = 1.0;
		global_domain.num_x = n + 1;
		global_domain.num_y = m + 1;
	}
	else
	{
		global_domain.min_x = 0.0;
		global_domain.max_x = 4 * M_PI;
		global_domain.min_y = 0.0;
		global_domain.max_y = M_PI;
		global_domain.num_x = n + 1;
		global_domain.num_y = m + 1;
	}
	double start_c = get_clock_time(&tm);
	double start_w = MPI_Wtime();

	const int num_owned_columns = (n + 2) / num_procs;
	const int first_col = rank * num_owned_columns;

	TriDiagMat *T_tri = TriDiagMat_initialize(T_size);

	populate_sequential_spline_matrix(T_tri, T_size);

	std::vector<double> u_ij;
	if (function_switch == 0)
	{
		u_ij = populate_u_vector_x2y2(global_domain, n, m, rank, num_procs);
	}
	else
	{
		u_ij = populate_u_vector_sinxexpy(global_domain, n, m, rank, num_procs);
	}

	//Need to solve matrix equation for each column
	LU_factor_tridiagonal(T_tri);

	int size_d = num_owned_columns * (m + 2);
	std::vector<double> d_i(size_d);

	for (int i = 0; i < num_owned_columns; i++)
	{
		tridiagonal_matrix_solve(T_tri, &d_i[i * (m + 2)], &u_ij[i * (m + 2)]);
	}

	result.first_linear_solves_c = get_clock_time(&tm) - start_c;
	result.first_linear_solves_w = MPI_Wtime() - start_w;

	double tmp_c = get_clock_time(&tm);
	double tmp_w = MPI_Wtime();

	transpose_distributed_matrix(d_i, m + 2, num_owned_columns, num_procs);

	result.dist_matrix_transpose_c = get_clock_time(&tm) - tmp_c;
	result.dist_matrix_transpose_w = MPI_Wtime() - tmp_w;

	tmp_c = get_clock_time(&tm);
	tmp_w = MPI_Wtime();

	int size_c = num_owned_columns * (n + 2);
	std::vector<double> c_i_prime(size_c);
	for (int i = 0; i < num_owned_columns; i++)
	{
		tridiagonal_matrix_solve(T_tri, &c_i_prime[i * (n + 2)], &d_i[i * (n + 2)]);
	}

	result.second_linear_solves_c = get_clock_time(&tm) - tmp_c;
	result.second_linear_solves_w = MPI_Wtime() - tmp_w;

	result.total_clock_time = get_clock_time(&tm) - start_c;
	result.total_wall_time = MPI_Wtime() - start_w;

	transpose_distributed_matrix(c_i_prime, m + 2, num_owned_columns, num_procs);

	std::vector<double> tp_x = linspace(global_domain.min_x, global_domain.max_x, num_test_points);
	std::vector<double> tp_y = linspace(global_domain.min_y, global_domain.max_y, num_test_points);

	std::vector<std::array<double, 2>> test_xy;
	test_xy.reserve(num_owned_columns * num_test_points);

	for (int i = 0; i < num_test_points; i++)
	{
		int interval_x = global_domain.get_x_interval(tp_x[i]);
		if (interval_x >= rank * num_owned_columns && interval_x < (rank + 1) * num_owned_columns)
		{
			for (int j = 0; j < num_test_points; j++)
			{
				test_xy.push_back({tp_x[i], tp_y[j]});
			}
		}
		else if (rank == num_procs - 1 && interval_x == (rank + 1) * num_owned_columns)
		{
			for (int j = 0; j < num_test_points; j++)
			{
				test_xy.push_back({tp_x[i], tp_y[j]});
			}
		}
	}

	std::vector<double> vals = calc_2D_spline_values(global_domain, c_i_prime, test_xy, m, n, rank, num_procs);

	double max_error = -1.0;
	if (function_switch == 0)
	{
		for (int i = 0; i < test_xy.size(); i++)
		{
			const double x = test_xy[i][0];
			const double y = test_xy[i][1];
			const double exact = x * x * y * y;
			const double error = fabs(exact - vals[i]);
			if (error > max_error)
			{
				max_error = error;
			}
		}
	}
	else
	{
		for (int i = 0; i < test_xy.size(); i++)
		{
			const double x = test_xy[i][0];
			const double y = test_xy[i][1];
			const double exact = sin(x) * exp(y);
			const double error = fabs(exact - vals[i]);
			if (error > max_error)
			{
				max_error = error;
			}
		}
	}

	double tmp = -1.0;
	MPI_Reduce(&max_error, &tmp, 1, MPI_DOUBLE, MPI_MAX, 0, MPI_COMM_WORLD);
	result.max_error = tmp;
	MPI_Reduce(&result.dist_matrix_transpose_c, &tmp, 1, MPI_DOUBLE, MPI_MAX, 0, MPI_COMM_WORLD);
	result.dist_matrix_transpose_c = tmp;
	MPI_Reduce(&result.dist_matrix_transpose_w, &tmp, 1, MPI_DOUBLE, MPI_MAX, 0, MPI_COMM_WORLD);
	result.dist_matrix_transpose_w = tmp;
	MPI_Reduce(&result.first_linear_solves_c, &tmp, 1, MPI_DOUBLE, MPI_MAX, 0, MPI_COMM_WORLD);
	result.first_linear_solves_c = tmp;
	MPI_Reduce(&result.first_linear_solves_w, &tmp, 1, MPI_DOUBLE, MPI_MAX, 0, MPI_COMM_WORLD);
	result.first_linear_solves_w = tmp;
	MPI_Reduce(&result.second_linear_solves_c, &tmp, 1, MPI_DOUBLE, MPI_MAX, 0, MPI_COMM_WORLD);
	result.second_linear_solves_c = tmp;
	MPI_Reduce(&result.second_linear_solves_w, &tmp, 1, MPI_DOUBLE, MPI_MAX, 0, MPI_COMM_WORLD);
	result.second_linear_solves_w = tmp;
	MPI_Reduce(&result.total_clock_time, &tmp, 1, MPI_DOUBLE, MPI_MAX, 0, MPI_COMM_WORLD);
	result.total_clock_time = tmp;
	MPI_Reduce(&result.total_wall_time, &tmp, 1, MPI_DOUBLE, MPI_MAX, 0, MPI_COMM_WORLD);
	result.total_wall_time = tmp;

	TriDiagMat_destroy(T_tri);
	return result;
}

error_timing_alt2 max_error_2d_spline_alt2(int n, int m, int function_switch, int rank, int num_procs, int num_test_points)
{
	assert(function_switch == 0 || function_switch == 1);
	error_timing_alt2 result;
	int T_size = m + 2; //Dimension of matrix that will need to be solved
	int S_size = n + 2;
	struct timespec tm;
	RectDomain global_domain;
	if (function_switch == 0)
	{
		global_domain.min_x = 0.0;
		global_domain.max_x = 1.0;
		global_domain.min_y = 0.0;
		global_domain.max_y = 1.0;
		global_domain.num_x = n + 1;
		global_domain.num_y = m + 1;
	}
	else
	{
		global_domain.min_x = 0.0;
		global_domain.max_x = 4 * M_PI;
		global_domain.min_y = 0.0;
		global_domain.max_y = M_PI;
		global_domain.num_x = n + 1;
		global_domain.num_y = m + 1;
	}

	double start_c = get_clock_time(&tm);
	double start_w = MPI_Wtime();

	const int num_owned_columns = S_size / num_procs;

	TriDiagMat *T_tri = TriDiagMat_initialize(T_size);

	populate_sequential_spline_matrix(T_tri, T_size);

	std::vector<double> u_ij;
	if (function_switch == 0)
	{
		u_ij = populate_u_vector_x2y2(global_domain, n, m, rank, num_procs);
	}
	else
	{
		u_ij = populate_u_vector_sinxexpy(global_domain, n, m, rank, num_procs);
	}

	//Need to solve matrix equation for each column
	LU_factor_tridiagonal(T_tri);

	int size_d = num_owned_columns * T_size;
	std::vector<double> d_i(size_d);

	for (int i = 0; i < num_owned_columns; i++)
	{
		tridiagonal_matrix_solve(T_tri, &d_i[i * T_size], &u_ij[i * T_size]);
	}
	TriDiagMat_destroy(T_tri);

	result.first_linear_solves_c = get_clock_time(&tm) - start_c;
	result.first_linear_solves_w = MPI_Wtime() - start_w;

	int q = S_size / num_procs;
	assert(q - 1 > 0);

	std::vector<double> d_i_prime = convert_storage_order(d_i, T_size, num_owned_columns);

	TriDiagMat *S_tri = TriDiagMat_initialize(q - 1);
	populate_distributed_spline_matrix(S_tri, S_size, rank, num_procs);
	//Setup missing parts of matrix
	double Ap2 = 1.0 / 8.0;
	double Ap3 = 1.0 / 8.0;
	double Ap4 = 0.75;
	double Bp1 = 1.0 / 8.0;
	double Cp1 = 1.0 / 8.0;

	if (rank == 0 && q == 2)
	{
		Ap2 = 0.5;
	}

	if (rank == num_procs - 1)
	{
		Ap3 = 0.5;
		Ap4 = 0.5;
	}

	double part2_c = get_clock_time(&tm);
	double part2_w = MPI_Wtime();

	//Here the "p" denotes the subsection of the matrix/vector that belongs to processor p
	std::vector<double> c_i_prime = partition2_tridiagonal(rank, num_procs, S_tri, Ap2, Ap3, Ap4, Bp1, Cp1, &d_i_prime[0], T_size);

	result.partition2_c = get_clock_time(&tm) - part2_c;
	result.partition2_w = MPI_Wtime() - part2_w;

	result.total_clock_time = get_clock_time(&tm) - start_c;
	result.total_wall_time = MPI_Wtime() - start_w;

	std::vector<double> c_i = convert_storage_order(c_i_prime, num_owned_columns, T_size);

	TriDiagMat_destroy(S_tri);

	std::vector<double> tp_x = linspace(global_domain.min_x, global_domain.max_x, num_test_points);
	std::vector<double> tp_y = linspace(global_domain.min_y, global_domain.max_y, num_test_points);

	std::vector<std::array<double, 2>> test_xy;
	test_xy.reserve(num_owned_columns * num_test_points);

	for (int i = 0; i < num_test_points; i++)
	{
		int interval_x = global_domain.get_x_interval(tp_x[i]);
		if (interval_x >= rank * num_owned_columns && interval_x < (rank + 1) * num_owned_columns)
		{
			for (int j = 0; j < num_test_points; j++)
			{
				test_xy.push_back({tp_x[i], tp_y[j]});
			}
		}
		else if (rank == num_procs - 1 && interval_x == (rank + 1) * num_owned_columns)
		{
			for (int j = 0; j < num_test_points; j++)
			{
				test_xy.push_back({tp_x[i], tp_y[j]});
			}
		}
	}

	std::vector<double> vals = calc_2D_spline_values(global_domain, c_i, test_xy, m, n, rank, num_procs);

	double max_error = -1.0;
	if (function_switch == 0)
	{
		for (int i = 0; i < test_xy.size(); i++)
		{
			const double x = test_xy[i][0];
			const double y = test_xy[i][1];
			const double exact = x * x * y * y;
			const double error = fabs(exact - vals[i]);
			if (error > max_error)
			{
				max_error = error;
			}
		}
	}
	else
	{
		for (int i = 0; i < test_xy.size(); i++)
		{
			const double x = test_xy[i][0];
			const double y = test_xy[i][1];
			const double exact = sin(x) * exp(y);
			const double error = fabs(exact - vals[i]);
			if (error > max_error)
			{
				max_error = error;
			}
		}
	}

	double tmp = -1.0;
	MPI_Reduce(&max_error, &tmp, 1, MPI_DOUBLE, MPI_MAX, 0, MPI_COMM_WORLD);
	result.max_error = tmp;

	MPI_Reduce(&result.first_linear_solves_c, &tmp, 1, MPI_DOUBLE, MPI_MAX, 0, MPI_COMM_WORLD);
	result.first_linear_solves_c = tmp;
	MPI_Reduce(&result.first_linear_solves_w, &tmp, 1, MPI_DOUBLE, MPI_MAX, 0, MPI_COMM_WORLD);
	result.first_linear_solves_w = tmp;

	MPI_Reduce(&result.partition2_c, &tmp, 1, MPI_DOUBLE, MPI_MAX, 0, MPI_COMM_WORLD);
	result.partition2_c = tmp;
	MPI_Reduce(&result.partition2_w, &tmp, 1, MPI_DOUBLE, MPI_MAX, 0, MPI_COMM_WORLD);
	result.partition2_w = tmp;

	MPI_Reduce(&result.total_clock_time, &tmp, 1, MPI_DOUBLE, MPI_MAX, 0, MPI_COMM_WORLD);
	result.total_clock_time = tmp;
	MPI_Reduce(&result.total_wall_time, &tmp, 1, MPI_DOUBLE, MPI_MAX, 0, MPI_COMM_WORLD);
	result.total_wall_time = tmp;
	return result;
}