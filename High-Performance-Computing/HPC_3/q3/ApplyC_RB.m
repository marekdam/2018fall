function [x] = ApplyC_RB(blue, dimx,dimy)
%Applies matrix multiplication on blue to red C13 in notes
drx = dimx/2;
dry = dimy/2;

x = zeros(drx*dry,1);
%Bottom left
x(1) = blue(1);

%Left boundary
for j = 2:dry
    x(j) = 6*blue(j);
end

%Middle Points
for i = 2:drx
    x((i-1)*dry + 1) = blue((i-1)*dry + 1 - dry) + blue((i-1)*dry + 1);
    for j = 2:dry
        x((i-1)*dry + j) = 6*(blue((i-1)*dry + j - dry) + blue((i-1)*dry + j));
    end
end
end