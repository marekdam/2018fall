function [x] = PermuteToNormal(red,green,blue,yellow, dimx,dimy)
%From a colored permuted system reconstruct the natural ordered vector
x = zeros(dimx*dimy, 1);

%Convert red green
idx = 1;
for i = 1:2:dimx
   for j = 1:2:dimy
       x((i-1)*dimy + j) = red(idx);
       x((i-1)*dimy + j + 1) = green(idx);
       idx= idx+1;
   end
end

%Convert blue yellow
idx = 1;
for i = 2:2:dimx
   for j = 1:2:dimy
       x((i-1)*dimy + j) = blue(idx);
       x((i-1)*dimy + j + 1) = yellow(idx);
       idx= idx+1;
   end
end
end