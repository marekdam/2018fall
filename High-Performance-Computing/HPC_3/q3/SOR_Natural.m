function [x, k] = SOR_Natural(b, n, m, epsi, nit, w)
%Solves the Quadratic Spline problem for a given problem in b vector
%SOR Algorithm
x= zeros((n+2)*(m+2),1);

%Guess is zero so we don't need to do this
% q0 = Apply_LDW(w,x,n,m);
% tmp = Apply_UW1WD(w,x,n,m);
% q1 = b-tmp;
q0 = x;
q1 = b;
r0 = q1-q0;

errnorm0 = norm(r0);
errnorm = errnorm0;
for k = 1:nit
   if errnorm <= epsi*errnorm0 
      k = k - 1;
      break;
   end
   x = Solve_LDW(w,q1,n,m);
   q0 = q1;
   tmp = Apply_UW1WD(w,x,n,m);
   q1 = b-tmp;
   r = q1-q0;
   errnorm = norm(r);
end
end

