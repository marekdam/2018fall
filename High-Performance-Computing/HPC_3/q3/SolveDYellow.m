function [x] = SolveDYellow(yellow,dimx,dimy)
%Solves Diagonal matrix problem for red vector
drx = dimx/2;
dry = dimy/2;

x = zeros(drx*dry,1);

%Middle Points
for i = 1:drx-1
    for j = 1:dry-1
        x((i-1)*dry + j) = yellow((i-1)*dry + j)/36;
    end
    x((i-1)*dry + dry) = yellow((i-1)*dry + dry)/6;
end


%Right boundary
for j = 1:dry-1
    x((drx-1)*dry + j) = yellow((drx-1)*dry + j)/6;
end
%Top right
x((drx-1)*dry + dry) = yellow((drx-1)*dry + dry);

end