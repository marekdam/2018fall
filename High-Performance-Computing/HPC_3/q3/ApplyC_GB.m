function [x] = ApplyC_GB(blue,dimx,dimy)
%Applies matrix multiplication on blue to green C23 in notes
drx = dimx/2;
dry = dimy/2;

x = zeros(drx*dry,1);

%Left boundary
for j = 1:dry-1
    x(j) = blue(j) + blue(j+1);
end
x(dry) = blue(dry);

%Middle Points
for i = 2:drx
    for j = 1:dry-1
        x((i-1)*dry + j) = blue((i-1)*dry + j-dry) +blue((i-1)*dry + j -dry + 1) ...
            + blue((i-1)*dry + j) +blue((i-1)*dry + j + 1);
    end
    x((i-1)*dry + dry) = blue((i-1)*dry + dry-dry) ...
            + blue((i-1)*dry + dry);
end
end