function [q] = Apply_UW1WD(w, x, n, m)
%Applies (U + ((w-1)/w)D) to vector x modelling a quadratic spline
%matrix with n and m
dimx = n+2;
dimy = m+2;
q = zeros(dimx*dimy,1);
w_r = (w-1)/w;

%On x min boundary
%bottom corner
q(1) = x(2)+x(1+dimy)+x(2+dimy)...
            +x(1)*w_r;

%Middle of boundary
for j=2:dimy-1
	 q(j) = x(j+dimy-1)+6*x(j+dimy)+...
            x(j+dimy + 1)+...
            x(j + 1)+6*x(j)*w_r;
end
%top corner
q(dimy) = x(2*dimy-1)+x(2*dimy)+...
            x(dimy)*w_r;
        
%Middle points
for i = 2:dimx-1   
    %Ymin boundary
    idx = (i-1)*dimy + 1;
    idx_plus = idx + dimy;
    q(idx) =  x(idx_plus + 1)+x(idx_plus)+6*x(idx+1)+6*x(idx)*w_r;    
    
    for j=2:dimy-1
        idx = (i-1)*dimy + j;
        idx_plus = idx + dimy;
        q(idx) = x(idx_plus-1)+6*x(idx_plus)+...
            x(idx_plus + 1)+...
            6*x(idx + 1)+36*x(idx)*w_r;
    end
    
    %Ymax boundary
    idx = (i-1)*dimy + dimy;
    idx_plus = idx + dimy;    
    q(idx) = x(idx_plus - 1)+x(idx_plus)+6*x(idx)*w_r;      
end        
        
%On x max boundary
%bottom corner
idx = (dimx-1)*dimy + 1;
q(idx) = x(idx+1)+...
            +x(idx)*w_r;

%Middle of boundary
for j=2:dimy-1
	idx = (dimx-1)*dimy + j;
	q(idx) = x(idx + 1)+6*x(idx)*w_r;
end

%right corner
q(dimx*dimy) = x(dimx*dimy)*w_r;
        
end

