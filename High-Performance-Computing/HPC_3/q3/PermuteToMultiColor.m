function [red, green, blue, yellow] = PermuteToMultiColor(x, dimx, dimy)
%Performs a permutation of vector from normal ordering 
%to multicolor
%Assumes dimx and dimy are always even

red = zeros(dimy*dimx/4,1);
green = zeros(dimy*dimx/4,1);
blue = zeros(dimy*dimx/4,1);
yellow = zeros(dimy*dimx/4,1);

%Get Red Green
idx = 1;
for i = 1:2:dimx
    for j= 1:2:dimy
        red(idx) = x((i-1)*dimy + j);
        green(idx) = x((i-1)*dimy + j + 1);
        idx = idx+1;
    end
end

%Get Blue Yellow
idx = 1;
for i = 2:2:dimx
    for j= 1:2:dimy
        blue(idx) = x((i-1)*dimy + j);
        yellow(idx) = x((i-1)*dimy + j + 1);
        idx = idx+1;
    end
end
end
