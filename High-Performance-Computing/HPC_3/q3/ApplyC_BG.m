function [x] = ApplyC_BG(green,dimx,dimy)
%Applies matrix multiplication on green to blue C32 in notes
drx = dimx/2;
dry = dimy/2;

x = zeros(drx*dry,1);

%Middle Points
for i = 1:drx-1
    x((i-1)*dry + 1) = green((i-1)*dry + 1) + green((i-1)*dry + 1+dry);
    for j = 2:dry
        x((i-1)*dry + j) = green((i-1)*dry + j-1) +green((i-1)*dry + j)...
            +green((i-1)*dry + j + dry - 1) + green((i-1)*dry + j + dry);
    end
end

%Bottom right
x((drx-1)*dry + 1) = green((drx-1)*dry + 1);
%Right boundary
for j = 2:dry
    x((drx-1)*dry + j) = green((drx-1)*dry + j - 1) + green((drx-1)*dry + j);
end

end