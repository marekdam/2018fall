function [x] = ApplyC_YR(red,dimx,dimy)
%Applies matrix multiplication on red to yellow C41 in notes
drx = dimx/2;
dry = dimy/2;

x = zeros(drx*dry,1);

%Middle Points
for i = 1:drx-1
    for j = 1:dry-1
        x((i-1)*dry + j) = red((i-1)*dry + j) + red((i-1)*dry + j + 1)...
            +red((i-1)*dry + j + dry) + red((i-1)*dry + j +dry + 1);
    end
    x((i-1)*dry + dry) = red((i-1)*dry + dry) + red((i-1)*dry + dry + dry);
end

%Right boundary
for j = 1:dry-1
    x((drx-1)*dry + j) = red((drx-1)*dry + j)+red((drx-1)*dry + j + 1);
end
%Top right
x((drx-1)*dry + dry) = red((drx-1)*dry + dry);

end