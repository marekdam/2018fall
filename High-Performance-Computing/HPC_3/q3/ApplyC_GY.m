function [x] = ApplyC_GY(yellow,dimx,dimy)
%Applies matrix multiplication on yellow to green C24 in notes
drx = dimx/2;
dry = dimy/2;

x = zeros(drx*dry,1);

%Left boundary
for j = 1:dry-1
    x(j) = 6*yellow(j);
end
x(dry) = yellow(dry);

%Middle Points
for i = 2:drx
    for j = 1:dry-1
        x((i-1)*dry + j) =  6*(yellow((i-1)*dry + j - dry) + yellow((i-1)*dry + j));
    end
    x((i-1)*dry + dry) = yellow((i-1)*dry + dry - dry) + yellow((i-1)*dry + dry);
end
end