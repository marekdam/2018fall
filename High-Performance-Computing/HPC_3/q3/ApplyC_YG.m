function [x] = ApplyC_YG(green,dimx,dimy)
%Applies matrix multiplication on green to yellow C42 in notes
drx = dimx/2;
dry = dimy/2;

x = zeros(drx*dry,1);

%Middle Points
for i = 1:drx-1
    for j = 1:dry-1
        x((i-1)*dry + j) = 6*(green((i-1)*dry + j) + green((i-1)*dry + j + dry));
    end
    x((i-1)*dry + dry) = green((i-1)*dry + dry) + green((i-1)*dry + dry + dry);
end

%Right boundary
for j = 1:dry-1
    x((drx-1)*dry + j) = 6*(green((drx-1)*dry + j));
end
%Top right
x((drx-1)*dry + dry) = green((drx-1)*dry + dry);

end