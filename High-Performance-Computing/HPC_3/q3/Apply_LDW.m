function [q] = Apply_LDW(w, x, n, m)
%Applies (L+D/w) to vector x modelling a quadratic spline
%matrix with n and m
dimx = n+2;
dimy = m+2;
q = zeros(dimx*dimy,1);

%On x min boundary
q(1) = x(1)/w;
for j=2:dimy
	q(j) = x(j-1)+6*x(j)/w;               
end
q(dimy) = x(dimy-1)+x(dimy)/w;

%Middle points
for i = 2:dimx-1
    %Ymin boundary
    idx = (i-1)*dimy + 1;
    idx_minus = idx - dimy;
    
    q(idx) = x(idx_minus)+...
            x(idx_minus + 1)+...
            6*x(idx)/w;    
    
    for j=2:dimy-1
        idx = (i-1)*dimy + j;
        idx_minus = idx - dimy;
        q(idx) = x(idx_minus-1)+6*x(idx_minus)+...
            x(idx_minus + 1)+...
            6*x(idx - 1)+36*x(idx)/w;
    end
    
    %Ymax boundary
    idx = (i-1)*dimy + dimy;
    idx_minus = idx - dimy;
    
    q(idx) = x(idx_minus - 1)+x(idx_minus)+6*x(idx-1)+6*x(idx)/w;         
end

%On x max boundary
%bottom corner
idx = (dimx-1)*dimy + 1;
q(idx) = x(idx - dimy)+x(idx - dimy + 1)+...
            +x(idx)/w;

%Middle of boundary
for j=2:dimy-1
	idx = (dimx-1)*dimy + j;
	idx_minus = idx - dimy;
	 q(idx) = x(idx_minus-1)+6*x(idx_minus)+...
            x(idx_minus + 1)+...
            x(idx - 1)+6*x(idx)/w;
end
%top corner
q(dimx*dimy) = x(dimx*(dimy-1)-1)+x(dimx*(dimy-1))+...
            x(dimx*dimy - 1)+x(dimx*dimy)/w;

end

