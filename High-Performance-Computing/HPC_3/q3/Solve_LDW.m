function [x] = Solve_LDW(w, q, n, m)
%Solves (L+D/w) to vector x modelling a quadratic spline
%matrix with n and m
dimx = n+2;
dimy = m+2;
x = zeros(dimx*dimy,1);

%First dimy values
x(1) = w*q(1);
for j=2:dimy
	x(j) = (w/6)*(q(j)-x(j-1));               
end
x(dimy) = w*(q(dimy)-x(j-1));

%Middle points
for i = 2:dimx-1
    %Ymin boundary
    idx = (i-1)*dimy + 1;
    idx_minus = idx - dimy;
    
    x(idx) = (w/6)*(-x(idx_minus)+...
            -x(idx_minus + 1)+...
            q(idx));    
    
    for j=2:dimy-1
        idx = (i-1)*dimy + j;
        idx_minus = idx - dimy;
        x(idx) = (w/36)*(-x(idx_minus-1)-6*x(idx_minus)+...
            -x(idx_minus + 1)+...
            -6*x(idx - 1)+q(idx));
    end
    
    %Ymax boundary
    idx = (i-1)*dimy + dimy;
    idx_minus = idx - dimy;
    
    x(idx) = (w/6)*(-x(idx_minus - 1)-x(idx_minus)-6*x(idx-1)+q(idx));         
end

%On x max boundary
%bottom corner
idx = (dimx-1)*dimy + 1;
x(idx) = w*(-x(idx - dimy)-x(idx - dimy + 1)+...
            +q(idx));

%Middle of boundary
for j=2:dimy-1
	idx = (dimx-1)*dimy + j;
	idx_minus = idx - dimy;
	 x(idx) = (w/6)*(-x(idx_minus-1)-6*x(idx_minus)+...
            -x(idx_minus + 1)+...
            -x(idx - 1)+q(idx));
end
%top corner
x(dimx*dimy) = w*(-x(dimx*(dimy-1)-1)-x(dimx*(dimy-1))+...
            -x(dimx*dimy - 1)+q(dimx*dimy));
        
end

