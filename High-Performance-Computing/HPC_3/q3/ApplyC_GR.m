function [x] = ApplyC_GR(red,dimx,dimy)
%Applies matrix multiplication on red to green C21 in notes
drx = dimx/2;
dry = dimy/2;

x = zeros(drx*dry,1);

%Left boundary
for j = 1:dry-1
    x(j) = red(j) + red(j+1);
end
x(dry) = red(dry);

%Middle Points
for i = 2:drx
    for j = 1:dry-1
        x((i-1)*dry + j) = 6*(red((i-1)*dry + j) +red((i-1)*dry + j + 1)) ;
    end
    x((i-1)*dry + dry) = 6*red((i-1)*dry + dry);
end
end