function [x] = ApplyDRed(red, dimx,dimy)
%Solves Diagonal matrix problem for red vector
drx = dimx/2;
dry = dimy/2;

x = zeros(drx*dry,1);
%Bottom left
x(1) = red(1);

%Left boundary
for j = 2:dry
    x(j) = red(j)*6;
end

%Middle Points
for i = 2:drx
    x((i-1)*dry + 1) = red((i-1)*dry + 1)*6;
    for j = 2:dry
        x((i-1)*dry + j) = red((i-1)*dry + j)*36;
    end
end
end