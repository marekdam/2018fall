function [x] = ApplyDGreen(green,dimx,dimy)
%Solves Diagonal matrix problem for red vector
drx = dimx/2;
dry = dimy/2;

x = zeros(drx*dry,1);

%Left boundary
for j = 1:dry-1
    x(j) = green(j)*6;
end

%Top right
x(dry) = green(dry);

%Middle Points
for i = 2:drx
    for j = 1:dry-1
        x((i-1)*dry + j) = green((i-1)*dry + j)*36;
    end
    x((i-1)*dry + dry) = green((i-1)*dry + dry)*6;
end
end
