function [x] = ApplyC_BY(yellow,dimx,dimy)
%Applies matrix multiplication on yelloe to blue C34 in notes
drx = dimx/2;
dry = dimy/2;

x = zeros(drx*dry,1);

%Middle Points
for i = 1:drx-1
    x((i-1)*dry + 1) = 6*yellow((i-1)*dry + 1);
    for j = 2:dry
        x((i-1)*dry + j) = 6*(yellow((i-1)*dry + j-1) + yellow((i-1)*dry + j));
    end
end

%Bottom right
x((drx-1)*dry + 1) = yellow((drx-1)*dry + 1);
%Right boundary
for j = 2:dry
    x((drx-1)*dry + j) = yellow((drx-1)*dry + j-1) + yellow((drx-1)*dry + j);
end

end

