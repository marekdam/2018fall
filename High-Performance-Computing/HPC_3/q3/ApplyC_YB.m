function [x] = ApplyC_YB(blue,dimx,dimy)
%Applies matrix multiplication on blue to yellow C43 in notes
drx = dimx/2;
dry = dimy/2;

x = zeros(drx*dry,1);

%Middle Points
for i = 1:drx-1
    for j = 1:dry-1
        x((i-1)*dry + j) = 6*(blue((i-1)*dry + j) + blue((i-1)*dry + j + 1));
    end
    x((i-1)*dry + dry) = 6*blue((i-1)*dry + dry);
end

%Right boundary
for j = 1:dry-1
    x((drx-1)*dry + j) = blue((drx-1)*dry + j)+blue((drx-1)*dry + j + 1);
end
%Top right
x((drx-1)*dry + dry) = blue((drx-1)*dry + dry);

end