function [x] = ApplyC_RG(green, dimx,dimy)
%Applies matrix multiplication on green to red C12 in notes
drx = dimx/2;
dry = dimy/2;

x = zeros(drx*dry,1);
%Bottom left
x(1) = green(1);

%Left boundary
for j = 2:dry
    x(j) = green(j-1)+green(j);
end

%Middle Points
for i = 2:drx
    x((i-1)*dry + 1) = green((i-1)*dry + 1)*6;
    for j = 2:dry
        x((i-1)*dry + j) = green((i-1)*dry + j -1)*6 + green((i-1)*dry + j)*6;
    end
end
end