function [x] = ApplyDBlue(blue,dimx,dimy)
%Solves Diagonal matrix problem for red vector
drx = dimx/2;
dry = dimy/2;

x = zeros(drx*dry,1);

%Middle Points
for i = 1:drx-1
    x((i-1)*dry + 1) = blue((i-1)*dry + 1)*6;
    for j = 2:dry
        x((i-1)*dry + j) = blue((i-1)*dry + j)*36;
    end
end

%Bottom right
x((drx-1)*dry + 1) = blue((drx-1)*dry + 1);
%Right boundary
for j = 2:dry
    x((drx-1)*dry + j) = blue((drx-1)*dry + j)*6;
end

end

