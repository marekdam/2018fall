function [x, k] = SOR_MultiColor(b, n, m, epsi, nit, w)

x= zeros((n+2)*(m+2),1);
dimx = n+2;
dimy = m+2;

[x_r, x_g, x_b, x_y] = PermuteToMultiColor(x,dimx,dimy);
[b_r, b_g, b_b, b_y] = PermuteToMultiColor(b,dimx,dimy);

q0_r = x_r;
q0_g = x_g;
q0_b = x_b;
q0_y = x_y;

q1_r = b_r;
q1_g = b_g;
q1_b = b_b;
q1_y = b_y;

r0 = [q1_r; q1_g; q1_b; q1_y]-[q0_r; q0_g; q0_b; q0_y;];

errnorm0 = norm(r0);
errnorm = errnorm0;

for k = 1:nit
    
   if errnorm <= epsi*errnorm0 
      k = k - 1;
      break;
   end
   
	%% Red updates
	q0_r = q1_r;
	q1_r = b_r + ((1-w)/w)*ApplyDRed(x_r,dimx,dimy)-ApplyC_RG(x_g,dimx,dimy)...
       -ApplyC_RB(x_b,dimx,dimy)-ApplyC_RY(x_y,dimx,dimy);
   	x_r = SolveDRed(w*q1_r,dimx,dimy);
    
	%% Green updates
    q0_g = q1_g;
	q1_g = b_g + ((1-w)/w)*ApplyDGreen(x_g,dimx,dimy)-ApplyC_GR(x_r,dimx,dimy)...
       -ApplyC_GB(x_b,dimx,dimy)-ApplyC_GY(x_y,dimx,dimy);
	x_g = SolveDGreen(w*q1_g,dimx,dimy);

	%% Blue updates
	q0_b = q1_b;
	q1_b = b_b + ((1-w)/w)*ApplyDBlue(x_b,dimx,dimy)-ApplyC_BR(x_r,dimx,dimy)...
       -ApplyC_BG(x_g,dimx,dimy)-ApplyC_BY(x_y,dimx,dimy);
	x_b = SolveDBlue(w*q1_b,dimx,dimy);

    %% Yellow updates
	q0_y = q1_y;
	q1_y = b_y + ((1-w)/w)*ApplyDYellow(x_y,dimx,dimy)-ApplyC_YR(x_r,dimx,dimy)...
       -ApplyC_YG(x_g,dimx,dimy)-ApplyC_YB(x_b,dimx,dimy);
	x_y = SolveDYellow(w*q1_y,dimx,dimy);    

	r =  [q1_r; q1_g; q1_b; q1_y]-[q0_r; q0_g; q0_b; q0_y;];
	errnorm = norm(r);
end

x = PermuteToNormal(x_r, x_g, x_b, x_y,dimx,dimy);

end

