clear
close all

n = [8 16 32 64 128 256];
m = n;

ax = 0;
bx = 4*pi;
ay = 0;
by = pi;

epsi = 1e-9;
max_it = 100;
w = [0.8 0.9 1.0 1.1 1.2];

max_error_knots = zeros(1,length(n)*length(w));
max_error_linspace = zeros(1,length(n)*length(w));
nit_nat = zeros(1,length(n)*length(w));
nit_multicolor = zeros(1,length(n)*length(w));

for t = 1:length(n)*length(w)
n_idx = ceil(t/length(w));
w_idx = mod(t-1, length(w))+1;
hx = (bx-ax)/(n(n_idx));
hy = (by-ay)/(m(n_idx));

fun = @(x,y) sin(x).*exp(y);
[knots_x, knots_y, u_xy] = CalcRHS_SplineVector(fun, n(n_idx), m(n_idx), ax, ay, bx, by);
[x_natural, nit_nat(t)] = SOR_Natural(u_xy, n(n_idx), m(n_idx), epsi, max_it, w(w_idx));
[x_multicolor, nit_multicolor(t)] = SOR_MultiColor(u_xy, n(n_idx), m(n_idx), epsi, max_it, w(w_idx));

[tp_x, tp_y] = meshgrid(knots_x, knots_y);

q = CalcSplineAt(tp_x, tp_y, knots_x, knots_y, x_natural, ax, ay, hx, hy);
error = abs(fun(tp_x, tp_y)-q);
max_error_knots(t) = max(error(:));

x = linspace(ax, bx, 38);
y = linspace(ay, by, 38);
[tp_x, tp_y] = meshgrid(x, y);
q = CalcSplineAt(tp_x, tp_y, knots_x, knots_y, x_natural, ax, ay, hx, hy);
error = abs(fun(tp_x, tp_y)-q);
max_error_linspace(t) = max(error(:));

end

error_knots_beta = max_error_knots;
error_linspace_beta = max_error_linspace;

order_knots = zeros(1,length(n)-1);
order_linspace = order_knots;
for i = 1:length(n)-1
    order_knots(i) = log10(error_knots_beta(i)/error_knots_beta(i+1))/...
        log10(n(i+1)/n(i));
	
    order_linspace(i) = log10(error_linspace_beta(i)/error_linspace_beta(i+1))/...
        log10(n(i+1)/n(i));
end

fprintf('n=m\t| w | nit(nat.) | nit(4c) | max err knots | max err 38\n')
for t = 1:length(n)*length(w)
    n_idx = ceil(t/length(w));
    w_idx = mod(t-1, length(w))+1;
    n_ = n(n_idx);
    w_ = w(w_idx);
    fprintf('%d\t %.1f \t  %d\t  %d\t     %.3e \t     %.3e\n', n_, w_,nit_nat(t), ...
        nit_multicolor(t),  error_knots_beta(t), error_linspace_beta(t));
end

figure(1)
plot(w, nit_nat(end-length(w) + 1:end),':', w, nit_multicolor(end-length(w) + 1:end));
title('Number of Iterations for Natural and Multicolor Ordering')
legend('Natural Ordering', 'Four Color Ordering')
xlabel('\omega')
ylabel('Number Iterations')

