function [x] = ApplyC_RY(yellow, dimx,dimy)
%Applies matrix multiplication on yellow to red C14 in notes
drx = dimx/2;
dry = dimy/2;

x = zeros(drx*dry,1);
%Bottom left
x(1) = yellow(1);

%Left boundary
for j = 2:dry
    x(j) = yellow(j-1) + yellow(j);
end

%Middle Points
for i = 2:drx
    x((i-1)*dry + 1) = yellow((i-1)*dry + 1 - dry) + yellow((i-1)*dry + 1);
    for j = 2:dry
        x((i-1)*dry + j) = yellow((i-1)*dry + j - dry)...
            +yellow((i-1)*dry + j - dry - 1)...
            +yellow((i-1)*dry + j-1)+ yellow((i-1)*dry + j);
    end
end
end