clear
close all

n = [8 16 32 64 128 256];%[30, 62, 126, 254, 510];
m = n;

ax = 0.0;
bx = 1;
ay = 0.0;
by = 1;
max_error_knots = zeros(1,length(n));
max_error_linspace = zeros(1,length(n));

for t = 1:length(n)
hx = (bx-ax)/(n(t));
hy = (by-ay)/(m(t));

fun = @(x,y) x.^2.*y.^2;
[knots_x, knots_y, C] = CreateQuadraticSpline(fun, n(t), m(t), ax, ay, bx, by);
[tp_x, tp_y] = meshgrid(knots_x, knots_y);

q = CalcSplineAt(tp_x, tp_y, knots_x, knots_y, C, ax, ay, hx, hy);
error = abs(fun(tp_x, tp_y)-q);
max_error_knots(t) = max(error(:));

x = linspace(ax, bx, 38);
y = linspace(ay, by, 38);
[tp_x, tp_y] = meshgrid(x, y);

q = CalcSplineAt(tp_x, tp_y, knots_x, knots_y, C, ax, ay, hx, hy);
error = abs(fun(tp_x, tp_y)-q);
max_error_linspace(t) = max(error(:));

end

error_knots_alpha = max_error_knots;
error_linspace_alpha = max_error_linspace;

ax = 0;
bx = 4*pi;
ay = 0;
by = pi;
max_error_knots = zeros(1,length(n));
max_error_linspace = zeros(1,length(n));

for t = 1:length(n)
hx = (bx-ax)/(n(t));
hy = (by-ay)/(m(t));

fun = @(x,y) sin(x).*exp(y);

[knots_x, knots_y, C] = CreateQuadraticSpline(fun, n(t), m(t), ax, ay, bx, by);

[tp_x, tp_y] = meshgrid(knots_x, knots_y);

q = CalcSplineAt(tp_x, tp_y, knots_x, knots_y, C, ax, ay, hx, hy);
error = abs(fun(tp_x, tp_y)-q);
max_error_knots(t) = max(error(:));

x = linspace(ax, bx, 38);
y = linspace(ay, by, 38);
[tp_x, tp_y] = meshgrid(x, y);
q = CalcSplineAt(tp_x, tp_y, knots_x, knots_y, C, ax, ay, hx, hy);
error = abs(fun(tp_x, tp_y)-q);
max_error_linspace(t) = max(error(:));

end

error_knots_beta = max_error_knots;
error_linspace_beta = max_error_linspace;

order_knots = zeros(1,length(n)-1);
order_linspace = order_knots;
for i = 1:length(n)-1
    order_knots(i) = log10(error_knots_beta(i)/error_knots_beta(i+1))/...
        log10(n(i+1)/n(i));
	
    order_linspace(i) = log10(error_linspace_beta(i)/error_linspace_beta(i+1))/...
        log10(n(i+1)/n(i));
end

fprintf('Max Error-Alpha Case\n')
fprintf('Knots \t\t38 Points\n')
fprintf('%.3e \t%.3e\n', [error_knots_alpha; error_linspace_alpha]);
fprintf("\n\n");
fprintf('Max Error-Beta Case\n')
fprintf('Knots \t\t38 Points\n')
fprintf('%.3e \t%.3e\n', [error_knots_beta; error_linspace_beta]);
fprintf("\n\n");
fprintf('Convergence Order\n')
fprintf('Knots\t\t38 Points\n');
fprintf('%.3e \t%.3e\n', [order_knots; order_linspace]);
