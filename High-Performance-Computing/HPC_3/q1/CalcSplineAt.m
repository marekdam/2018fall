function [q] = CalcSplineAt(x, y, knots_x, knots_y, C, ax, ay, hx, hy)
%Given the solution vector C, calculates the spline
%value at x,y
m = length(knots_y) - 1;
q = zeros(size(x));

for i = 1:size(x, 2)
   for j = 1:size(x,1) 
   
   max_x = find(knots_x >= x(i,j),1);
   max_y = find(knots_y >= y(i,j),1); 
   for i_phi = max(max_x - 1,1) : max_x + 1
       for j_phi = max(max_y - 1,1) : max_y + 1
            idx = (m+2)*(i_phi-1) + j_phi;
            q(i,j) = q(i,j) + C(idx)*ModelFunction((x(i,j)-ax)/hx - i_phi + 3)*ModelFunction((y(i,j)-ay)/hy - j_phi + 3);
       end
   end
   end 
end
end

