function [A] = BuildQuadraticSplineMatrix(n, m)
%Builds sparse quadratic spline matrix from tensor product
mOnes = ones(1,m+2);
nOnes = ones(1,n+2);
T = 6*diag(mOnes, 0) + diag(mOnes(1:m+1), -1) + diag(mOnes(1:m+1), 1);
T(1,1) = 1;
T(m+2,m+2) = 1;

S = 6*diag(nOnes, 0) + diag(nOnes(1:n+1), -1) + diag(nOnes(1:n+1), 1);
S(1,1) = 1;
S(n+2,n+2) = 1;

A = kron(sparse(S),sparse(T));
end

