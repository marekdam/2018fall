function [knots_x, knots_y, C] = CreateQuadraticSpline(fun, n, m, ax, ay, bx, by)
%Formulates the Quadratic Spline problem

hx = (bx-ax)/(n);
hy = (by-ay)/(m);
knots_x = (ax:hx:bx)';
knots_y = (ay:hy:by)';
tau_x = (ax + hx/2 :hx: bx - hx/2)'; %Midpoints x
tau_y = (ay + hy/2 :hy: by - hy/2)'; %Midpoints y

%Problem Vector models functions f = x^2
u_x = [ax; tau_x; bx;];
u_y = [ay; tau_y; by;];

u_xy = zeros((n+2)*(m+2), 1);
for i=1:(n+2)
   for j = 1:(m+2)
       u_xy((m+2)*(i-1) + j) = fun(u_x(i),u_y(j));
   end
end

%Scale the values
for i=1:(n+2)
   for j = 1:(m+2)
       scale_factor = 64;
       if(i == 1 || i == n+2) 
           scale_factor = scale_factor / 4;
       end
       	if(j == 1 || j == n+2) 
           scale_factor = scale_factor / 4;
       end
       
       u_xy((m+2)*(i-1) + j) = scale_factor*u_xy((m+2)*(i-1) + j);
   end
end

A= BuildQuadraticSplineMatrix(n, m);
C = A\u_xy;
end

