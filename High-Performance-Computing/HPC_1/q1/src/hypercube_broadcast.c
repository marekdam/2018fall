#include "mpi.h"
#include <stdio.h>
#include <stdlib.h>

#define NPIN 3

int main(int argc, char** argv)
{
  int rank, size;
  MPI_Init(&argc, &argv);
  MPI_Comm_rank(MPI_COMM_WORLD, &rank);
  MPI_Comm_size(MPI_COMM_WORLD, &size);

  float local_value = -1.0;
  if(rank == NPIN)
    {
      local_value = (float)rank +1.0;
    }

  float* process_values = NULL;
      
  if(rank==0)
    {
      process_values = (float*)malloc(sizeof(float)*size);
    }
      
  MPI_Gather(&local_value, 1, MPI_FLOAT, process_values, 1, MPI_FLOAT, 0, MPI_COMM_WORLD);  

  if(rank == 0)
    {
      printf("Starting point was:\n");
      for(int i =0; i < size; i++)
	{
	  printf("Process %d holds value: %f\n", i, process_values[i]);   
	}
      printf("\n");
      free(process_values);
    }
      
  //Broadcast algorithm
  int n = 1;
  while(n<size)
    {
      if((rank&n) != (NPIN&n))
	{
	  int comm_with = rank^n;//flip function
	  MPI_Status recv_status;
	  MPI_Recv(&local_value, 1, MPI_FLOAT, comm_with, comm_with, MPI_COMM_WORLD, &recv_status);	
	}
      else
	{
	  int comm_with = rank^n;//flip function
	  MPI_Request send_request;
	  MPI_Send(&local_value, 1, MPI_FLOAT, comm_with, rank, MPI_COMM_WORLD);	  
	}
      
      float* process_values = NULL;
      
      if(rank==0)
	{
	  process_values = (float*)malloc(sizeof(float)*size);
	}
      
      MPI_Gather(&local_value, 1, MPI_FLOAT, process_values, 1, MPI_FLOAT, 0, MPI_COMM_WORLD);  

      if(rank == 0)
	{
	  for(int i =0; i < size; i++)
	    {
	      printf("Process %d holds value: %f\n", i, process_values[i]);   
	    }
	  printf("Iteration %d finished \n\n", n);
	  free(process_values);
	}
      n = n<<1;
    }
        
  MPI_Finalize();
  return 0;
}
