#include "mpi.h"
#include <stdio.h>
#include <stdlib.h>
#include <time.h>

#define NUM_TIMING_TRIALS 50

//Averages the values of a vector
double average(double* vector, int size)
{
  double average = 0.0;
  for(int i = 0; i < size; i++)
    {
      average += vector[i];
    }
  return average/(double)size;
}

//Returns the clock time in seconds
double get_clock_time(struct timespec* tm)
{
  int id = clock_gettime(CLOCK_PROCESS_CPUTIME_ID, tm);
  double ts = (double)(tm->tv_sec);
  double tns = (double)(tm->tv_nsec) * 1e-9;
  return ts+tns;
}

int main(int argc, char** argv)
{
  int rank, size;
  MPI_Init(&argc, &argv);
  MPI_Comm_rank(MPI_COMM_WORLD, &rank);
  MPI_Comm_size(MPI_COMM_WORLD, &size);

  //Pretend this is the value in each processor that will need
  //to be operated on globally
  float local_value = (float)rank + 1.0;
  
  double max_times_MPI[NUM_TIMING_TRIALS];

  struct timespec tm;
  
  //Timing using MPI
  for(int i = 0; i < NUM_TIMING_TRIALS; i++)
    {
      MPI_Barrier(MPI_COMM_WORLD);

      //start timer
      double clock0 = get_clock_time(&tm);

      //Results of operations
      float global_sum_MPI;
      float global_product_MPI;
      float global_max_MPI;
      float global_min_MPI;

      //global reduce operations using MPI
      MPI_Allreduce(&local_value, &global_sum_MPI, 1, MPI_FLOAT, MPI_SUM, MPI_COMM_WORLD);
      MPI_Allreduce(&local_value, &global_product_MPI, 1, MPI_FLOAT, MPI_PROD, MPI_COMM_WORLD);
      MPI_Allreduce(&local_value, &global_max_MPI, 1, MPI_FLOAT, MPI_MAX, MPI_COMM_WORLD);
      MPI_Allreduce(&local_value, &global_min_MPI, 1, MPI_FLOAT, MPI_MIN, MPI_COMM_WORLD);
      
      //end timer
      double clock1 = get_clock_time(&tm);
      double total_clocktime = clock1 - clock0;

      //find max time of all nodes
      double maxtime;
      MPI_Allreduce(&total_clocktime, &maxtime, 1, MPI_DOUBLE, MPI_MAX, MPI_COMM_WORLD);
      max_times_MPI[i] = maxtime;
    }

  //Output of average maximum time results for main node
  if (rank == 0) {
    //printf("Global sum for MPI = %f\n", global_sum_MPI); //Bug check
    //printf("Global product for MPI= %f\n", global_product_MPI);
    //printf("Global max for MPI= %f\n", global_max_MPI); //Bug check
    //printf("Global min for MPI= %f\n", global_min_MPI);
    double average_max_time = average(max_times_MPI, NUM_TIMING_TRIALS);
    printf("Average time for MPI =\t\t%f secs\n", average_max_time);
  }

  double max_times[NUM_TIMING_TRIALS];
  
  //Timing using my global operations algorithm
  for(int i = 0; i < NUM_TIMING_TRIALS; i++)
    {
      MPI_Barrier(MPI_COMM_WORLD);

      //start time
      double clock0 = get_clock_time(&tm);

      //All values that will be operated on
      float global_sum = local_value;
      float global_product = local_value;
      float global_max = local_value;
      float global_min = local_value;
      
      int n = 1;
      while(n<size)
	{
	  int comm_with = rank^n;//flip function
	  
	  //Prepare to send all values that will be operated on globally
	  float operands[4];
	  operands[0] = global_sum;
	  operands[1] = global_product;
	  operands[2] = global_max;
	  operands[3] = global_min;
	  
	  //Send and receive could block here when swapping data so
	  //the nodes need to communicate with no blocking send for robustness
	  MPI_Request send_request;
	  MPI_Isend(operands, 4, MPI_FLOAT, comm_with, rank, MPI_COMM_WORLD, &send_request);
	         
	  float recv_operands[4];
	  MPI_Status recv_status;
	  MPI_Recv(recv_operands, 4, MPI_FLOAT, comm_with, comm_with, MPI_COMM_WORLD, &recv_status);

	  //Confirming sent values before updating
	  MPI_Status send_status;
	  MPI_Wait(&send_request, &send_status);

	  //Perform all operations
	  global_sum += recv_operands[0];
	  global_product *= recv_operands[1];
	  if(global_max < recv_operands[2])
	    global_max = recv_operands[2];
	  if(global_min > recv_operands[3])
	    global_min = recv_operands[3];

	  n = n<<1;
	}
      
      //end time
      double clock1 = get_clock_time(&tm);
      double total_clocktime = clock1 - clock0;

      //find max time of all nodes
      double maxtime;
      MPI_Allreduce(&total_clocktime, &maxtime, 1, MPI_DOUBLE, MPI_MAX, MPI_COMM_WORLD);
      max_times[i] = maxtime;
    }

  //Output of average time results using rank = 0 node
  if (rank == 0) {
    //printf("Global sum for my code = %f\n", global_sum); //Bug check
    //printf("Global product for my code = %f\n", global_product);
    //printf("Global max for my code = %f\n", global_max);
    //printf("Global min for my code = %f\n", global_min);
    double average_max_time = average(max_times, NUM_TIMING_TRIALS);
    printf("Average time for my code =\t%f secs\n", average_max_time);
  }
  
  //Print all processors used
  char* processor_names = NULL;
  if(rank == 0)
    {
      processor_names = malloc(sizeof(char)*size*MPI_MAX_PROCESSOR_NAME);
    }
  
  char processor_name[MPI_MAX_PROCESSOR_NAME];
  int result_length;
  MPI_Get_processor_name(processor_name, &result_length);
  
  MPI_Gather(&processor_name, MPI_MAX_PROCESSOR_NAME, MPI_CHAR, processor_names, MPI_MAX_PROCESSOR_NAME, MPI_CHAR, 0, MPI_COMM_WORLD);   

  if(rank == 0)
    {
      for(int i = 0; i < size; i++)
	{
	  printf("Processor #%d is %s\n",i, &processor_names[i*MPI_MAX_PROCESSOR_NAME]);
	}
      free(processor_names);
    }
  //end printing processors
  
  MPI_Finalize();
  return 0;
}
