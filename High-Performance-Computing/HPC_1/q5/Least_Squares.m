%Least Squares with Comm. Data
%Problem size
size = [1 1; 1 1000;1 2000;1 4000;];
%Communication time
time = [2.57e-05; 3.49e-04; 6.15e-04; 1.24e-03;];

X = size\time; %Solution vector contains ts and tf

%Plot of points and solution to check
figure(1)
scatter(size(:,2), time)
hold on
plot(size(:,2), size*X)