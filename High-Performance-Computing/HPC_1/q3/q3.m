%Setup data and independent variables
Data = [16777 56623 134218 inf; 8458 28381 67178 inf; 4333 14294 33693 inf; 2305 7286 16985 134426; 1326 3816 8666 67386; 871 2116 4541 33901; 678 130 2513 17193;];
p = [1; 2; 4; 8; 16; 32; 64];
n = [256 384 512 1024];

%Interpolation needed at this point
xq = 406.3747;
%Interpolate 3 row 
vq = interp1(n, Data(3,:),xq);
