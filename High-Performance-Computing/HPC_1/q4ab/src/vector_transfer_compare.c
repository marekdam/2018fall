#include "mpi.h"
#include <stdio.h>
#include <stdlib.h>
#include <time.h>

#define NUM_TIMING_TRIALS 1
#define NUM_VECTOR_SIZES 5 //n = 1000, 2000, 4000, 8000, 16000
#define NUM_COMPONENTS_SENT(nn) 1000*(1<<(nn-1))

//Averages the values of a vector
double average(double* vector, int size)
{
  double average = 0.0;
  for(int i = 0; i < size; i++)
    {
      average += vector[i];
    }
  return average/(double)size;
}

//Used to confirm communication is working
void fill_vector(float* vector, int size, float value)
{
  for(int i = 0; i < size; i++)
    {
      vector[i] = value;
    } 
}

//Returns the clock time in seconds
double get_clock_time(struct timespec* tm)
{
  int id = clock_gettime(CLOCK_PROCESS_CPUTIME_ID, tm);
  double ts = (double)(tm->tv_sec);
  double tns = (double)(tm->tv_nsec) * 1e-9;
  return ts+tns;
}

int main(int argc, char** argv)
{
  int rank, size;
  MPI_Init(&argc, &argv);
  MPI_Comm_rank(MPI_COMM_WORLD, &rank);
  MPI_Comm_size(MPI_COMM_WORLD, &size);

  double single_component_times[NUM_VECTOR_SIZES];
  double chunked_component_times[NUM_VECTOR_SIZES];

  for(int nn = 1; nn <= NUM_VECTOR_SIZES; nn++)
    {
      int n = NUM_COMPONENTS_SENT(nn);

      int vector_size = n*n;
      float* local_vector = (float*)malloc(sizeof(float)*vector_size);
      fill_vector(local_vector, vector_size, rank);
      
      double max_times_single_component[NUM_TIMING_TRIALS];
      struct timespec tm;
      
      //Timing using single component communication
      for(int t = 0; t < NUM_TIMING_TRIALS; t++)
	{
	  //Synchronize processors
	  MPI_Barrier(MPI_COMM_WORLD);

	  //start timer
	  double clock0 = get_clock_time(&tm);

	  //transfer components
	  for(int i = 0; i < vector_size; i += n)
	    {
	      //Send single component to next neighbor
	      int next_neigh = (rank+1)%size;
	      MPI_Request send_request;
	      MPI_Isend(&local_vector[i], 1, MPI_FLOAT, next_neigh, rank, MPI_COMM_WORLD, &send_request);

	      //Receive component from previous neighbour
	      int prev_neigh = (rank+size-1)%size; //makes sure previous neighbor is positive
	      float recv_component;
	      MPI_Status recv_status;
	      MPI_Recv(&recv_component, 1, MPI_FLOAT, prev_neigh, prev_neigh, MPI_COMM_WORLD, &recv_status);

	      //Confirming sent component before updating
	      MPI_Status send_status;
	      MPI_Wait(&send_request, &send_status);
	      local_vector[i] = recv_component;
	    }
	    
	  //end timer
	  double clock1 = get_clock_time(&tm);
	  double total_clocktime = clock1 - clock0;

	  //find max time of all nodes
	  double maxtime;
	  MPI_Allreduce(&total_clocktime, &maxtime, 1, MPI_DOUBLE, MPI_MAX, MPI_COMM_WORLD);
	  max_times_single_component[t] = maxtime;
	}

      //Save average time for current vector size
      if (rank == 0) {
	/* printf("Components of vector for rank 0 are:\n"); //Bug check */
	/* for(int i = 0; i < n*n; i++) */
	/* { */
	/*  printf("[%d] = %f\n", i, local_vector[i]); //Bug check */
	/* } */
	double average_max_time = average(max_times_single_component, NUM_TIMING_TRIALS);
	single_component_times[nn-1] = average_max_time;

      }

      double max_times_chunked[NUM_TIMING_TRIALS];
      fill_vector(local_vector, vector_size, rank);

      //Timing using chunked communication
      for(int t = 0; t < NUM_TIMING_TRIALS; t++)
	{
	  MPI_Barrier(MPI_COMM_WORLD);

	  //start time
	  double clock0 = get_clock_time(&tm);

	  //Chose to include memory allocation in timing
	  float* components_send = (float*)malloc(sizeof(float)*n);
	  float* components_receive = (float*)malloc(sizeof(float)*n);

	  //Copy components for sending
	  for(int i = 0; i < vector_size; i += n)
	    {
	      components_send[i/n] = local_vector[i];
	    }

	  //Send all components to next neighbor
	  int next_neigh = (rank+1)%size;
	  MPI_Request send_request;
	  MPI_Isend(components_send, n, MPI_FLOAT, next_neigh, rank, MPI_COMM_WORLD, &send_request);

	  //Receive all components from previous neighbour
	  int prev_neigh = (rank+size-1)%size;//makes sure previous neighbor is positive
	  MPI_Status recv_status;
	  MPI_Recv(components_receive, n, MPI_FLOAT, prev_neigh, prev_neigh, MPI_COMM_WORLD, &recv_status);

	  //Copy received components    
	  for(int i = 0; i < vector_size; i += n)
	    {
	      local_vector[i] = components_receive[i/n];
	    }

	  //Confirming sent components before freeing memory
	  MPI_Status send_status;
	  MPI_Wait(&send_request, &send_status);
	  
	  //cleanup before ending timer
	  free(components_send);
	  free(components_receive);
	  
	  //end time
	  double clock1 = get_clock_time(&tm);
	  double total_clocktime = clock1 - clock0;

	  //find max time of all nodes
	  double maxtime;
	  MPI_Allreduce(&total_clocktime, &maxtime, 1, MPI_DOUBLE, MPI_MAX, MPI_COMM_WORLD);      
	  max_times_chunked[t] = maxtime;
	}

      //Output of average maximum time results for main node
      if (rank == 0) {
	/* printf("Components of vector for rank 0 are:\n"); //Bug check */
	/* for(int i = 0; i < n*n; i++) */
	/* { */
	/*  printf("[%d] = %f\n", i, local_vector[i]); //Bug check */
	/* } */
	double average_max_time = average(max_times_chunked, NUM_TIMING_TRIALS);
	chunked_component_times[nn] = average_max_time;
      }
      
      //cleanup
      free(local_vector);
    }

  //Print results from experiment
  if(rank == 0)
    {
      printf("Results for sending individual components:\n");
      printf("Number of components:\tTime it took:\n");
      for(int i = 0; i < NUM_VECTOR_SIZES; i++)
	{
	  printf("\t(%d)\t\t%f secs\n", NUM_COMPONENTS_SENT(i+1), single_component_times[i]);
	}
      
      printf("Results for sending chunked components:\n");
      printf("Number of components:\tTime it took:\n");
      for(int i = 0; i < NUM_VECTOR_SIZES; i++)
	{
	  printf("\t(%d)\t\t%f secs\n", NUM_COMPONENTS_SENT(i+1), chunked_component_times[i]);
	}
    }
  
  //Print all processors used
  char* processor_names = NULL;
  if(rank == 0)
    {
      processor_names = malloc(sizeof(char)*size*MPI_MAX_PROCESSOR_NAME);
    }
  
  char processor_name[MPI_MAX_PROCESSOR_NAME];
  int result_length;
  MPI_Get_processor_name(processor_name, &result_length);
  
  MPI_Gather(&processor_name, MPI_MAX_PROCESSOR_NAME, MPI_CHAR, processor_names, MPI_MAX_PROCESSOR_NAME, MPI_CHAR, 0, MPI_COMM_WORLD);   

  if(rank == 0)
    {
      for(int i = 0; i < size; i++)
	{
	  printf("Processor #%d is %s\n",i, &processor_names[i*MPI_MAX_PROCESSOR_NAME]);
	}
      free(processor_names);
    }
  //end printing processors
  
  MPI_Finalize();
  return 0;
}
