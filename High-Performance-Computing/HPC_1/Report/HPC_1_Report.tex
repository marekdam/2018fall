\documentclass[letterpaper]{article}
\usepackage{amsmath}
\usepackage[margin=0.75in]{geometry}
\usepackage{algorithm}
\usepackage{algpseudocode}
\usepackage{graphicx}
\usepackage{listings}
\usepackage{color}
\usepackage{xcolor}

\definecolor{mGreen}{rgb}{0,0.6,0}
\definecolor{mGray}{rgb}{0.5,0.5,0.5}
\definecolor{mPurple}{rgb}{0.58,0,0.82}
\definecolor{backgroundColour}{rgb}{0.95,0.95,0.92}

\lstdefinestyle{CStyle}{
	backgroundcolor=\color{backgroundColour},   
	commentstyle=\color{mGreen},
	keywordstyle=\color{magenta},
	numberstyle=\tiny\color{mGray},
	stringstyle=\color{mPurple},
	basicstyle=\footnotesize,
	breakatwhitespace=false,         
	breaklines=true,                 
	captionpos=b,                    
	keepspaces=true,                 
	numbers=left,                    
	numbersep=5pt,                  
	showspaces=false,                
	showstringspaces=false,
	showtabs=false,                  
	tabsize=2,
	language=C
}
\definecolor{mygreen}{RGB}{28,172,0} % color values Red, Green, Blue
\definecolor{mylilas}{RGB}{170,55,241}

\lstset{language=Matlab,%
    %basicstyle=\color{red},
    breaklines=true,%
    morekeywords={matlab2tikz},
    keywordstyle=\color{blue},%
    morekeywords=[2]{1}, keywordstyle=[2]{\color{black}},
    identifierstyle=\color{black},%
    stringstyle=\color{mylilas},
    commentstyle=\color{mygreen},%
    showstringspaces=false,%without this there will be a symbol in the places where there is a space
    numbers=left,%
    numberstyle={\tiny \color{black}},% size of the numbers
    numbersep=9pt, % this defines how far the numbers are from the text
    emph=[1]{for,end,break},emphstyle=[1]\color{red}, %some words to emphasise
    %emph=[2]{word1,word2}, emphstyle=[2]{style},    
}

\makeatletter
\def\BState{\State\hskip-\ALG@thistlm}
\makeatother

\title{CSC456-2306F - High Performance Computing Assignment \#1}

\author{Damian Marek}
%\textwidth 7.0in
%\oddsidemargin 0in
%\headsep 0in
%\headheight 0in
%\headsep 0in
%\textheight 9in
%\footskip 0in
%\topmargin 0in
\begin{document}
\maketitle

\section*{Question 1}
The algorithm for broadcast to all in a hypercube with \begin{math}p = 2^d \end{math} processors is given below. It is essentially the opposite of the fan-in algorithm.

\begin{algorithm}
	\caption{One-to-All Broadcast}
	\begin{algorithmic}[1]
		\State $A \gets Message For Broadcast$
		\State $np \gets Current Processor$
		\State $npin \gets Processor That Needs To Broadcast$
		\For{ i = 0 .. $d$ - 1}
			\If{\Call{Bit}{$np$, $d$} \begin{math}\neq\end{math} \Call{Bit}{$npin$, $d$}}
				\State $SendingProcessor \gets  \Call{Flip}{np, d}$
				\State \Call{ReceiveMessage}{$SendingProcessor$, $A$}
			\Else
				\State $ReceivingProcessor \gets  \Call{Flip}{np, d}$
				\State \Call{SendMessage}{$ReceivingProcessor$, $A$}
			\EndIf	
		\EndFor		
	\end{algorithmic}
\end{algorithm}
\section*{Question 2}
The plots for Question 2 a) and b) are shown on the next page. Looking at the plots for the global summation algorithm it is evident that program execution time is a logarithmic function of the number of processors. It is also clear that there is strong agreement between the MPI version and the version that I wrote. This time dependence on the number of processors is expected, since the program that I wrote models a hypercube network. With this type of network doubling the number of processors only increases the number of communication steps by 1. 

From part a) it is clear that the bottleneck in this program are the communication steps. Sending and receiving from other processors is much slower than performing the simple arithmetic. Thus, to optimize the program in part b) the goal is to send all the data at once and perform all of the operations at each communication step. Instead of sending a single float, the processors communicate by sending 4 floats in an array. Then each processor can unpack the array and perform each operation on the individual operands.

The results in Figure 1 agree with this analysis. The naive method of using the built-in MPI\_Allreduce functions to perform each operation sequentially is much slower than the customized program I wrote that minimizes the send and receive function calls. The program I wrote in part b) is approximately as fast as the program written in part a), since there is approximately the same amount of communication time spent.

Comparing the code used in part b) I would have estimated the time difference to be a factor of 4. There are 4 calls to MPI\_Allreduce whereas there is effectively only one call in my code. Looking at the measured time it appears using the four calls to MPI\_Allreduce is only about 3 times slower. It is possible that there is some unaccounted for optimizations performed by MPI or the CDF network. Perhaps messages can be buffered and sent independently of each other, so that sending 4 messages does not exactly correspond to 4 times slower.

The C code used to create the data for these plots can be found in the Code Listings section.
\begin{figure}
\begin{center} 
 \includegraphics[ width=5in, height=5in, keepaspectratio, ]{Plot_q2ab.png}
 \end{center}
\caption{Execution times for programs using MPI\_Allreduce compared to times using my formulation}
\end{figure}
\begin{figure}
	\begin{center} 
		\includegraphics[ width=5in, height=5in, keepaspectratio, ]{Plot_q2ab_zoomed.png}
	\end{center}
\caption{Zoomed in plot of Figure 1}
\end{figure}

\pagebreak
\subsection*{Output of Global Summation}
The tests were run late at night and the processors used were under low load. It may seeem like the processors were chosen in a specific order, but it was simply because they were available.
\begin{lstlisting}[style=CStyle]
b2200-00:~/dev/csc2306f/hpc_assignment1/q2a$ mpirun -np 2 -pernode -machinefile ../m.txt global_sum_compare 
Average time for MPI =          0.000116 secs
Average time for my code =      0.000116 secs
Processor #0 is b2210-01
Processor #1 is b2210-02
b2200-00:~/dev/csc2306f/hpc_assignment1/q2a$ mpirun -np 4 -pernode -machinefile ../m.txt global_sum_compare 
Average time for MPI =          0.000198 secs
Average time for my code =      0.000194 secs
Processor #0 is b2210-01
Processor #1 is b2210-02
Processor #2 is b2210-03
Processor #3 is b2210-04
b2200-00:~/dev/csc2306f/hpc_assignment1/q2a$ mpirun -np 8 -pernode -machinefile ../m.txt global_sum_compare 
Average time for MPI =          0.000312 secs
Average time for my code =      0.000307 secs
Processor #0 is b2210-01
Processor #1 is b2210-02
Processor #2 is b2210-03
Processor #3 is b2210-04
Processor #4 is b2210-05
Processor #5 is b2210-06
Processor #6 is b2210-07
Processor #7 is b2210-08
b2200-00:~/dev/csc2306f/hpc_assignment1/q2a$ mpirun -np 16 -pernode -machinefile ../m.txt global_sum_compare 
Average time for MPI =          0.000408 secs
Average time for my code =      0.000385 secs
Processor #0 is b2210-01
Processor #1 is b2210-02
Processor #2 is b2210-03
Processor #3 is b2210-04
Processor #4 is b2210-05
Processor #5 is b2210-06
Processor #6 is b2210-07
Processor #7 is b2210-08
Processor #8 is b2210-09
Processor #9 is b2210-10
Processor #10 is b2210-11
Processor #11 is b2210-12
Processor #12 is b2210-13
Processor #13 is b2210-14
Processor #14 is b2210-15
Processor #15 is b2210-16
\end{lstlisting}
\pagebreak

\subsection*{Output for Global Operations}
\begin{lstlisting}[style=CStyle]
b2200-00:~/dev/csc2306f/hpc_assignment1/q2b$ mpirun -np 2 -pernode -machinefile ../m.txt global_operations_compare 
Average time for MPI =          0.000316 secs
Average time for my code =      0.000088 secs
Processor #0 is b2210-01
Processor #1 is b2210-02
b2200-00:~/dev/csc2306f/hpc_assignment1/q2b$ mpirun -np 4 -pernode -machinefile ../m.txt global_operations_compare 
Average time for MPI =          0.000541 secs
Average time for my code =      0.000184 secs
Processor #0 is b2210-01
Processor #1 is b2210-02
Processor #2 is b2210-03
Processor #3 is b2210-04
b2200-00:~/dev/csc2306f/hpc_assignment1/q2b$ mpirun -np 8 -pernode -machinefile ../m.txt global_operations_compare 
Average time for MPI =          0.000908 secs
Average time for my code =      0.000317 secs
Processor #0 is b2210-01
Processor #1 is b2210-02
Processor #2 is b2210-03
Processor #3 is b2210-04
Processor #4 is b2210-05
Processor #5 is b2210-06
Processor #6 is b2210-07
Processor #7 is b2210-08
b2200-00:~/dev/csc2306f/hpc_assignment1/q2b$ mpirun -np 16 -pernode -machinefile ../m.txt global_operations_compare 
Average time for MPI =          0.001218 secs
Average time for my code =      0.000398 secs
Processor #0 is b2210-01
Processor #1 is b2210-02
Processor #2 is b2210-03
Processor #3 is b2210-04
Processor #4 is b2210-05
Processor #5 is b2210-06
Processor #6 is b2210-07
Processor #7 is b2210-08
Processor #8 is b2210-09
Processor #9 is b2210-10
Processor #10 is b2210-11
Processor #11 is b2210-12
Processor #12 is b2210-13
Processor #13 is b2210-14
Processor #14 is b2210-15
Processor #15 is b2210-16
\end{lstlisting}

\section*{Question 3}
\subsection*{a)}
The fixed problem size efficiency is given by:
\begin{displaymath}
    E_p = \frac{1}{p}\frac{t_1(n)}{t_p(n)}
\end{displaymath}
where $p$ is the number of processors, $t_1(n)$ is the time the program takes to execute on one processor and $t_p(n)$ is the time the program takes to execute on $p$ processors. For a problem size of $n=256$ the single processor takes 16777. Plugging in the execution times for the $p = 4, 8, 64$ cases results in efficiencies of:
\begin{displaymath}
    E_4 = \frac{16777}{4333}/4 = 0.968 \quad
    E_8 = \frac{16777}{2305}/8 = 0.910 \quad 
    E_{64} = \frac{16777}{678}/64 = 0.387
\end{displaymath}

\subsection*{b)}
The scaled problem size efficiency is given by:
\begin{displaymath}
    E_p = \frac{1}{p}\frac{t_1(n_1)}{t_p(n_p)}\frac{\tilde{t}_1(n_p)}{\tilde{t}_1(n_1)}
\end{displaymath}
where $n_p$ is the scaled problem size, $n_1$ is the original problem size and tilde denotes modelled or theoretical times. The goal is to choose $n_p$ such that $\frac{\tilde{t}_1(n_p)}{\tilde{t}_1(n_1)}$ is approximately equal to $p$. This problem is a matrix-matrix multiplication which scales like $n^3$. Thus, $n_p \approx \sqrt[3]{p} n_1$. The equivalent problems sizes are: $n_4 \approx 406$, $n_8 = 512$ and $n_{64} = 1024$. I used Matlab to linearly interpolate the given tabulated date between $n = 384, 512$ to give a estimated $t_4(n_4) = 17685$ Using the table and the interpolated data point, the scaled efficiencies based on workload can be calculated.
\begin{displaymath}
    E_4 = \frac{16777}{17685} = 0.949 \quad
    E_8 =\frac{16777}{16985} = 0.988 \quad 
    E_{64} = \frac{16777}{17193} = 0.976
\end{displaymath}
The Matlab code used was:
\lstinputlisting{../q3/q3.m}

\subsection*{c)}
Similarly to part b) we need to calculate the scaled efficiency, but this time based on memory requirements. The storage of two unique matrices requires $2n^2$ of memory. The constant coefficient is does not play an important role in choosing the scaled problem size. The scaled problem size is now $n_p \approx \sqrt{p} n_1$ and the resulting problem sizes are: $n_4 \approx 512$ and  $n_{16} = 1024$. The calculated scaled efficiencies are then:
\begin{displaymath}
    E_4 = \frac{16777}{33693} = 0.498 \quad
    E_{16} = \frac{16777}{67386} = 0.249 \quad 
\end{displaymath}
\section*{Question 4}
The results of the communication experiment of Question 4 are given below in Table 1 and Table 2. These tables record the execution time in seconds of a program running on \textit{p} processors and sending \textit{n} components of a larger vector. There is a clear difference in the execution time for the program that communicates with individual components at a time and the other program that communicates by sending all the components at the same time in a chunk.

For the case of 16000 components, sending all the components at the same time is roughly 3 orders of magnitude faster. Both programs' execution time increases as the number of components to be sent is increased. However, as the number of processors used increases there is no noticeable affect on the execution time. This is because of the type of communication. As the number of processors increase each individual processor is still sending \textbackslash receiving the same amount of data and there is only one main communication step. Since the actual CDF communication network (LAN) is not a perfect cyclic array, if the number of processors increased dramatically it could overload the network and we should be able to see an increase in execution time.

It is also interesting to note the estimated communication bandwidth. If we choose to study the execution time for the largest number of components, we can assume most time is spent sending data. If there were 16000 components and each component was a float of 4 bytes, then the bandwidth is roughly 350 Mbps. This bandwidth is on the same order as commonly used ethernet networks. The estimated bandwidth of other case is much lower at 0.4 Mbps, which is far below the capabilities of modern networks.

The C code used to generate the tabulated data and the following output can be found in the Code Listings section.
\pagebreak
\begin{table}[!htb]
\begin{center} 
\begin{tabular}{|c|c|c|c|c|c|}
\hline 
 \textit{p} \textbackslash \space \textit{n}  & 1000 & 2000 & 4000 & 8000 & 16000 \\ 
\hline 
 2 & 0.077964 & 0.157518 & 0.301430 & 0.622121 & 1.245257\\ 
\hline 
 4 & 0.074059 & 0.153962 & 0.306072 & 0.610805 & 1.195417 \\ 
\hline 
 8 & 0.074330 & 0.151714 & 0.304783 & 0.606172 & 1.197620 \\ 
\hline 
 16 & 0.075291 & 0.149606 & 0.302371 & 0.601747 & 1.193865 \\ 
\hline 
\end{tabular} 
\end{center}
\caption{Execution times (seconds) for program sending with individual components}
\end{table}

\begin{table}[!htb]
\begin{center} 
\begin{tabular}{|c|c|c|c|c|c|}
\hline 
 \textit{p} \textbackslash \space \textit{n}  & 1000 & 2000 & 4000 & 8000 & 16000 \\ 
\hline 
 2 & 0.000000 & 0.000196 & 0.000292 & 0.000635 & 0.001465 \\ 
\hline 
 4 & 0.000000 & 0.000424 & 0.000493 & 0.000698 & 0.001283 \\ 
\hline 
 8 & 0.000000 & 0.000411 & 0.000481 & 0.000799 & 0.001314 \\ 
\hline 
 16 & 0.000000 & 0.000475 & 0.000711 & 0.000863 & 0.001836 \\ 
\hline 
\end{tabular} 
\end{center}
\caption{Execution times (seconds) for program sending with chunked components}
\end{table}

\subsection*{Output of Vector Transfer}
\begin{lstlisting}[style=CStyle]
b2200-00:~/dev/csc2306f/hpc_assignment1/q4ab$ mpirun -np 2 -pernode -machinefile ../m.txt vector_transfer_compare 
Results for sending individual components:
Number of components:   Time it took:
        (1000)          0.077964 secs
        (2000)          0.157518 secs
        (4000)          0.301430 secs
        (8000)          0.622121 secs
        (16000)         1.245257 secs
Results for sending chunked components:
Number of components:   Time it took:
        (1000)          0.000000 secs
        (2000)          0.000196 secs
        (4000)          0.000292 secs
        (8000)          0.000635 secs
        (16000)         0.001465 secs
Processor #0 is b2210-01
Processor #1 is b2210-02
b2200-00:~/dev/csc2306f/hpc_assignment1/q4ab$ mpirun -np 4 -pernode -machinefile ../m.txt vector_transfer_compare 
Results for sending individual components:
Number of components:   Time it took:
        (1000)          0.074059 secs
        (2000)          0.153962 secs
        (4000)          0.306072 secs
        (8000)          0.610805 secs
        (16000)         1.195417 secs
Results for sending chunked components:
Number of components:   Time it took:
        (1000)          0.000000 secs
        (2000)          0.000424 secs
        (4000)          0.000493 secs
        (8000)          0.000698 secs
        (16000)         0.001283 secs
Processor #0 is b2210-01
Processor #1 is b2210-02
Processor #2 is b2210-03
Processor #3 is b2210-04
b2200-00:~/dev/csc2306f/hpc_assignment1/q4ab$ mpirun -np 8 -pernode -machinefile ../m.txt vector_transfer_compare 
Results for sending individual components:
Number of components:   Time it took:
        (1000)          0.074330 secs
        (2000)          0.151714 secs
        (4000)          0.304783 secs
        (8000)          0.606172 secs
        (16000)         1.197620 secs
Results for sending chunked components:
Number of components:   Time it took:
        (1000)          0.000000 secs
        (2000)          0.000411 secs
        (4000)          0.000481 secs
        (8000)          0.000799 secs
        (16000)         0.001314 secs
Processor #0 is b2210-01
Processor #1 is b2210-02
Processor #2 is b2210-03
Processor #3 is b2210-04
Processor #4 is b2210-05
Processor #5 is b2210-06
Processor #6 is b2210-07
Processor #7 is b2210-08
b2200-00:~/dev/csc2306f/hpc_assignment1/q4ab$ mpirun -np 16 -pernode -machinefile ../m.txt vector_transfer_compare 
Results for sending individual components:
Number of components:   Time it took:
        (1000)          0.075291 secs
        (2000)          0.149606 secs
        (4000)          0.302371 secs
        (8000)          0.601747 secs
        (16000)         1.193865 secs
Results for sending chunked components:
Number of components:   Time it took:
        (1000)          0.000000 secs
        (2000)          0.000475 secs
        (4000)          0.000711 secs
        (8000)          0.000863 secs
        (16000)         0.001836 secs
Processor #0 is b2210-01
Processor #1 is b2210-02
Processor #2 is b2210-03
Processor #3 is b2210-04
Processor #4 is b2210-05
Processor #5 is b2210-06
Processor #6 is b2210-07
Processor #7 is b2210-08
Processor #8 is b2210-09
Processor #9 is b2210-10
Processor #10 is b2210-11
Processor #11 is b2210-12
Processor #12 is b2210-13
Processor #13 is b2210-14
Processor #14 is b2210-15
Processor #15 is b2210-16
\end{lstlisting}
\pagebreak
\section*{Question 5}
Fitting the tabulated data to the communication time model resulted in a \begin{math}t_s\end{math} of \begin{math}2.984e-05\end{math} and a \begin{math}t_f\end{math} of \begin{math}3.014e-07\end{math}. The startup time is almost 2 orders of magnitude larger than the latency per unit of datum. In the following Matlab code, the matrix ``size" is the problem matrix and the right-hand side vector is ``time". The solution vector is given in "X".
\lstinputlisting{../q5/Least_Squares.m}



\pagebreak
\section*{Code Listings} \label{Code Listings}
\subsection*{Code for Global Summation - Q2a}
\begin{lstlisting}[style=CStyle]
#include "mpi.h"
#include <stdio.h>
#include <stdlib.h>
#include <time.h>

#define NUM_TIMING_TRIALS 50

//Averages the values of a vector
double average(double *vector, int size)
{
  double average = 0.0;
  for (int i = 0; i < size; i++)
  {
    average += vector[i];
  }
  return average / (double)size;
}

//Returns the clock time in seconds
double get_clock_time(struct timespec *tm)
{
  int id = clock_gettime(CLOCK_PROCESS_CPUTIME_ID, tm);
  double ts = (double)(tm->tv_sec);
  double tns = (double)(tm->tv_nsec) * 1e-9;
  return ts + tns;
}

int main(int argc, char **argv)
{
  int rank, size;
  MPI_Init(&argc, &argv);
  MPI_Comm_rank(MPI_COMM_WORLD, &rank);
  MPI_Comm_size(MPI_COMM_WORLD, &size);

  float local_value = (float)rank + 1.0;
  double max_times_MPI[NUM_TIMING_TRIALS];
  float global_sum_MPI;
  struct timespec tm;

  //Timing using MPI
  for (int i = 0; i < NUM_TIMING_TRIALS; i++)
  {
    MPI_Barrier(MPI_COMM_WORLD);

    //start timer;
    double clock0 = get_clock_time(&tm);

    //global summation calculation
    MPI_Allreduce(&local_value, &global_sum_MPI, 1, MPI_FLOAT, MPI_SUM, MPI_COMM_WORLD);

    //end timer
    double clock1 = get_clock_time(&tm);
    double total_clocktime = clock1 - clock0;

    //find max time of all nodes
    double maxtime;
    MPI_Allreduce(&total_clocktime, &maxtime, 1, MPI_DOUBLE, MPI_MAX, MPI_COMM_WORLD);
    max_times_MPI[i] = maxtime;
  }

  //Output of average maximum time results for main node
  if (rank == 0)
  {
    double average_max_time = average(max_times_MPI, NUM_TIMING_TRIALS);
    printf("Average time for MPI =\t\t%f secs\n", average_max_time);
  }

  float global_sum;
  double max_times[NUM_TIMING_TRIALS];

  //Timing using my global summation
  for (int i = 0; i < NUM_TIMING_TRIALS; i++)
  {
    MPI_Barrier(MPI_COMM_WORLD);

    //start time
    double clock0 = get_clock_time(&tm);

    //Summation algorithm
    global_sum = local_value;
    int n = 1;
    while (n < size)
    {
      int comm_with = rank ^ n; //flip function
      //Send and receive could block here when swapping data so
      //the nodes need to communicate with no blocking send for robustness
      MPI_Request send_request;
      MPI_Isend(&global_sum, 1, MPI_FLOAT, comm_with, rank, MPI_COMM_WORLD, &send_request);

      float recv_sum;
      MPI_Status recv_status;
      MPI_Recv(&recv_sum, 1, MPI_FLOAT, comm_with, comm_with, MPI_COMM_WORLD, &recv_status);

      //Confirming sent global value before updating
      MPI_Status send_status;
      MPI_Wait(&send_request, &send_status);

      global_sum += recv_sum;
      n = n << 1;
    }

    //end time
    double clock1 = get_clock_time(&tm);
    double total_clocktime = clock1 - clock0;

    //find max time of all nodes
    double maxtime;
    MPI_Allreduce(&total_clocktime, &maxtime, 1, MPI_DOUBLE, MPI_MAX, MPI_COMM_WORLD);
    max_times[i] = maxtime;
  }

  //Output of average maximum time results for main node
  if (rank == 0)
  {
    double average_max_time = average(max_times, NUM_TIMING_TRIALS);
    printf("Average time for my code =\t%f secs\n", average_max_time);
  }

  //Print all processors used
  char *processor_names = NULL;
  if (rank == 0)
  {
    processor_names = malloc(sizeof(char) * size * MPI_MAX_PROCESSOR_NAME);
  }

  char processor_name[MPI_MAX_PROCESSOR_NAME];
  int result_length;
  MPI_Get_processor_name(processor_name, &result_length);

  MPI_Gather(&processor_name, MPI_MAX_PROCESSOR_NAME, MPI_CHAR, processor_names, MPI_MAX_PROCESSOR_NAME, MPI_CHAR, 0, MPI_COMM_WORLD);

  if (rank == 0)
  {
    for (int i = 0; i < size; i++)
    {
      printf("Processor #%d is %s\n", i, &processor_names[i * MPI_MAX_PROCESSOR_NAME]);
    }
    free(processor_names);
  }
  //end printing processors

  MPI_Finalize();
  return 0;
}
\end{lstlisting}

\pagebreak
\subsection*{Code for Global Operations - Q2b}
\begin{lstlisting}[style=CStyle]
#include "mpi.h"
#include <stdio.h>
#include <stdlib.h>
#include <time.h>

#define NUM_TIMING_TRIALS 50

//Averages the values of a vector
double average(double *vector, int size)
{
    double average = 0.0;
    for (int i = 0; i < size; i++)
    {
        average += vector[i];
    }
    return average / (double)size;
}

//Returns the clock time in seconds
double get_clock_time(struct timespec *tm)
{
    int id = clock_gettime(CLOCK_PROCESS_CPUTIME_ID, tm);
    double ts = (double)(tm->tv_sec);
    double tns = (double)(tm->tv_nsec) * 1e-9;
    return ts + tns;
}

int main(int argc, char **argv)
{
    int rank, size;
    MPI_Init(&argc, &argv);
    MPI_Comm_rank(MPI_COMM_WORLD, &rank);
    MPI_Comm_size(MPI_COMM_WORLD, &size);

    //Pretend this is the value in each processor that will need
    //to be operated on globally
    float local_value = (float)rank + 1.0;

    double max_times_MPI[NUM_TIMING_TRIALS];

    struct timespec tm;

    //Timing using MPI
    for (int i = 0; i < NUM_TIMING_TRIALS; i++)
    {
        MPI_Barrier(MPI_COMM_WORLD);

        //start timer
        double clock0 = get_clock_time(&tm);

        //Results of operations
        float global_sum_MPI;
        float global_product_MPI;
        float global_max_MPI;
        float global_min_MPI;

        //global reduce operations using MPI
        MPI_Allreduce(&local_value, &global_sum_MPI, 1, MPI_FLOAT, MPI_SUM, MPI_COMM_WORLD);
        MPI_Allreduce(&local_value, &global_product_MPI, 1, MPI_FLOAT, MPI_PROD, MPI_COMM_WORLD);
        MPI_Allreduce(&local_value, &global_max_MPI, 1, MPI_FLOAT, MPI_MAX, MPI_COMM_WORLD);
        MPI_Allreduce(&local_value, &global_min_MPI, 1, MPI_FLOAT, MPI_MIN, MPI_COMM_WORLD);

        //end timer
        double clock1 = get_clock_time(&tm);
        double total_clocktime = clock1 - clock0;

        //find max time of all nodes
        double maxtime;
        MPI_Allreduce(&total_clocktime, &maxtime, 1, MPI_DOUBLE, MPI_MAX, MPI_COMM_WORLD);
        max_times_MPI[i] = maxtime;
    }

    //Output of average maximum time results for main node
    if (rank == 0)
    {
        double average_max_time = average(max_times_MPI, NUM_TIMING_TRIALS);
        printf("Average time for MPI =\t\t%f secs\n", average_max_time);
    }

    double max_times[NUM_TIMING_TRIALS];

    //Timing using my global operations algorithm
    for (int i = 0; i < NUM_TIMING_TRIALS; i++)
    {
        MPI_Barrier(MPI_COMM_WORLD);

        //start time
        double clock0 = get_clock_time(&tm);

        //All values that will be operated on
        float global_sum = local_value;
        float global_product = local_value;
        float global_max = local_value;
        float global_min = local_value;

        int n = 1;
        while (n < size)
        {
            int comm_with = rank ^ n; //flip function

            //Prepare to send all values that will be operated on globally
            float operands[4];
            operands[0] = global_sum;
            operands[1] = global_product;
            operands[2] = global_max;
            operands[3] = global_min;

            //Send and receive could block here when swapping data so
            //the nodes need to communicate with no blocking send for robustness
            MPI_Request send_request;
            MPI_Isend(operands, 4, MPI_FLOAT, comm_with, rank, MPI_COMM_WORLD, &send_request);

            float recv_operands[4];
            MPI_Status recv_status;
            MPI_Recv(recv_operands, 4, MPI_FLOAT, comm_with, comm_with, MPI_COMM_WORLD, &recv_status);

            //Confirming sent values before updating
            MPI_Status send_status;
            MPI_Wait(&send_request, &send_status);

            //Perform all operations
            global_sum += recv_operands[0];
            global_product *= recv_operands[1];
            if (global_max < recv_operands[2])
                global_max = recv_operands[2];
            if (global_min > recv_operands[3])
                global_min = recv_operands[3];

            n = n << 1;
        }

        //end time
        double clock1 = get_clock_time(&tm);
        double total_clocktime = clock1 - clock0;

        //find max time of all nodes
        double maxtime;
        MPI_Allreduce(&total_clocktime, &maxtime, 1, MPI_DOUBLE, MPI_MAX, MPI_COMM_WORLD);
        max_times[i] = maxtime;
    }

    //Output of average time results using rank = 0 node
    if (rank == 0)
    {
        double average_max_time = average(max_times, NUM_TIMING_TRIALS);
        printf("Average time for my code =\t%f secs\n", average_max_time);
    }

    //Print all processors used
    char *processor_names = NULL;
    if (rank == 0)
    {
        processor_names = malloc(sizeof(char) * size * MPI_MAX_PROCESSOR_NAME);
    }

    char processor_name[MPI_MAX_PROCESSOR_NAME];
    int result_length;
    MPI_Get_processor_name(processor_name, &result_length);

    MPI_Gather(&processor_name, MPI_MAX_PROCESSOR_NAME, MPI_CHAR, processor_names, MPI_MAX_PROCESSOR_NAME, MPI_CHAR, 0, MPI_COMM_WORLD);

    if (rank == 0)
    {
        for (int i = 0; i < size; i++)
        {
            printf("Processor #%d is %s\n", i, &processor_names[i * MPI_MAX_PROCESSOR_NAME]);
        }
        free(processor_names);
    }
    //end printing processors

    MPI_Finalize();
    return 0;
}
\end{lstlisting}
\pagebreak
\subsection*{Code for Vector Transfer Comparison - Q4}
\begin{lstlisting}[style=CStyle]
#include "mpi.h"
#include <stdio.h>
#include <stdlib.h>
#include <time.h>

#define NUM_TIMING_TRIALS 1
#define NUM_VECTOR_SIZES 5 //n = 1000, 2000, 4000, 8000, 16000
#define NUM_COMPONENTS_SENT(nn) 1000 * (1 << (nn - 1))

//Averages the values of a vector
double average(double *vector, int size)
{
    double average = 0.0;
    for (int i = 0; i < size; i++)
    {
        average += vector[i];
    }
    return average / (double)size;
}

//Used to confirm communication is working
void fill_vector(float *vector, int size, float value)
{
    for (int i = 0; i < size; i++)
    {
        vector[i] = value;
    }
}

//Returns the clock time in seconds
double get_clock_time(struct timespec *tm)
{
    int id = clock_gettime(CLOCK_PROCESS_CPUTIME_ID, tm);
    double ts = (double)(tm->tv_sec);
    double tns = (double)(tm->tv_nsec) * 1e-9;
    return ts + tns;
}

int main(int argc, char **argv)
{
    int rank, size;
    MPI_Init(&argc, &argv);
    MPI_Comm_rank(MPI_COMM_WORLD, &rank);
    MPI_Comm_size(MPI_COMM_WORLD, &size);

    double single_component_times[NUM_VECTOR_SIZES];
    double chunked_component_times[NUM_VECTOR_SIZES];

    for (int nn = 1; nn <= NUM_VECTOR_SIZES; nn++)
    {
        int n = NUM_COMPONENTS_SENT(nn);

        int vector_size = n * n;
        float *local_vector = (float *)malloc(sizeof(float) * vector_size);
        fill_vector(local_vector, vector_size, rank);

        double max_times_single_component[NUM_TIMING_TRIALS];
        struct timespec tm;

        //Timing using single component communication
        for (int t = 0; t < NUM_TIMING_TRIALS; t++)
        {
            //Synchronize processors
            MPI_Barrier(MPI_COMM_WORLD);

            //start timer
            double clock0 = get_clock_time(&tm);

            //transfer components
            for (int i = 0; i < vector_size; i += n)
            {
                //Send single component to next neighbor
                int next_neigh = (rank + 1) % size;
                MPI_Request send_request;
                MPI_Isend(&local_vector[i], 1, MPI_FLOAT, next_neigh, rank, MPI_COMM_WORLD, &send_request);

                //Receive component from previous neighbour
                int prev_neigh = (rank + size - 1) % size; //makes sure previous neighbor is positive
                float recv_component;
                MPI_Status recv_status;
                MPI_Recv(&recv_component, 1, MPI_FLOAT, prev_neigh, prev_neigh, MPI_COMM_WORLD, &recv_status);

                //Confirming sent component before updating
                MPI_Status send_status;
                MPI_Wait(&send_request, &send_status);
                local_vector[i] = recv_component;
            }

            //end timer
            double clock1 = get_clock_time(&tm);
            double total_clocktime = clock1 - clock0;

            //find max time of all nodes
            double maxtime;
            MPI_Allreduce(&total_clocktime, &maxtime, 1, MPI_DOUBLE, MPI_MAX, MPI_COMM_WORLD);
            max_times_single_component[t] = maxtime;
        }

        //Save average time for current vector size
        if (rank == 0)
        {
            double average_max_time = average(max_times_single_component, NUM_TIMING_TRIALS);
            single_component_times[nn - 1] = average_max_time;
        }

        double max_times_chunked[NUM_TIMING_TRIALS];
        fill_vector(local_vector, vector_size, rank);

        //Timing using chunked communication
        for (int t = 0; t < NUM_TIMING_TRIALS; t++)
        {
            MPI_Barrier(MPI_COMM_WORLD);

            //start time
            double clock0 = get_clock_time(&tm);

            //Chose to include memory allocation in timing
            float *components_send = (float *)malloc(sizeof(float) * n);
            float *components_receive = (float *)malloc(sizeof(float) * n);

            //Copy components for sending
            for (int i = 0; i < vector_size; i += n)
            {
                components_send[i / n] = local_vector[i];
            }

            //Send all components to next neighbor
            int next_neigh = (rank + 1) % size;
            MPI_Request send_request;
            MPI_Isend(components_send, n, MPI_FLOAT, next_neigh, rank, MPI_COMM_WORLD, &send_request);

            //Receive all components from previous neighbour
            int prev_neigh = (rank + size - 1) % size; //makes sure previous neighbor is positive
            MPI_Status recv_status;
            MPI_Recv(components_receive, n, MPI_FLOAT, prev_neigh, prev_neigh, MPI_COMM_WORLD, &recv_status);

            //Copy received components
            for (int i = 0; i < vector_size; i += n)
            {
                local_vector[i] = components_receive[i / n];
            }

            //Confirming sent components before freeing memory
            MPI_Status send_status;
            MPI_Wait(&send_request, &send_status);

            //cleanup before ending timer
            free(components_send);
            free(components_receive);

            //end time
            double clock1 = get_clock_time(&tm);
            double total_clocktime = clock1 - clock0;

            //find max time of all nodes
            double maxtime;
            MPI_Allreduce(&total_clocktime, &maxtime, 1, MPI_DOUBLE, MPI_MAX, MPI_COMM_WORLD);
            max_times_chunked[t] = maxtime;
        }

        //Output of average maximum time results for main node
        if (rank == 0)
        {
            double average_max_time = average(max_times_chunked, NUM_TIMING_TRIALS);
            chunked_component_times[nn] = average_max_time;
        }

        //cleanup
        free(local_vector);
    }

    //Print results from experiment
    if (rank == 0)
    {
        printf("Results for sending individual components:\n");
        printf("Number of components:\tTime it took:\n");
        for (int i = 0; i < NUM_VECTOR_SIZES; i++)
        {
            printf("\t(%d)\t\t%f secs\n", NUM_COMPONENTS_SENT(i + 1), single_component_times[i]);
        }

        printf("Results for sending chunked components:\n");
        printf("Number of components:\tTime it took:\n");
        for (int i = 0; i < NUM_VECTOR_SIZES; i++)
        {
            printf("\t(%d)\t\t%f secs\n", NUM_COMPONENTS_SENT(i + 1), chunked_component_times[i]);
        }
    }

    //Print all processors used
    char *processor_names = NULL;
    if (rank == 0)
    {
        processor_names = malloc(sizeof(char) * size * MPI_MAX_PROCESSOR_NAME);
    }

    char processor_name[MPI_MAX_PROCESSOR_NAME];
    int result_length;
    MPI_Get_processor_name(processor_name, &result_length);

    MPI_Gather(&processor_name, MPI_MAX_PROCESSOR_NAME, MPI_CHAR, processor_names, MPI_MAX_PROCESSOR_NAME, MPI_CHAR, 0, MPI_COMM_WORLD);

    if (rank == 0)
    {
        for (int i = 0; i < size; i++)
        {
            printf("Processor #%d is %s\n", i, &processor_names[i * MPI_MAX_PROCESSOR_NAME]);
        }
        free(processor_names);
    }
    //end printing processors

    MPI_Finalize();
    return 0;
}
\end{lstlisting}

\end{document}