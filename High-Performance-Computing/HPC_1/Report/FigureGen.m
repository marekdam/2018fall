%Plotting Figures for Q2
p = [2 4 8 16];
Time_MPI_Global_Sum = [0.000116 0.000198 0.000312 0.000408];
Time_My_Code = [0.000116 0.000194 0.000307 0.000385];
Time_MPI_Global_Operations = [0.000316 0.000541 0.000908 0.001218];
Time_My_Code_Global_Operations = [0.000088 0.000184 0.000317 0.000398];

figure(1)
plot(p, 1e3*Time_MPI_Global_Sum, '-.', p, 1e3*Time_My_Code, ':', p, ...
    1e3*Time_MPI_Global_Operations, '--', p, 1e3*Time_My_Code_Global_Operations);
title('Program Execution Time vs. Number of Processors Used')
xlabel('p - Number of Processors') 
ylabel('Time (ms)') 
legend({'MPI Global Summation','My Global Summation','MPI Global Operations','My Global Operations'},'Location','northwest')
%hold on

% x = linspace(1,16,15);
% y = 3*1.5e-4*log(x);
% plot(x,y);
% hold off

%Plotting Figures for Q4
