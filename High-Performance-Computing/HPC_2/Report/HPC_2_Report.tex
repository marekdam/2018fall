\documentclass[letterpaper]{article}
\usepackage{amsmath}
\usepackage[margin=0.75in]{geometry}
\usepackage{algorithm}
\usepackage{algpseudocode}
\usepackage{graphicx}
\usepackage{listings}
\usepackage{color}
\usepackage{xcolor}
\usepackage{float}
\usepackage{placeins}
\usepackage{array, booktabs}
 
\definecolor{mGreen}{rgb}{0,0.6,0}
\definecolor{mGray}{rgb}{0.5,0.5,0.5}
\definecolor{mPurple}{rgb}{0.58,0,0.82}
\definecolor{backgroundColour}{rgb}{0.95,0.95,0.92}

\lstdefinestyle{CStyle}{
	commentstyle=\color{mGreen},
	keywordstyle=\color{magenta},
	numberstyle=\tiny\color{mGray},
	stringstyle=\color{mPurple},
	basicstyle=\footnotesize,
	breakatwhitespace=false,         
	breaklines=true,                 
	captionpos=b,                    
	keepspaces=true,                 
	numbers=left,                    
	numbersep=5pt,                  
	showspaces=false,                
	showstringspaces=false,
	showtabs=false,                  
	tabsize=2,
	language=C
}
\lstdefinestyle{BashStyle}{   
	keywordstyle=\color{magenta},
	numberstyle=\tiny\color{mGray},
	basicstyle=\footnotesize,
	breakatwhitespace=false,         
	breaklines=true,                 
	captionpos=b,                    
	keepspaces=true,                 
	numbers=left,                    
	numbersep=5pt,                  
	showspaces=false,                
	showstringspaces=false,
	showtabs=false,                  
	tabsize=2,
	language=C
}
\definecolor{mygreen}{RGB}{28,172,0} % color values Red, Green, Blue
\definecolor{mylilas}{RGB}{170,55,241}

\title{CSC456-2306F - High Performance Computing Assignment \#2}

\author{Damian Marek - 1004826307}

\begin{document}
\maketitle

\section*{Question 1}
\subsection*{a)}
The fixed problem size efficiency is given by:
\begin{displaymath}
    E_p = \frac{1}{p}\frac{t_1(n)}{t_p(n)}
\end{displaymath}
From the problem description, the sequential time complexity and parallel time complexity are:
\begin{displaymath}
    T_1(n) = n^3t_f \quad \quad T_p(n) = \frac{n^3t_f}{p} + 2\sqrt{p}(t_s+ \frac{n^2}{p}t_w)
\end{displaymath}
The fixed efficiency is given by:
\begin{displaymath}
    E_p = \frac{1}{p}\frac{n^3t_f}{\frac{n^3t_f}{p} + 2\sqrt{p}(t_s+ \frac{n^2}{p}t_w)} = \frac{n^3t_f}{n^3t_f + 2\sqrt{p}(pt_s+ n^2t_w)} = \frac{1}{1 + \frac{2\sqrt{p}(pt_s+ n^2t_w)}{n^3t_f}}
\end{displaymath}
\subsection*{b)}
To keep the efficiency constant, the workload has to scale with the number of processors so as to keep the denominator constant in part a).
\begin{displaymath}
    K = \frac{2\sqrt{p}(pt_s+ n^2t_w)}{n^3t_f}
\end{displaymath}
For large $n$, the the expression becomes:
\begin{displaymath}
    K \approx \frac{2\sqrt{p}t_w}{nt_f}
\end{displaymath}
Rearranging and taking the cube:
\begin{displaymath}
    K^3n^3t_{f}^3 \approx 8p^{3/2}t_{w}^3
\end{displaymath}
Now the sequential workload can be expressed as a function of $p$ and constant $K$:
\begin{displaymath}
    T_1(n) \approx \frac{8p^{3/2}t_{w}^3}{K^3t_{f}^2}
\end{displaymath}
The iso-efficiency function is \fbox{$p^{3/2}$}, which is between linear and quadratic, which means it is not ideal. It is also a worse scaling then $n\log(n)$, it could be described as acceptable.
\subsection*{c)}
The generalized equation for scaled efficiency is given below.
\begin{displaymath}
    E_p = \frac{1}{p}\frac{t_1(n_1)}{t_p(n_p)}\frac{\tilde{t}_1(n_p)}{\tilde{t}_1(n_1)}
\end{displaymath}
The problem size $n_p$ is chosen to satisfy $t_1(n_p) = pt_1(n_1)$. To calculate the scaled speedup then we would scale the workload by $p$. Using the sequential time complexity, we find that $n_p = p^{1/3}n_1$. The problem size is scaled by this amount to find the scaled workload speedup and also the scaled  efficiency.
\begin{displaymath}
    E_p = \frac{n^3t_f}{n^3t_f + 2\sqrt{p}(t_s+n^2t_w\frac{p^{2/3}}{p})} = \frac{1}{1 + \frac{2\sqrt{p}(t_s+n^2t_wp^{-1/3})}{n^3t_f}}
\end{displaymath}
\subsection*{d)}
The various efficiencies are plotted in Figure 1. As expected, the efficiency calculation which scales the workload in order to keep the efficiency constant is a straight line. Its initial efficiency is given by the case where $p = 1$ and $n = 256$. The fixed workload efficiency has the worst efficiency and steepest descent. The scaled workload method results in an efficiency, which drops slower and is closer to the isoefficiency constant value.

\begin{figure}[!ht]
	\centering
	\includegraphics[ width=5.5in, height=5.5in, keepaspectratio, ]{{../MATLAB/q1d}.png}
\caption{The efficiencies calculated using the three methods in the previous sections are plotted.}
\end{figure}
\FloatBarrier
\pagebreak
\section*{Question 2}
\subsection*{a)}
The computational complexity of LU factorization is given by:
\begin{displaymath}
C_{computational} = \sum_{k=1}^{n-1} \frac{n+p-k}{p}(n-k)t_f = \frac{t_f}{p}\sum_{k=1}^{n-1} n^2 + np -2nk - pk + k^2 \approx \frac{t_f}{p}\sum_{k=1}^{n-1} k^2 \approx \frac{n^3}{3p}t_f
\end{displaymath}
This comes from the main computation in the inner loop of the algorithm. The computation is calculated over $(n-k)$ columns. Each processor needs to run these column calculations over $\frac{n+p-k}{p}$ rows, because of the interleaved storage. Finally, there are $n-1$ steps $k$ which need to be calculated.

At each step $k$ a message of length $n-k+1$ needs to be broadcast from the processor containing the row to all of the other processors in the hypercube. This takes $d$ steps of communication. Using the model of communication given in the problem the total communication complexity is:
\begin{displaymath}
C_{communication} = \sum_{k=1}^{n-1} d(t_s + (n-k+1)t_w) = d t_s(n-1) + d(n-1)(n+1) t_w - \frac{d n(n-1)}{2}t_w \approx  d n t_s + d \frac{n^2}{2} t_w
\end{displaymath}
The total parallel complexity is then on the order of:
\begin{displaymath}
C_{total} = C_{computational} + C_{communication} \approx \frac{n^3}{3p}t_f + d n t_s + d \frac{n^2}{2} t_w 
\end{displaymath}
In a send-ahead strategy, the time spent communicating and time spent computing can be overlapped, which should shorten the overall time. The processor which contains the row that needs to be sent in the coming step k + 1 can send it as soon as it is updated. Fortunately, the row required is the first one that is updated. While the message is being sent the processor can continue updating the rest of the rows that it owns. If the computation time and communication time are about the same magnitude and can be thought of as independent operations, then the total time complexity will be closer to the maximum of either computation or communication time.

\subsection*{b)}
The computational complexity for the column storage LU factorization is the same as the row storage case, since the same operations have to be done, but now each processors loops through its columns and all of the required rows.
\begin{displaymath}
C_{computational} \approx \frac{n^3}{3p}t_f
\end{displaymath}
The communication complexity is the same as well. Instead of processor with row k transmitting to all processors at each step, the processor which contains column k transmits it to all of the processors. The processor will already contain the kth row of the column it is working on.

\begin{displaymath}
C_{communication} \approx  d n t_s + d \frac{n^2}{2} t_w
\end{displaymath}
\begin{displaymath}
C_{total} = C_{computational} + C_{communication} \approx \frac{n^3}{3p}t_f + d n t_s + d \frac{n^2}{2} t_w
\end{displaymath}

The send-ahead strategy will not work in this case. If the structure of the loops cannot be changed, this results in each row getting updated completely before moving onto the next row. The column that is needed in the $k+1$ step will not be completed until the last row is updated, which is immediately before the next $k+1$ step. To take advantage of send-ahead the loop order would need to be reversed, so that the first column is updated immediately.

\pagebreak

\section*{Question 3}
In the following sections the computational complexity for various narrow-banded matrix operations need to be used. The following table lists some of the complexities used:
\begin{table}[htb]
\begin{center} 
\begin{tabular}{|c|c|}
\hline 
Operation & Cost Assuming \(\beta \ll n \) \\
\hline 
 LU Factor \(A_{n \times n} \) & \(\beta^2n\) \\
\hline
 Forward Substituion \(A_{n \times n} \) &\(\beta n\) \\
\hline 
 Backward Substitution \(A_{n \times n} \) & \(\beta n\) \\
\hline 
 Solve LU Factored Equation  \(A_{n \times n} \) &\(2\beta n\) \\
\hline 
\end{tabular} 
\end{center}
\caption{Approximate Cost of Operations on Matrices with Bandwidth $\beta$}
\end{table}

\begin{table}[htb]
\begin{center} 
\begin{tabular}{|c|c|}
\hline 
Operation & Cost \\
\hline 
 Matrix Multiplication \(A_{m \times n} \times B_{n \times l} \) & \(mnl\) \\
\hline 
\end{tabular} 
\end{center}
\caption{Approximate Cost of General Matrix Operations}
\end{table}

\subsection*{a)}
The complexity of the partitioning method II for one processor can be written done by using the complexity of the constituent operations. Some of the operations are not needed if there is only one processor (Step 3, Step 6, Step 7). The dimension of many of the matrices is $q-\beta$, which is approximated as $q$, since the matrices are narrow-banded. Furthermore, solving the reduced system assumes worst case that the matrix has a bandwidth of $2\beta$ and size $\beta$.

In each step, the dominant term is displayed in the table, the complexity might depend on lower order values, which become small as the problem size is increased.

\begin{table}[htb]
\begin{center} 
\begin{tabular}{|c|c|c|}
\hline 
Step 1  & LU Factor \(A_{11} ( q - \beta \times q - \beta) \) & \(\beta^2q\)\\ 
\hline 
Step 2 & Solve \(A_{11}A_{12}^{'} = A_{12} \) & \(2\beta^2q\)\\ 
\hline 
Step 3 & No Operation & 0\\ 
\hline 
Step 4 & Solve \(A_{11}b_{11}^{'} = b_{11} \) & \(2\beta q\)\\ 
\hline 
Step 5 & Compute  \(A_{14}^{'} = A_{14} - A_{13}A_{12}^{'} \) & \(\beta^2q\)\\ 
\hline 
Step 6  & No Operation & 0\\ 
\hline 
Step 7 & No Operation & 0\\ 
\hline 
Step 8 & Compute  \(b_{12}^{'} = b_{12} - A_{13}b_{11}^{'} \) & \(\beta q\)\\ 
\hline 
Step 9 & Solve Reduced System \((\beta \times \beta)\) & \( 4 \beta^3\) \\ 
\hline 
Step 10 & Compute \(x_{11} = b_{11}^{'} - A_{12}^{'}x_{12} \)  & \(\beta q\)\\ 
\hline
Total Cost & -------- & \(4\beta^2q + 4 \beta q + 4 \beta^3 \) \\
\hline
\end{tabular} 
\end{center}
\caption{The approximate cost of operations using one processor for partition method 2.}
\end{table}
The dominant terms of the total cost are: \fbox{$4\beta^2 q + 4\beta q$}. Keep in mind $q$ is equal to $n$ for a sequential algorithm running on one processor. For a narrow banded matrix $\beta$ should not be large, so the two terms will be of roughly same magnitude.
\subsection*{b)}
From Table 1, the sequential complexity of solving a narrow-banded matrix can be found to be: \fbox{$\beta^2 n + 2\beta n$}. Recognizing that for one processor $q = n$ in partitioning method II, we can see that the parallel algorithm is about 4 times slower. However, the algorithms scales at the same order with respect to problem size.
\subsection*{c)}
From inspection of the partitioning method II, there is a clear method for parallizing it. Each processor, $p$, will hold its respective block matrices, by replacing $i$ with $p$. Then each processor is responsible for performing each step in the algorithm on its respective block matrices. There is a requirement for communication at certain steps. Steps 5, 6, and 8 require data (block matrices and vectors) from the adjacent processor. Step 9 requires all processors to share the reduced problem. In step 10, no communication is required if each processor duplicates the work in step 9 and thus has the reduced problem solution. The final step of the algorithm might be to gather the solution into one processor, which also requires communication.
\subsection*{d)}
\begin{table}[ht]
\begin{center} 
\begin{tabular}{|c|c|c|}
\hline
Step 1  & LU Factor \(A_{p1} ( q - \beta \times q - \beta) \) & \(\beta^2q\)\\
\hline 
Step 2 & Solve \(A_{p1}A_{p2}^{'} = A_{p2} \) & \(2\beta^2q\)\\ 
\hline 
Step 3 & Solve \(A_{p1}C_{p1}^{'} = C_{p1} \) & \(2\beta^2q\)\\ 
\hline 
Step 4 & Solve \(A_{p1}b_{p1}^{'} = b_{p1} \) & \(2\beta q\)\\ 
\hline 
Step 5 & Compute  \(A_{p4}^{'} = A_{p4} - A_{p3}A_{p2}^{'} - B_{p1}C_{p + 1,1}^{'} \) & \(2\beta^2q\)\\ 
\hline 
Step 6  & Compute  \(B_{p2}^{'} = B_{p2} - B_{p1}A_{p + 1,2}^{'}\) & \(\beta^2q\)\\ 
\hline 
Step 7 & Compute  \(C_{p2}^{'} = C_{p2} - A_{p3}C_{p,1}^{'}\) & \(\beta^2q\)\\ 
\hline 
Step 8 & Compute  \(b_{p2}^{'} = b_{p2} - A_{p3}b_{p1}^{'} - B_{p1}b_{p + 1,1}^{'} \) & \(2\beta q\)\\ 
\hline 
Step 9 & Solve Reduced System \((\beta p \times \beta p)\) & \( 4 \beta^3p\) \\ 
\hline 
Step 10 & Compute \(x_{p1} = b_{p1}^{'} - A_{p2}^{'}x_{p2} - C_{p1}^{'}x_{p - 1,2} \)  & \(2\beta q\)\\ 
\hline
Total Cost & -------- & \(9\beta^2q + 6 \beta q + 4 \beta^3 p \) \\
\hline
\end{tabular} 
\end{center}
\caption{The approximate parallel complexity using $p$ processors for partition method 2.}
\end{table}
The dominant terms for the parallel complexity are: \fbox{$9\beta^2 q + 6\beta q$}. For a narrow banded matrix $\beta$ should not be large, so the two terms will be of roughly same magnitude.
\subsection*{e)}
Each processor needs to calculate and then send the following matrices/vectors: $A_{p2}^{'}, b_{p,1}^{'},C_{p,1}^{'}$ with respective sizes: $(q-\beta \times \beta), (q-\beta \times 1), (q-\beta \times \beta)$. The total amount of elements that need to be sent is then: $2\beta q -2 \beta^2 +q - \beta$ The processor needs to send to the processor adjacent with a lower index and at the same time receive the same data from the processor with the larger index. This is a description of a linear array communication network that can be embedded inside a hypercube. The total complexity of this communication step is:
\begin{displaymath}
T_{Comm1} = t_s + mt_w = t_s + t_w(2\beta q -2 \beta^2 +q - \beta)
\end{displaymath}
Before solving the reduced problem in Step 9, the data needs to be shared amongst all processors in the hypercube, this requires an all-to-all broadcast. The vector $b_{p,2}$ with size $(\beta \times 1)$ and matrices $A_{p4}^{'}, B_{p,2}^{'},C_{p,2}^{'}$ all with sizes: $(\beta \times \beta)$ need to be shared. The total number of elements each processor needs to share is: $3\beta^2 + \beta$. It is possible to optimize this step, so that the first and last processor share less data, but the number of elements given above is still approximately accurate. In a hypercube, total exchange takes place over $d = \log_2p$ steps. However, at each step the amount of data that is transmitted doubles, which is a geometric series.
\begin{displaymath}
T_{Comm2} = \sum_{k=0}^{d-1} t_s + t_w(3\beta^2 + \beta)2^{k} = d t_s + t_w(3\beta^2 + \beta)\sum_{k=0}^{d-1}2^{k} = d t_s + t_w(3\beta^2 + \beta)(2^d - 1) = d t_s + t_w(3\beta^2 + \beta)(p - 1)
\end{displaymath}
The total communication complexity is then:
\begin{displaymath}
T_{tot} = T_{Comm1} + T_{Comm2} = (d+1)t_s + t_w((2\beta q -2 \beta^2 +q - \beta) + (3\beta^2 + \beta)(p - 1))
\end{displaymath}
\begin{displaymath}
T_{tot} \approx (d+1)t_s + t_w((2\beta + 1) q + 3\beta^2(p - 1))
\end{displaymath}
\subsection*{f)}
The C code for this question is given at the end of the report in the code listing section. There are 3 files that were used: ``q3f.c", ``linear\_algebra.h" and ``linear\_algebra.c".
\\
From the output of the program, it clearly shows that as $n$ increases the maximum error decreases. The error decreases faster than the $n$ increases, it is not a linear relationship. Importantly, the quadratic spline is able to perfectly reproduce a quadratic function. There is some floating point error from the operations performed.
\begin{table}[ht]
\begin{center} 
\begin{tabular}{|c|c|c|}
\hline
 & Order of Convergence for Knots & Order of Convergence for $3n+1$ points\\
\hline 
(30, 62) & 4.25  & 3.81 \\ 
\hline 
(62, 126) & 4.13 & 3.15 \\ 
\hline 
(126, 254) & 4.05 & 3.05 \\ 
\hline
(254, 510) & 4.02 & 3.02\\ 
\hline
\end{tabular} 
\end{center}
\caption{The experimentally calculated orders of convergence of maximum errors on equidistant points.}
\end{table}

One of the optimizations I made was to minimize the communication time by minimizing the amount of elements each processor needs to share with its adjacent neighbor. Since the total matrix is tridiagonal the block matrix $B_{p1}$ only has one none zero element. Instead of receiving the complete primed matrices that it needs to multiply, the neighbor processor only sends the correct single element.
\subsection*{Output of Program} 
\begin{lstlisting}[style=BashStyle]
dmarek@calculator:~/dev/hpcompute/hpc_2/q3f/build/Release$ mpirun -np 4 ./q3f

Maximum errors for data from x^2 in domain (0, 1)

n=30;   Max Error on Knots =        2.22044605e-16
        Max Error on 3n+1 points =  3.33066907e-16
n=62;   Max Error on Knots =        1.11022302e-16
        Max Error on 3n+1 points =  3.33066907e-16
n=126;	Max Error on Knots =        2.22044605e-16
        Max Error on 3n+1 points = 	3.33066907e-16
n=254;	Max Error on Knots =        2.22044605e-16
        Max Error on 3n+1 points = 	3.33066907e-16
n=510;	Max Error on Knots =        2.22044605e-16
        Max Error on 3n+1 points = 	3.33066907e-16

Maximum errors for data from sin(x) in domain (0, 12*PI)

n=30;   Max Error on Knots =        2.09665402e-02
        Max Error on 3n+1 points = 	2.09665402e-02
n=62;   Max Error on Knots =        1.09952285e-03
        Max Error on 3n+1 points =  1.49109026e-03
n=126;  Max Error on Knots =        6.28994188e-05
        Max Error on 3n+1 points =  1.68263922e-04
n=254;	Max Error on Knots =        3.79789634e-06
        Max Error on 3n+1 points =  2.02714204e-05
n=510;	Max Error on Knots =        2.33322896e-07
        Max Error on 3n+1 points = 	2.49594656e-06
\end{lstlisting}
\pagebreak
\section*{Code Listing for Question 3 f)}
\subsection*{Filename: q3f.c}
\lstinputlisting[style=CStyle]{../q3f/src/q3f.c}
\pagebreak
\subsection*{Filename: linear\_algebra.h}
\lstinputlisting[style=CStyle]{../q3f/src/linear_algebra.h}
\pagebreak
\subsection*{Filename: linear\_algebra.c}
\lstinputlisting[style=CStyle]{../q3f/src/linear_algebra.c}
\end{document}