#ifndef LINEAR_ALGEBRA_H
#define LINEAR_ALGEBRA_H

struct _TriDiagMat
{
	double *D; //Main diagonal
	double *DL; //Lower diagonal
	double *DU;	//Upper diagonal
	int n;	// Dimension of matrix
};
typedef struct _TriDiagMat TriDiagMat;

TriDiagMat *TriDiagMat_initialize(int n);

void TriDiagMat_destroy(TriDiagMat *A);

void fill_vector(double * src, int n, double value);

//Assuming a tridiagonal matrix given by the main diagonal D,
//the lower diagonal DL and the upper diagonal DU, the LU
//factor calculation can be optimized
void LU_factor_tridiagonal(TriDiagMat *A);

//Assuming a lower triagonal matrix with a main diagonal of ones,
//and a lower diagonal DL the forward substitution algorithm takes
//this optimized form
void fwd_sub_tri(TriDiagMat *A, double *b, double *y);

//Assuming an upper triagonal matrix with a main diagonal of D,
//and an upper diagonal DU the backward substitution algorithm takes
//this optimized form
void bwd_sub_tri(TriDiagMat *A, double *c, double *x);

//A should be in LU form
void tridiagonal_matrix_solve(TriDiagMat *A_LU, double *x, double *b);

//Debug functions to check matrix components
void print_tridiagonal_matrix(TriDiagMat* A);
void print_vector(double *x, int n);

#endif