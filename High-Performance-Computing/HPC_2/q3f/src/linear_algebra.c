#include "linear_algebra.h"
#include <assert.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

TriDiagMat *TriDiagMat_initialize(int n)
{
	assert(n > 0);

	double *D = (double *)malloc(sizeof(double) * n);
	double *DL = (double *)malloc(sizeof(double) * (n - 1));
	double *DU = (double *)malloc(sizeof(double) * (n - 1));

	if(D && DL && DU)
	{
		TriDiagMat *A = (TriDiagMat *)malloc(sizeof(TriDiagMat));
		if(A)
		{
			A->n = n;
			A->D = D;
			A->DL = DL;
			A->DU = DU;
			return A;
		}
	}

	free(D);
	free(DL);
	free(DU);
	return NULL;
}

void TriDiagMat_destroy(TriDiagMat *A)
{
	free(A->D);
	free(A->DL);
	free(A->DU);
	free(A);
}

void fill_vector(double * src, int n, double value)
{
	for(int i = 0; i < n; i++)
	{
		src[i] = value;
	}
}

//Assuming a tridiagonal matrix given by the main diagonal D,
//the lower diagonal DL and the upper diagonal DU, the LU
//factor calculation can be optimized
void LU_factor_tridiagonal(TriDiagMat *A)
{
	double *DL = A->DL;
	double *D = A->D;
	double *DU = A->DU;
	int size = A->n;
	for (int k = 0; k < size - 1; k++)
	{
		DL[k] = DL[k] / D[k];
		D[k + 1] = D[k + 1] - DL[k] * DU[k];
	}
}

//Assuming a lower triagonal matrix with a main diagonal of ones,
//and a lower diagonal DL the forward substitution algorithm takes
//this optimized form
void fwd_sub_tri(TriDiagMat *A, double *b, double *y)
{
	y[0] = b[0];
	double *DL = A->DL;
	int size = A->n;
	for (int i = 1; i < size; i++)
	{
		y[i] = b[i] - DL[i - 1] * y[i - 1];
	}
}

//Assuming an upper triagonal matrix with a main diagonal of D,
//and an upper diagonal DU the backward substitution algorithm takes
//this optimized form
void bwd_sub_tri(TriDiagMat *A, double *c, double *x)
{
	double *D = A->D;
	double *DU = A->DU;
	int size = A->n;
	x[size - 1] = c[size - 1] / D[size - 1];

	for (int i = size - 2; i >= 0; i--)
	{
		x[i] = c[i] - DU[i] * x[i + 1];
		x[i] = x[i] / D[i];
	}
}

//A should be in LU form
void tridiagonal_matrix_solve(TriDiagMat *A_LU, double *x, double *b)
{
	fwd_sub_tri(A_LU, b, x);
	bwd_sub_tri(A_LU, x, x);
}

//Debug function to check matrix components
void print_tridiagonal_matrix(TriDiagMat *A)
{
	int size = A->n;
	double *DL = A->DL;
	double *D = A->D;
	double *DU = A->DU;
	for (int i = 0; i < size; i++)
	{
		for (int j = 0; j < size; j++)
		{
			if (i == j + 1)
			{
				printf("%.4g\t", DL[j]);
			}
			else if (i == j)
			{
				printf("%.4g\t", D[i]);
			}
			else if (i + 1 == j)
				printf("%.4g\t", DU[i]);
			else
			{
				printf("--\t");
			}
		}
		printf("\n");
	}
}

void print_vector(double *x, int n)
{
	for (int i = 0; i < n; i++)
	{
		printf("%.4g ", x[i]);
	}
	printf("\n");
}