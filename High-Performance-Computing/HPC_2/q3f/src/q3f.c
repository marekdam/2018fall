#include "mpi.h"
#include <assert.h>
#include <stdio.h>
#include <stdlib.h>
#include <math.h>

#include "linear_algebra.h"

double *partition2_tridiagonal(int rank, int size, TriDiagMat *A_p1, double A_p2, double A_p3,
							   double A_p4, double B_p1, double C_p1, double *b_p1, double b_p2);

double calc_max_error_spline(int n, int from_function_switch, int rank, int num_procs, int num_test_points);

void populate_equally_distributed_points(double *array, double start, double end, int N)
{
	double delta = (end - start) / (N - 1);
	for (int i = 0; i < N; i++)
	{
		array[i] = start + i * delta;
	}
}

//Populates the U vector for x^2 case
void populate_u_vector_x2(double *array, double global_start, double global_end, int N, int rank, int num_procs)
{
	//N is not the same as lowercase "n", it is the dimension of the problem
	//This delta is distance between knots, and there are
	//N-1 knots
	double delta = (global_end - global_start) / (N - 2);
	int q = N / num_procs;
	int midpoint_idx_start = rank * q - 1;
	int midpoint_idx_end = (rank + 1) * q - 1;
	int array_idx = 0;
	if (rank == 0)
	{
		array[0] = global_start * global_start;
		array_idx++;
		midpoint_idx_start = 0;
	}
	if (rank == num_procs - 1)
	{
		array[q - 1] = global_end * global_end;
		midpoint_idx_end = N - 2;
	}

	double midpoint_start = global_start + delta / 2;
	for (int i = midpoint_idx_start; i < midpoint_idx_end; i++)
	{
		double x_pos = midpoint_start + i * delta;
		array[array_idx] = x_pos * x_pos;
		array_idx++;
	}
}

//Populates the U vector for sin(x) case
void populate_u_vector_sin(double *array, double global_start, double global_end, int N, int rank, int num_procs)
{
	//N is not the same as lowercase "n", it is the dimension of the problem
	//This delta is distance between knots, and there are
	//N-1 knots
	double delta = (global_end - global_start) / (N - 2);
	int q = N / num_procs;
	int midpoint_idx_start = rank * q - 1;
	int midpoint_idx_end = (rank + 1) * q - 1;
	int array_idx = 0;
	if (rank == 0)
	{
		array[0] = sin(global_start);
		array_idx++;
		midpoint_idx_start = 0;
	}
	if (rank == num_procs - 1)
	{
		array[q - 1] = sin(global_end);
		midpoint_idx_end = N - 2;
	}

	double midpoint_start = global_start + delta / 2;
	for (int i = midpoint_idx_start; i < midpoint_idx_end; i++)
	{
		double x_pos = midpoint_start + i * delta;
		array[array_idx] = sin(x_pos);
		array_idx++;
	}
}

//Populate the Matrix used to solve quadratic spline problems
void populate_spline_matrix(TriDiagMat *matrix, int N, int rank, int num_procs)
{
	int q = N / num_procs;
	assert(matrix->n == q - 1);

	if (rank == 0)
	{
		matrix->D[0] = 0.5;
	}
	else
	{
		matrix->D[0] = 0.75;
	}

	for (int i = 0; i < (q - 2); i++)
	{
		matrix->D[i + 1] = 0.75;
		matrix->DU[i] = 1.0 / 8.0;
		matrix->DL[i] = 1.0 / 8.0;
	}

	if (rank == 0)
	{
		matrix->DU[0] = 0.5;
	}
}

//Given a test point returns the interval [x_i-1, x_i] given by knot locations
int find_interval(double *points, int size_points, double test_point)
{
	for (int i = 0; i < size_points; i++)
	{
		if (test_point <= points[i])
		{
			return i;
		}
	}
	return size_points;
}

double model_function(double x)
{
	if (x >= 0 && x <= 1)
	{
		return x * x / 2.;
	}
	else if (x >= 1 && x <= 2)
	{
		double x1 = x - 1;
		return 0.5 * (-2. * x1 * x1 + 2. * x1 + 1);
	}
	else if (x >= 2. && x <= 3.)
	{
		return 0.5 * (3. - x) * (3. - x);
	}
	else
	{
		return 0.0;
	}
}

int main(int argc, char **argv)
{
	int rank, num_procs;
	MPI_Init(&argc, &argv);
	MPI_Comm_rank(MPI_COMM_WORLD, &rank);
	MPI_Comm_size(MPI_COMM_WORLD, &num_procs);

	int n_array[5] = {30, 62, 126, 254, 510};

	if (rank == 0)
	{
		printf("\nMaximum errors for data from x^2 in domain (0, 1)\n\n");
	}

	for (int i = 0; i < 5; i++)
	{
		double max_error = calc_max_error_spline(n_array[i], 0, rank, num_procs, n_array[i] + 1);
		double max_error_3n = calc_max_error_spline(n_array[i], 0, rank, num_procs, 3 * n_array[i] + 1);
		if (rank == 0)
		{
			printf("n=%d;\tMax Error on Knots = \t\t%.8e\n", n_array[i], max_error);
			printf("\tMax Error on 3n+1 points = \t%.8e\n", max_error_3n);
		}
	}

	if (rank == 0)
	{
		printf("\nMaximum errors for data from sin(x) in domain (0, 12*PI)\n\n");
	}

	for (int i = 0; i < 5; i++)
	{
		double max_error = calc_max_error_spline(n_array[i], 1, rank, num_procs, n_array[i] + 1);
		double max_error_3n = calc_max_error_spline(n_array[i], 1, rank, num_procs, 3 * n_array[i] + 1);
		if (rank == 0)
		{
			printf("n=%d;\tMax Error on Knots = \t\t%.8e\n", n_array[i], max_error);
			printf("\tMax Error on 3n+1 points = \t%.8e\n", max_error_3n);
		}
	}

	MPI_Finalize();
	return EXIT_SUCCESS;
}

double calc_max_error_spline(int n, int function_switch, int rank, int num_procs, int num_test_points)
{
	assert(function_switch == 0 || function_switch == 1);
	int problem_dimension = n + 2;		   //Dimension of matrix that will need to be solved
	int q = problem_dimension / num_procs; //Size of the sub matrices/solution vectors for each proc.
	double global_start = 0.0;
	double global_end = 1.0;

	if (function_switch == 1)
	{
		global_end = 12 * M_PI;
	}

	TriDiagMat *A_tri = TriDiagMat_initialize(q - 1);
	double *u = (double *)malloc(sizeof(double) * (q));
	double *u_global = (double *)malloc(sizeof(double) * (problem_dimension));
	double *x_global = (double *)malloc(sizeof(double) * (problem_dimension));

	populate_spline_matrix(A_tri, problem_dimension, rank, num_procs);

	if (function_switch == 0)
	{
		populate_u_vector_x2(u, global_start, global_end, problem_dimension, rank, num_procs);
	}
	else
	{
		populate_u_vector_sin(u, global_start, global_end, problem_dimension, rank, num_procs);
	}

	double Ap2 = 1.0 / 8.0;
	double Ap3 = 1.0 / 8.0;
	double Ap4 = 0.75;
	double Bp1 = 1.0 / 8.0;
	double Cp1 = 1.0 / 8.0;

	if (rank == num_procs - 1)
	{
		Ap3 = 0.5;
		Ap4 = 0.5;
	}

	//Here the "p" denotes the subsection of the matrix/vector that belongs to processor p
	double *c_p = partition2_tridiagonal(rank, num_procs, A_tri, Ap2, Ap3, Ap4, Bp1, Cp1, u, u[q - 1]);

	//Need to share boundary values of the solution vector
	//so that each processor can calculate the spline
	//value in its interval
	double cp_minus = 0.0;
	double cp_plus = 0.0;
	if (num_procs > 1)
	{
		if (rank == 0)
		{
			MPI_Request plus_request;
			MPI_Isend(&c_p[q - 1], 1, MPI_DOUBLE, rank + 1, rank, MPI_COMM_WORLD, &plus_request);
			MPI_Status recv_status_plus;
			MPI_Recv(&cp_plus, 1, MPI_DOUBLE, rank + 1, rank + 1, MPI_COMM_WORLD, &recv_status_plus);
			MPI_Status plus_status;
			MPI_Wait(&plus_request, &plus_status);
		}
		else if (rank == num_procs - 1)
		{
			MPI_Request minus_request;
			MPI_Isend(&c_p[0], 1, MPI_DOUBLE, rank - 1, rank, MPI_COMM_WORLD, &minus_request);
			MPI_Status recv_status_minus;
			MPI_Recv(&cp_minus, 1, MPI_DOUBLE, rank - 1, rank - 1, MPI_COMM_WORLD, &recv_status_minus);
			MPI_Status minus_status;
			MPI_Wait(&minus_request, &minus_status);
		}
		else
		{
			MPI_Request plus_request;
			MPI_Isend(&c_p[q - 1], 1, MPI_DOUBLE, rank + 1, rank, MPI_COMM_WORLD, &plus_request);
			MPI_Status recv_status_plus;
			MPI_Recv(&cp_plus, 1, MPI_DOUBLE, rank + 1, rank + 1, MPI_COMM_WORLD, &recv_status_plus);
			MPI_Request minus_request;
			MPI_Isend(&c_p[0], 1, MPI_DOUBLE, rank - 1, rank, MPI_COMM_WORLD, &minus_request);
			MPI_Status recv_status_minus;
			MPI_Recv(&cp_minus, 1, MPI_DOUBLE, rank - 1, rank - 1, MPI_COMM_WORLD, &recv_status_minus);
			MPI_Status plus_status;
			MPI_Wait(&plus_request, &plus_status);
			MPI_Status minus_status;
			MPI_Wait(&minus_request, &minus_status);
		}
	}

	//Calculate spline at points
	int num_knots = problem_dimension - 1;
	double *global_knot_coords = (double *)malloc(sizeof(double) * (num_knots));
	populate_equally_distributed_points(global_knot_coords, global_start, global_end, num_knots);

	int num_knots_p = num_knots / num_procs + 1; //The last processor actually only contains num_knots_p - 1
	double interval_p_start = global_knot_coords[rank * num_knots_p];
	double interval_p_end = global_knot_coords[(rank + 1) * num_knots_p - 1];
	if (rank == num_procs - 1)
	{
		interval_p_end = global_knot_coords[num_knots - 1];
	}

	double *test_points = (double *)malloc(sizeof(double) * (num_test_points));
	double *result = (double *)malloc(sizeof(double) * (num_test_points));
	populate_equally_distributed_points(test_points, global_start, global_end, num_test_points);
	fill_vector(result, num_test_points, -1.0);

	double delta_knots = (global_end - global_start) / (num_knots - 1);
	double max_error = -1.0;
	for (int i = 0; i < num_test_points; i++)
	{
		if (test_points[i] >= interval_p_start && test_points[i] <= interval_p_end)
		{
			int interval = find_interval(global_knot_coords, num_knots, test_points[i]);
			int local_interval = interval - rank * num_knots_p;
			double normalized_x = (test_points[i] - global_start) / delta_knots - interval + 2;
			double psi1 = model_function(normalized_x);
			normalized_x = (test_points[i] - global_start) / delta_knots - interval + 3;
			double psi_prev = model_function(normalized_x);
			normalized_x = (test_points[i] - global_start) / delta_knots - interval + 1;
			double psi_next = model_function(normalized_x);
			result[i] = c_p[local_interval] * psi1;

			if (local_interval != 0)
			{
				result[i] += c_p[local_interval - 1] * psi_prev;
			}
			else
			{
				result[i] += cp_minus * psi_prev;
			}
			if (local_interval != q - 1)
			{
				result[i] += c_p[local_interval + 1] * psi_next;
			}
			else
			{
				result[i] += cp_plus * psi_next;
			}

			double exact = -1.0;
			if (function_switch == 0)
			{
				exact = test_points[i] * test_points[i];
			}
			else
			{
				exact = sin(test_points[i]);
			}

			double error = fabs(result[i] - exact);
			if (error > max_error)
			{
				max_error = error;
			}
		}
	}

	double global_max_error = -1.0;
	MPI_Reduce(&max_error, &global_max_error, 1, MPI_DOUBLE, MPI_MAX, 0, MPI_COMM_WORLD);

	free(result);
	free(test_points);
	free(global_knot_coords);
	TriDiagMat_destroy(A_tri);
	free(u);
	free(c_p);
	free(u_global);
	free(x_global);

	return global_max_error;
}

//Assume Beta is 1 so many "matrix" blocks are actually single values or vectors
double *partition2_tridiagonal(int rank, int size, TriDiagMat *A_p1, double A_p2, double A_p3, double A_p4,
							   double B_p1, double C_p1, double *b_p1, double b_p2)
{
	int q = A_p1->n + 1;

	double message[3] = {0.0, 0.0, 0.0};
	//Step 1
	LU_factor_tridiagonal(A_p1);

	//Step 2
	double *rhs = (double *)malloc(sizeof(double) * (q - 1));
	double *A_p2_prime = (double *)malloc(sizeof(double) * (q - 1));
	fill_vector(rhs, q - 1, 0.0);
	rhs[q - 2] = A_p2;
	tridiagonal_matrix_solve(A_p1, A_p2_prime, rhs);
	message[0] = A_p2_prime[0];

	//Step 3
	double *C_p1_prime = NULL;
	if (rank != 0)
	{
		C_p1_prime = (double *)malloc(sizeof(double) * (q - 1));
		fill_vector(rhs, q - 1, 0.0);
		rhs[0] = C_p1;
		tridiagonal_matrix_solve(A_p1, C_p1_prime, rhs);
		message[1] = C_p1_prime[0];
	}
	free(rhs);

	//Step 4
	double *b_p1_prime = (double *)malloc(sizeof(double) * (q - 1));
	tridiagonal_matrix_solve(A_p1, b_p1_prime, b_p1);
	message[2] = b_p1_prime[0];

	//Need to send the first components of the 3 primed values to the previous processor which they will require

	MPI_Request pplus_request;
	if (rank != 0)
	{
		MPI_Isend(&message[0], 3, MPI_DOUBLE, rank - 1, rank, MPI_COMM_WORLD, &pplus_request);
	}

	//The steps are a bit out of order so that we can do one operation here
	//that does not depend on communication

	//Step 7
	//C_p2 is 0 for tridiagonal matrices if we assume q > 2
	//Confirming sent C_p1_prime before using
	double C_p2_prime = 0.0;
	if (rank != 0)
	{
		C_p2_prime = -A_p3 * C_p1_prime[q - 2];
	}

	double recv_message[3] = {0.0, 0.0, 0.0};
	if (rank != size - 1)
	{
		MPI_Status recv_status;
		MPI_Recv(&recv_message[0], 3, MPI_DOUBLE, rank + 1, rank + 1, MPI_COMM_WORLD, &recv_status);
	}

	//Step 5
	//The second term is a matrix multiplication
	//optimized to a single multiplication since A_p3 is all zero
	//except for the last element

	double A_p4_prime = A_p4 - A_p3 * A_p2_prime[q - 2];
	if (rank != size - 1)
	{
		double C_plus11_prime = recv_message[1];
		A_p4_prime = A_p4_prime - B_p1 * C_plus11_prime;
	}

	//Step 6
	//For a tridiagonal matrix B_p2 is 0 if q > 2B
	double B_p2_prime = 0.0;
	if (rank != size - 1)
	{
		double A_plus12_prime = recv_message[0];
		B_p2_prime = -B_p1 * A_plus12_prime;
	}

	//Step 8
	double b_p2_prime = b_p2 - A_p3 * b_p1_prime[q - 2];
	if (rank != size - 1)
	{
		double b_plus11_prime = recv_message[2];
		b_p2_prime = b_p2_prime - B_p1 * b_plus11_prime;
	}

	//Make sure to finish sending by now
	if (rank != 0)
	{
		MPI_Status send_status;
		MPI_Wait(&pplus_request, &send_status);
	}
	//Step 9 - Solve Reduced system
	//First substep is to share the reduced system and b vector
	//among all processors. Each processor
	//needs to share 4 doubles. The First and Last only need to share 3
	//but we will not optimize that.

	//This array contains both A, B, C and b entries interleaved
	//to minimize communication time.
	double *reduced_problem = (double *)malloc(sizeof(double) * size * 4);
	{
		double own_elements[4] = {A_p4_prime, B_p2_prime, C_p2_prime, b_p2_prime};
		MPI_Allgather(&own_elements[0], 4, MPI_DOUBLE, reduced_problem, 4, MPI_DOUBLE, MPI_COMM_WORLD);
	}

	double *reduced_prob_vector = (double *)malloc(sizeof(double) * size);

	//Form TriDiagonal problem
	TriDiagMat *reduced_tridiagonal = TriDiagMat_initialize(size);
	reduced_tridiagonal->D[0] = reduced_problem[0];
	reduced_prob_vector[0] = reduced_problem[3];
	for (int i = 1; i < size; i++)
	{
		reduced_tridiagonal->D[i] = reduced_problem[4 * i];
		reduced_tridiagonal->DU[i - 1] = reduced_problem[4 * (i - 1) + 1];
		reduced_tridiagonal->DL[i - 1] = reduced_problem[4 * i + 2];
		reduced_prob_vector[i] = reduced_problem[4 * i + 3];
	}
	free(reduced_problem);

	//Solve reduced diagonal problem
	double *reduced_solution = (double *)malloc(sizeof(double) * size);
	LU_factor_tridiagonal(reduced_tridiagonal);
	tridiagonal_matrix_solve(reduced_tridiagonal, reduced_solution, reduced_prob_vector);

	//Step 10 - Calculate solution vector
	double *x_p = (double *)malloc(sizeof(double) * (q));
	for (int i = 0; i < (q - 1); i++)
	{
		x_p[i] = b_p1_prime[i] - A_p2_prime[i] * reduced_solution[rank];
	}

	if (rank != 0)
	{
		for (int i = 0; i < (q - 1); i++)
		{
			x_p[i] = x_p[i] - C_p1_prime[i] * reduced_solution[rank - 1];
		}
	}

	x_p[q - 1] = reduced_solution[rank];

	free(A_p2_prime);
	free(C_p1_prime);
	free(b_p1_prime);
	TriDiagMat_destroy(reduced_tridiagonal);
	free(reduced_solution);
	free(reduced_prob_vector);
	return x_p;
}