function [value] = ModelFunction(x)
%UNTITLED Summary of this function goes here
%   Detailed explanation goes here
value =0.0;

if x >= 0.0 && x <= 1.0
    value = x^2;
elseif x >= 1.0 && x < 2.0
    value = -2*(x-1)^2 + 2*(x-1) + 1;    
elseif x >= 2.0 && x < 3.0
    value = (3-x)^2;
end
value = 0.5*value;
end

