clear
close all

tf = 5e-5;
ts = 1e-3;
tw = 1e-6;
n = 256;

p = [2 4 8 16 32 64];

I = p.^(1/2);
fixed_E = 1./(1 + (2.*sqrt(p).*(p.*ts+n.^2*tw))./(n^3*tf));
scaled_E = 1./(1 + (2.*sqrt(p).*(ts+n.^2*tw.*p.^(-1/3)))./(n^3*tf));

isoscaled_E = (1)./(1 + (2.*sqrt(p).*(p.*ts+n.^2*tw*I.^2))./(I.^3.*n^3*tf));

figure(1)
plot(p,fixed_E, p, scaled_E,':', p, isoscaled_E, '--')
lgd = legend('Efficiency - Fixed Workload', 'Efficiency - Scaled Workload', 'Efficiency - Isoefficiency Scaled');
lgd.Location = 'southwest';
xlabel('Number of Processors (p)')
ylabel('Efficiency')
