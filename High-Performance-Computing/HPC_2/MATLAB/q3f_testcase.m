%Some matrix tests for q3f partitioning
totn = 27;
p = 3;
q = totn/p;



center_diag = 2*ones(totn, 1);
off_diag = ones(totn-1,1);

A = diag(center_diag,0) + diag(off_diag, 1) + 0.5*diag(off_diag, -1);
b = (0:1:totn-1)';
x = A\b;

%A subblocks
A11 = A(1:(q-1),1:(q-1));
A12 = A(1:(q-1),q);
A13 = A(q,1:(q-1));
A14 = A(q,q);

%B subblocks
B11 = A(q,(q+1):(2*q-1));
B12 = A(q,(2*q):(3*q-3));

%C subblocks
C21 = A((q+1):(2*q-1),q);
C22 = A((2*q):(3*q-3),q);

b11 = b(1:(q-1));
b12 = b(q);

A12p = A11\A12;
C21p = A11\C21;
b11p = A11\b11;
A14p = A14 - A13*A12p - B11*C21p;
B12p = B12 - B11*A12p;

