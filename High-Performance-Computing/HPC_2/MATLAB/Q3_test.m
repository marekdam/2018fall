clear
close all

n = 510;
a = 0;
b = 12*pi;
h = (b-a)/(n);

x = linspace(a, b, n+1); %
t = (a + h/2 :h: b - h/2)'; %Midpoints

%Problem Vector models functions f = x^2
u_positions = [x(1); t(:,1); x(n + 1);];
%U = u_positions.^2;

U = sin(u_positions);

C = BuildQuadraticSpline(n, U);

test_points = linspace(a,b, n+1);
test_U = sin(test_points);%test_points.*test_points;

q = CalcSplineAt(test_points, x, C, a, h);

format long
error = max(abs(q-test_U))

figure(1)
plot(test_points,test_U, test_points, q)