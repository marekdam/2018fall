knots = [2.09665402e-02 1.09952285e-03 6.28994188e-05 3.79789634e-06 2.33322896e-07];
three = [2.09665402e-02 1.49109026e-03 1.68263922e-04 2.02714204e-05 2.49594656e-06];

order_knots = zeros(1,4);
order_three = zeros(1,4);

for i = 1:4
order_knots(i) = log(knots(i+1)/knots(i))/log(0.5);
order_three(i) = log(three(i+1)/three(i))/log(0.5);
end