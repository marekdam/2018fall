function [q] = CalcSplineAt(x, nodes, C, a, h)
%UNTITLED3 Summary of this function goes here
%   Detailed explanation goes here
q = x;
for i = 1:length(x)
   q(i) = 0.0;
   max_boundary = find(nodes >= x(i),1);
   for j = max(max_boundary - 1,1) : max_boundary + 1
       q(i) = q(i) + C(j)*ModelFunction((x(i)-a)/h - j + 3);
   end
end
end

