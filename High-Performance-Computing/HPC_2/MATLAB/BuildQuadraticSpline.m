function [C] = BuildQuadraticSpline(n, U)
%UNTITLED2 Summary of this function goes here
%   Detailed explanation goes here

nOnes = ones(1,n+2);

A = diag((6/8) * nOnes, 0) + diag((1/8) * nOnes(1:n+1), -1) + (1/8) * diag(nOnes(1:n+1), 1);
A(1,1) = 0.5;
A(1,2) = 0.5;
A(n+2, n+2) = 0.5;
A(n+2, n+1) = 0.5;

C = A\U;

end

