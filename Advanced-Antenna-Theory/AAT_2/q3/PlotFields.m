%Polar Plot of Radiation Intensity
%with Numeric Integration

theta = linspace(0,2*pi,100);

U_0 = ((sin(theta)./(cos(theta)-1)).^2).*(1-cos(2*pi*(cos(theta)-1)));
[max_U0, idx] = max(U_0);
max_theta = (theta(idx)/pi)*180;

temp_udB = 10*log10(U_0);
temp_udB = temp_udB - max(temp_udB);
temp_udB(temp_udB<-40) = -40;


figure(1)
figure(1)
polarplot(theta, temp_udB + 40)
title('Radiation Intensity Normalized to U_{max}')
ax = gca;
ax.ThetaZeroLocation = 'top';
ax.ThetaDir = 'clockwise';
thetatickformat('degrees')
rticks([10 20 30 40])
rticklabels({'-30 dB','-20 dB','-10 dB', '0 dB'})


ax = gca;
ax.ThetaZeroLocation = 'top';
ax.ThetaDir = 'clockwise';
thetatickformat('degrees')

u = linspace(-1, 0.9999999, 1000);
integrand = ((u+1)./(u-1)).*(cos(2*pi*u)-1);

numeric_integral = trapz(u,integrand);
