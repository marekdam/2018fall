%This script runs all necessary calculations to reproduce
%Figure 4.9 in Balanis for the case of finite radius dipole

%Run mesh routines and save in file.
%%%%Only need to do this once
%rwg1;
%rwg2;
%Get rid of all temp variables
clear;
close all;

impedance_fs = 119.9169832*pi;
c = 3e8;
electrical_length = linspace(0, 3.0, 100); % Length in wavelength
num_freqs = size(electrical_length, 2);
physical_length = 0.3;
radius = 0.5e-3; %The actual strip simulated by rwg code is 4*radius or 2e-3 meters

wavelength = physical_length./electrical_length;
frequency = c./wavelength;
omega = 2*pi.*frequency;
k_0 = 2*pi./wavelength;
k_0l = 2*pi.*electrical_length;

%This is the size of the vector returned by RWG code
rwg_result_length = 480;
I_numeric = zeros(num_freqs, rwg_result_length);
rwg_zcoords = zeros(1, rwg_result_length);

%Calculate Current Distribution Numerically
for i = 1:num_freqs
    %Keep the data encapsulated in these functions
    rwg3_func(frequency(i));
    rwg4_func();
    [rwg_zcoords(:), J_z] = rwg5_func();
    I_numeric(i,:) = J_z*2e-3; %Multiple current density by width of strip
end

%Numeric Calculation of D_0, R_in, R_rad
size_coords = 200;
z_coords = linspace(-physical_length/2, physical_length/2, size_coords);
theta_coords = linspace(0, pi, size_coords);

D_0_numeric = zeros(1, num_freqs);
R_rad_numeric = zeros(1, num_freqs);
R_in_numeric = zeros(1, num_freqs);

for i = 1:num_freqs
    I_0 = max(abs(I_numeric(i, :)));
    I_in = (I_numeric(i, rwg_result_length/2));
    
	space_factor = NumericSpaceFactor(k_0(i), theta_coords, rwg_zcoords, I_numeric(i,:));
    U = CalculateRadiationIntensity(k_0(i), space_factor, theta_coords);
    P_rad = 2*pi*trapz(theta_coords, U.*sin(theta_coords));
    D_0_numeric(i) = 4*pi*max(U)./(P_rad);
    R_rad_numeric(i) = 2*P_rad./(I_0*I_0);
    R_in_numeric(i) = (I_0/abs(I_in)).^2*R_rad_numeric(i);

end

%Figure 1 is reproduction of Figure 4.9 Balanis
figure(1)

yyaxis left
plot(electrical_length, R_rad_numeric, ':', electrical_length, R_in_numeric, '--')
xlabel('Dipole length (\lambda)')
ylabel('Resistance (\Omega)')
axis([0 max(electrical_length) 0 1000])

yyaxis right
plot(electrical_length, D_0_numeric, '-')
xlabel('Dipole length (\lambda)')
ylabel('D_0 (dimensionless)')
axis([0 max(electrical_length) 0 4])
grid on;
legend('R_{rad}', 'R_{in}', 'D_0', 'Location','northwest')
%%%%%%

%%%
%%%     D max, R in, R rad theoretical calculations
%%%
D_0 = zeros(1, num_freqs);
R_rad = zeros(1, num_freqs);
R_in = zeros(1, num_freqs);

for i = 1:num_freqs

Q_result = Q_Integral(k_0l(i),1000);
D = TheoreticalDirectivity(Q_result, theta_coords, k_0l(i));
D_0(i) = max(D);
R_rad(i) = impedance_fs.*Q_result./(2*pi);
R_in(i) = R_rad(i)./(sin(k_0l(i)./2).^2);

end



figure(2)
plot(electrical_length, R_rad_numeric, ':', electrical_length, R_rad, '--')
xlabel('Dipole length (\lambda)')
ylabel('Resistance (\Omega)')
axis([0 max(electrical_length) 0 1000])
grid on;
legend('Numerical R_{rad}', 'Theoretical R_{rad}','Location','northwest')

figure(3)
plot(electrical_length, R_in_numeric, ':', electrical_length, R_in, '--')
xlabel('Dipole length (\lambda)')
ylabel('Resistance (\Omega)')
axis([0 max(electrical_length) 0 1000])
grid on;
legend('Numerical R_{in}', 'Theoretical R_{in}','Location','northwest')

figure(4)
plot(electrical_length, D_0_numeric, ':', electrical_length, D_0, '--')
xlabel('Dipole length (\lambda)')
ylabel('D_0 (dimensionless)')
axis([0 max(electrical_length) 0 4])
grid on;
legend('Numerical D_{0}', 'Theoretical D_{0}','Location','northwest')
