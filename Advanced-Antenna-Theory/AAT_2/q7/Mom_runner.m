
%Run mesh routines and save in file.
%Only need to do this once
%rwg1;
%rwg2;
%Get rid of all temp variables
clear;

impedance_fs = 119.9169832*pi;
c = 3e8;
%electrical_length = 0.5;
%electrical_length = [ 1/50 0.25 0.5 0.75 1.0]; % Length in normalized wavelength
electrical_length = linspace(0, 3.0, 50);
num_freqs = size(electrical_length, 2);
physical_length = 0.3;
radius = 0.5e-3;
wavelength = physical_length./electrical_length;
frequency = c./wavelength;
omega = 2*pi.*frequency;
k_0 = 2*pi./wavelength;
k_0l = 2*pi.*electrical_length;


rwg_result_length = 480;
I_numeric = zeros(num_freqs, rwg_result_length);
rwg_zcoords = zeros(1, rwg_result_length);
%Calculate Current Distribution Numerically
for i = 1:num_freqs
    %Keep the data encapsulated in these functions
    rwg3_func(frequency(i));
    rwg4_func();
    [rwg_zcoords(:), J_z] = rwg5_func();
    I_numeric(i,:) = J_z*2e-3; %Multiple current density by width of strip
end

% figure(1);
% plot(rwg_zcoords, abs(I_numeric));
% xlabel('Dipole length (m)')
% ylabel('Current (A)')
% grid on;

labels = strings(num_freqs);
for i = 1:num_freqs
    labels(i) = [num2str(electrical_length(i)),' \lambda'];
end

legend(labels)

%Prepare results of theoretical calculation
%Current Calculation and Radiation Intensity Calculation
size_coords = 200;
z_coords = linspace(-physical_length/2, physical_length/2, size_coords);
theta_coords = linspace(0, pi, size_coords);

Udb_numeric = zeros(num_freqs, size_coords);
D_0_numeric = zeros(1, num_freqs);
R_rad_numeric = zeros(1, num_freqs);
R_in_numeric = zeros(1, num_freqs);

for i = 1:num_freqs
    I_0 = max(abs(I_numeric(i, :)));
    I_in = (I_numeric(i, rwg_result_length/2));
    
	space_factor = NumericSpaceFactor(k_0(i), theta_coords, rwg_zcoords, I_numeric(i,:));
    U = CalculateRadiationIntensity(k_0(i), space_factor, theta_coords);
    P_rad = 2*pi*trapz(theta_coords, U.*sin(theta_coords));
    D_0_numeric(i) = 4*pi*max(U)./(P_rad);
    R_rad_numeric(i) = 2*P_rad./(I_0*I_0);
    R_in_numeric(i) = (I_0/abs(I_in)).^2*R_rad_numeric(i);
    
    temp_udB = 10*log10(U);
    temp_udB = temp_udB - max(temp_udB);
    temp_udB(temp_udB<-40) = -40;
    Udb_numeric(i, :) = temp_udB;
end


% figure(2)
% polarplot(theta_coords, Udb_numeric + 40)
% title('Radiation Intensity Normalized to U_{max}')
% legend(labels)
% ax = gca;
% ax.ThetaZeroLocation = 'top';
% ax.ThetaDir = 'clockwise';
% thetatickformat('degrees')

figure(3)
yyaxis left
plot(electrical_length, R_rad_numeric, ':', electrical_length, R_in_numeric, '--')
xlabel('Dipole length (\lambda)')
ylabel('Resistance (\Omega)')
axis([0 max(electrical_length) 0 1000])

yyaxis right
plot(electrical_length, D_0_numeric, '-')
xlabel('Dipole length (\lambda)')
ylabel('D_0')
axis([0 max(electrical_length) 0 4])

legend('R_{rad}', 'R_{in}', 'D_0')


% I_theo = zeros(num_freqs, size_coords);
% U_theo = zeros(num_freqs, size_coords);
% U_theo_dB = U_theo;
% D_0 = zeros(1, num_freqs);
% R_rad = zeros(1, num_freqs);
% R_in = zeros(1, num_freqs);

%for i = 1:num_freqs
    
%%%%
%%%%     Current, Radiation intensity calculations
%%%%    
    
% temp_i = TheoreticalCurrent(z_coords, k_0(i), physical_length);
% temp_u = TheoreticalRadiationIntensity(theta_coords, k_0l(i), 1.0);
% temp_udB = 10*log10(temp_u);
% temp_udB = temp_udB - max(temp_udB);
% temp_udB(temp_udB<-40) = -40;
% I_theo(i,:) = temp_i(:);
% U_theo(i,:) = temp_u(:)/max(temp_u);
% U_theo_dB(i,:) = temp_udB(:);


%%%%
%%%%     D max, R in, R rad calculations
%%%%

% Q_result = Q_Integral(k_0l(i),1000);
% D = TheoreticalDirectivity(Q_result, theta_coords, k_0l(i));
% D_0(i) = max(D);
% R_rad(i) = impedance_fs.*Q_result./(2*pi);
% R_in(i) = R_rad(i)./(sin(k_0l(i)./2).^2);


%end



% figure(1)
% plot(z_coords, I_theo);
% title("Theoretical Current")
% lb1 = [num2str(electrical_length(1)),' \lambda'];
% lb2 = [num2str(electrical_length(2)),' \lambda'];
% lb3 = [num2str(electrical_length(3)),' \lambda'];
% lb4 = [num2str(electrical_length(4)),' \lambda'];
% lb5 = [num2str(electrical_length(5)),' \lambda'];
% legend(lb1,lb2,lb3,lb4,lb5)
% xlabel('Position (m)')
% ylabel('Current (A)')
% 
% figure(2)
% polarplot(theta_coords, U_theo_dB + 40)
% title('Radiation Intensity Normalized to U_{max}')
% legend(lb1,lb2,lb3,lb4,lb5)
% ax = gca;
% ax.ThetaZeroLocation = 'top';
% ax.ThetaDir = 'clockwise';
% thetatickformat('degrees')

% figure(3)
% yyaxis left
% plot(electrical_length, R_rad, ':', electrical_length, R_in, '--')
% xlabel('Dipole length (\lambda)')
% ylabel('Resistance (\Omega)')
% axis([0 max(electrical_length) 0 1000])
% 
% yyaxis right
% plot(electrical_length, D_0, '-')
% xlabel('Dipole length (\lambda)')
% ylabel('D_0')
% axis([0 max(electrical_length) 0 4])
% 
% legend('R_{rad}', 'R_{in}', 'D_0')
