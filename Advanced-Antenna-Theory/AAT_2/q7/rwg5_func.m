%RWG5 Visualizes the surface current magnitude 
%   Uses the mesh file from RWG2, mesh2.mat, and
%   the file containing surface current coefficients,
%   current.mat, from RWG4 as inputs.
%
%   Copyright 2002 AEMM. Revision 2002/03/05 
%   Chapter 2
%
%   Modified by Sean Victor Hum, December 2009.
%   Modified by Damian Marek 2018.

function [zcoords, J_z] = rwg5_func()

%load the data
load('mesh2');
load('current');

Index=find(t(4,:)<=1);
Triangles=length(Index);

% Work out current distribution
CurrentNorm = sqrt(sum(abs(J).^2));
Jmax=max(CurrentNorm);
MaxCurrent=strcat(num2str(Jmax),'[A/m]')
CurrentNorm1=CurrentNorm/max(CurrentNorm);
for m=1:Triangles
    N=t(1:3,m);
    X(1:3,m)=[p(1,N)]';
    Y(1:3,m)=[p(2,N)]';
    Z(1:3,m)=[p(3,N)]';      
end
C=repmat(CurrentNorm1,3,1);

% figure(1);
% h=fill3(X, Y, Z, C); %linear scale
% colormap gray;
% axis('equal');
% rotate3d
% ylim([-0.075 0.075])

%absJ=abs(J);

% Sort z-locations for plotting
[zz, idx] = sort(Center(3, :));
%Instead of plotting just return values and positions
J_z = J(3, idx);
zcoords = zz;

% figure(2);
% plot(zz, absJ(3, idx), zz, abs(J(1, idx)));
% xlabel('Dipole length, m')
% ylabel('Surface current density, A/m')
% grid on;
% legend('|J_z|', '|J_x|');
end