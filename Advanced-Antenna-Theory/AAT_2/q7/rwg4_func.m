%RWG4 Solves MoM equations for the antenna radiation problem
%   Uses the mesh file from RWG2, mesh2.mat, and
%   the impedance file from RWG3, impedance.mat,
%   as inputs.
%   
%   Also calculates the "voltage" vector V (the right-
%   hand side of moment equations)         
%                                           V(1:EdgesTotal)
%
%   The following parameters need to be specified:
%
%   The feed point position                 FeedPoint(1:3);
%   Number of feeding edges (one for the dipole; 
%   two for the monopole)                   srcidx(1:2);
%
%   Copyright 2002 AEMM. Revision 2002/03/14 
%   Chapter 4
%
%   Modified by Sean Victor Hum, December 2009.
%   Modified by Damian Marek, 2018.

function [] = rwg4_func()
%load the data
load('mesh2');
load('impedance');

for k = 1:length(Edge_)
    if (Edge_(:, k)' == [2 5] | Edge_(:, k)' == [5 2]) %DM-Set src location
        srcidx = k
    end;
end;

%Define the voltage vector
V = zeros(1, EdgesTotal);
V(srcidx)=1*EdgeLength(srcidx);    

%Solve system of MoM equations
tic;
I=Z\V.';
toc %elapsed time

Index=find(t(4,:)<=1);
Triangles=length(Index);

%Find the current density for every triangle
for k=1:Triangles
    i=[0 0 0]';
    for m=1:EdgesTotal
        IE=I(m)*EdgeLength(m);
        if(TrianglePlus(m)==k)
            i=i+IE*RHO_Plus(:,m)/(2*Area(TrianglePlus(m)));
        end
        if(TriangleMinus(m)==k)
            i=i+IE*RHO_Minus(:,m)/(2*Area(TriangleMinus(m)));
        end
    end
    J(1:3, k) = i(1:3);
end

% Calculate port quantities
GapCurrent  =sum(I(srcidx).*EdgeLength(srcidx)');
GapVoltage  =mean(V(srcidx)./EdgeLength(srcidx));
Impedance   =GapVoltage/GapCurrent
FeedPower   =1/2*real(GapCurrent*conj(GapVoltage))

FileName='current.mat'; 
save(FileName, 'f','omega','mu_','epsilon_','c_', 'eta_',...
    'I','V','J', 'srcidx');
end
%fname = 'shortdipole_sol.h5'
%h5create(fname, '/density0', size(J.'));
%h5create(fname, '/freq', size(f));
%h5disp(fname);
%h5write(fname, '/density0', J.');
%h5write(fname, '/freq', f);
    

   
