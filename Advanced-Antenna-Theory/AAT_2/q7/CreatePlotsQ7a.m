%This script runs all necessary calculations to reproduce
%Figure 4.8 in Balanis for the case of finite radius dipole

%Run mesh routines and save in file.
%Only need to do this once
rwg1;
rwg2;
%Get rid of all temp variables
clear;
close all;

impedance_fs = 119.9169832*pi;
c = 3e8;
electrical_length = [ 0.25 0.5 1.0 1.5 2.0 ]; % Length in normalized wavelength
num_freqs = size(electrical_length, 2);
physical_length = 0.3;
radius = 0.5e-3; %The actual strip simulated by rwg code is 4*radius or 2e-3 meters

wavelength = physical_length./electrical_length;
frequency = c./wavelength;
omega = 2*pi.*frequency;
k_0 = 2*pi./wavelength;
k_0l = 2*pi.*electrical_length;

%This is the size of the vector returned by RWG code
rwg_result_length = 480;
I_numeric = zeros(num_freqs, rwg_result_length);
rwg_zcoords = zeros(1, rwg_result_length);

%Calculate Current Distribution Numerically
for i = 1:num_freqs
    %Keep the data encapsulated in these functions
    rwg3_func(frequency(i));
    rwg4_func();
    [rwg_zcoords(:), J_z] = rwg5_func();
    I_numeric(i,:) = J_z*2e-3; %Multiple current density by width of strip
end

figure(1);
plot(rwg_zcoords, abs(I_numeric));
xlabel('Position Along Wire (m)')
ylabel('Current Magnitude |I| (A)')
grid on;

labels = strings(num_freqs,1);
for i = 1:num_freqs
    labels(i) = [num2str(electrical_length(i)),' \lambda'];
end

legend(labels)

%Prepare results of theoretical calculation
%Current Calculation and Radiation Intensity Calculation
size_coords = 200;
z_coords = linspace(-physical_length/2, physical_length/2, size_coords);

I_theo = zeros(num_freqs, size_coords);

for i = 1:num_freqs
    temp_i = TheoreticalCurrent(z_coords, k_0(i), physical_length);
    I_theo(i,:) = temp_i(:);
end


for i = 1:num_freqs
figure(i+1)
plot(z_coords, abs(I_theo(i, :))/max(abs(I_theo(i, :))), rwg_zcoords, abs(I_numeric(i,:))/max(abs(I_numeric(i,:))));
title(strcat({'Length ='}, {' '} , labels(i)))

legend('Theoretical Current','Numerical Current')
xlabel('Position Along Wire (m)')
ylabel('Normalized Current Magnitude |I|')
ylim([0 1.2])
grid on;
end


