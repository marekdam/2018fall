function [U] = CalculateRadiationIntensity(k_0, space_factor, theta_coords)
%Numerically Calculates the Radiation Intensity from spacefactor
%in the far field by W = (1/(2*n))*abs(E_theta).^2
impedance_fs =  119.9169832*pi;

coefficient = (impedance_fs*k_0/(4*pi)).^2;
theta_term = sin(theta_coords).^2.*(space_factor.*conj(space_factor));
U = coefficient.*theta_term./(2*impedance_fs);
end

