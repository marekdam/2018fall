%This script runs all necessary calculations to reproduce
%Figure 4.6 in Balanis for the case of finite radius dipole

%Run mesh routines and save in file.
%Only need to do this once
%rwg1;
%rwg2;
%Get rid of all temp variables
clear;
close all;

impedance_fs = 119.9169832*pi;
c = 3e8;
electrical_length = [ 1/50 0.25 0.5 0.75 1.0 ]; % Length in wavelength
num_freqs = size(electrical_length, 2);
physical_length = 0.3;
radius = 0.5e-3; %The actual strip simulated by rwg code is 4*radius or 2e-3 meters

wavelength = physical_length./electrical_length;
frequency = c./wavelength;
omega = 2*pi.*frequency;
k_0 = 2*pi./wavelength;
k_0l = 2*pi.*electrical_length;

%This is the size of the vector returned by RWG code
rwg_result_length = 480;
I_numeric = zeros(num_freqs, rwg_result_length);
rwg_zcoords = zeros(1, rwg_result_length);

%Calculate Current Distribution Numerically
for i = 1:num_freqs
    %Keep the data encapsulated in these functions
    rwg3_func(frequency(i));
    rwg4_func();
    [rwg_zcoords(:), J_z] = rwg5_func();
    I_numeric(i,:) = J_z*2e-3; %Multiple current density by width of strip
end


labels = strings(num_freqs,1);
for i = 1:num_freqs
    labels(i) = [num2str(electrical_length(i)),' \lambda'];
end

%Current Calculation and Radiation Intensity Calculation
size_coords = 200;
theta_coords = linspace(0, pi, size_coords);

Udb_numeric = zeros(num_freqs, size_coords);

for i = 1:num_freqs
    
	space_factor = NumericSpaceFactor(k_0(i), theta_coords, rwg_zcoords, I_numeric(i,:));
    U = CalculateRadiationIntensity(k_0(i), space_factor, theta_coords);
    
    temp_udB = 10*log10(U);
    temp_udB = temp_udB - max(temp_udB);
    temp_udB(temp_udB<-40) = -40;
    Udb_numeric(i, :) = temp_udB;
    
end

theta_mirrored = [flip(-1.0*theta_coords) theta_coords];
UdB_mirrored = [flip(Udb_numeric, 2) Udb_numeric];

figure(1)
polarplot(theta_mirrored, UdB_mirrored + 40)
title('Elevation Plane Amplitude Patterns')
legend(labels)
ax = gca;
ax.ThetaZeroLocation = 'top';
ax.ThetaDir = 'clockwise';
thetatickformat('degrees')
rticks([10 20 30 40])
rticklabels({'-30 dB','-20 dB','-10 dB', '0 dB'})

%Prepare results of theoretical calculation
%Current Calculation and Radiation Intensity Calculation
z_coords = linspace(-physical_length/2, physical_length/2, size_coords);

U_theo = zeros(num_freqs, 2*size_coords);
U_theo_dB = U_theo;

for i = 1:num_freqs
    
%%%
%%%     Radiation intensity calculations
%%%    
    temp_u = TheoreticalRadiationIntensity(theta_mirrored, k_0l(i), 1.0);
    temp_udB = 10*log10(temp_u);
    temp_udB = temp_udB - max(temp_udB);
    temp_udB(temp_udB<-40) = -40;
   
    U_theo(i,:) = temp_u(:)/max(temp_u);
    U_theo_dB(i,:) = temp_udB(:);
end



for i = 1:num_freqs
figure(i+1)
polarplot(theta_mirrored, UdB_mirrored(i,:) + 40,...
    theta_mirrored, U_theo_dB(i,:) + 40 )

title(strcat({'Length ='}...
    , {' '} , labels(i))');

legend('Numerical Pattern', 'Theoretical Pattern')
ax = gca;
ax.ThetaZeroLocation = 'top';
ax.ThetaDir = 'clockwise';
thetatickformat('degrees')
rticks([10 20 30 40])
rticklabels({'-30 dB','-20 dB','-10 dB', '0 dB'})
end


