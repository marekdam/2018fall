function [SpaceFactor] = NumericSpaceFactor(k_0, theta_coords, z1_coords, I_current)
%Calculates the space factor integral for far field of finite length
%dipoles

num_points = length(theta_coords);
SpaceFactor = zeros(1, num_points);

for i=1:num_points
    integrand = I_current.*exp(1i.*k_0.*z1_coords.*cos(theta_coords(i)));
    SpaceFactor(i) = trapz(z1_coords, integrand);
end

end

