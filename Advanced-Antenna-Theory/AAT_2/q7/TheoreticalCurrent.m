function [current] = TheoreticalCurrent(z_coords, k_0, length)
%Calculates the theoretical current of a finite length dipole
%assuming infinitesimal radius
N = size(z_coords, 2);
current = zeros(1, N);
for m = 1:N
    if z_coords(m)>=0
        current(m) = sin(k_0.*(length/2 - z_coords(m)));
    else
        current(m) = sin(k_0.*(length/2 + z_coords(m)));
    end
end
end

