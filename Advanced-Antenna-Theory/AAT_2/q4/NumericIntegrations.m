clear;
kl_term = (1.0)*pi;

theta = linspace(0.01, pi, 1000);

integrand = (cos(kl_term.*cos(theta))-cos(kl_term)).^2./sin(theta);

Q = trapz(theta, integrand);

N = 119.9169832*pi;

R_rad = N*Q/(2*pi);

R_in = R_rad/(sin(kl_term).^2);

Gamma = (R_in-50)/(R_in + 50);

VSWR = (1+abs(Gamma))/(1-abs(Gamma));