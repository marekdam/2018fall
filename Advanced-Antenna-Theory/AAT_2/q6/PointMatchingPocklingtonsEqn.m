function [z_points, I] = PointMatchingPocklingtonsEqn(freq, num_segments)
%Point-matching Pocklington's Eqn.

%Physical Characteristics
length = 300e-3;
radius = 0.5e-3;
omega = 2*pi*freq;
e_0 = 8.854187e-12;
light_speed = 3e8;
k_0 = omega ./ light_speed;

%Degrees of Freedom and Discretization
N = num_segments; %Number of Segments
dz = length/(N);
z_pos = linspace(-length/2, length/2, N+1); %Location of segment endpoints
z_points = z_pos(1:N)+dz/2; %Testing points are at midpoints of segment

%Excite antenna with delta gap Voltage of 1V
Voltage = 1.0;
E_i = zeros(N, 1);
E_i(floor(N/2)) = -1i*(omega*e_0)*(Voltage/dz);

Z = zeros(N, N);%Impedance Matrix

%Selfterm integration
%Taken from The Method of Moments in Electromagnetics Second Edition -
%Gibson, Eqn. (5.47)
Green_selfterms = log((sqrt(1+4*radius^2/dz^2)+1)/(sqrt(1 + 4*radius^2/dz^2)-1))-1i*k_0*dz; 
    
for n = 1:N
    z_src_max = z_pos(n + 1);
    z_src_min = z_pos(n);   
    num_points_integrate = 150;
    z_src = linspace(z_src_min, z_src_max, num_points_integrate);

    for m = 1:N
        z_obs = z_points(m);
        
        %Second term
        %Taken from The Method of Moments in Electromagnetics Second Edition -
        %Gibson, Eqn. (5.69)
        R_2 = sqrt((z_obs-z_src_max).^2+radius^2);
        R_1 = sqrt((z_obs-z_src_min).^2+radius^2);
        term2 = (z_obs - z_src_max)*((1+1i*k_0*R_2)/R_2^3)*exp(-1i*k_0*R_2);
        term1 = (z_obs - z_src_min)*((1+1i*k_0*R_1)/R_1^3)*exp(-1i*k_0*R_1);
        d2Gr_dz2 = term2 - term1;
        %Second term
        
        %First term
        if m ~= n
        R_0 = sqrt((z_obs-z_src).^2+radius.^2);
        Green = exp(-1i*k_0.*R_0)./R_0;
        Green_integrated = trapz(z_src, Green);
        else
        Green_integrated = Green_selfterms;               
        end
        %First term
        
        Z(m,n) = (k_0*k_0*Green_integrated + d2Gr_dz2)/(4*pi);        
    end
end

I = Z\E_i; %Solve matrix equation

end



