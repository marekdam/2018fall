function [D] = TheoreticalDirectivity(Q_result,theta, k_0l)
%Calculates the directivity of a finite length dipole

D = 2*(((cos(0.5*k_0l.*cos(theta))-cos(0.5*k_0l))./sin(theta)).^2)/Q_result;
end

