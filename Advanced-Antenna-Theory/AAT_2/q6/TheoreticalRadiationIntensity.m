function [U] = TheoreticalRadiationIntensity(theta, k_0l, I_0)
%Calculates the theoretical current of a finite length dipole
%assuming infinitesimal radius

impedance_fs = 119.9169832*pi;
factor = impedance_fs*I_0*conj(I_0)/(8*pi^2);
U = factor*((cos(0.5*k_0l.*cos(theta))-cos(0.5*k_0l))./sin(theta)).^2;

end

