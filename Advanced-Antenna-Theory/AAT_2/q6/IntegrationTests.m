%Integration tests of Green's Function Thin Wire Kernel
clear
%Physical Characteristics
length = 1.0;%300e-3;
radius = 0.5e-3;%10^-5;%0.005; %0.5e-3;
freq = 3e8;
omega = 2*pi*freq;
e_0 = 8.854187e-12;
light_speed = 3e8;% 299792458.0;
wavelength = light_speed ./ freq;
k_0 = omega ./ light_speed;

dz = 0.01;
x_obs = 0.0;
x_src_min = -dz/2;
x_src_max = dz/2;

x_src_v = linspace(x_src_min,x_src_max,101);

R = sqrt((x_obs-x_src_v).^2+radius.^2);
R_center = sqrt((x_obs).^2+radius.^2);

Green = k_0*k_0*(1/(4*pi))*exp(-1i*k_0.*R)./R;

Q = trapz(x_src_v, Green);
Q_bad = dz*k_0*k_0*(1/(4*pi))*exp(-1i*k_0.*R_center)/R_center;
Q_analytic = k_0*k_0*(1/(4*pi)*log((sqrt(1+(2*radius/dz)^2) + 1)/(sqrt(1+(2*radius/dz)^2) - 1))  ...
                - 1i*k_0*dz/(4*pi));
            
syms z z_1;

R_sym = sqrt((z-z_1)^2 + radius*radius);

Green_sym = exp(-1i*k_0*R_sym)/(4*pi*R_sym);
%G = exp(-1i*k_0s*R)/(4*pi*R);
d2G_dz2 = diff(Green_sym, z ,2);
d2G_dz2 = simplify(d2G_dz2);

integrand = k_0*k_0*Green_sym + d2G_dz2;
integrand_handle = matlabFunction(integrand);
result = integrand_handle(x_obs ,x_src_v);

G_reduced = (G/(R_sym^4))*((1+1i*k_0*R_sym)*(2*R_sym^2-3*radius^2) + (k_0*radius*R_sym)^2);
handle_Green_reduced = matlabFunction(G_reduced);

figure(1)
plot(x_src_v, real(result));