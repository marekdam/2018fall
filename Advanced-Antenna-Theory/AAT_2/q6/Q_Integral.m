function [Q] = Q_Integral(k_0l,N)
%Calculates the Q integral that shows up in many finite length
%dipole calculations

dlimit = 0.0001; %Have to avoid singularity
theta = linspace(dlimit, pi-dlimit, N);

integrand = ((cos(0.5*k_0l.*cos(theta))-cos(0.5*k_0l)).^2)./sin(theta);
Q = trapz(theta, integrand);
end

