clear;
close all;
num_t = 201;
theta = linspace(0, 180, num_t);

%Q2 Source
SF_Desired = zeros(1, num_t);

%Calculate desired SF
for idx = 1:num_t
    if theta(idx) >= 60.0 && theta(idx) < 120.0
        SF_Desired(idx) = 1.0;
    end
end

%Calculate synthesized array factor
kd = pi;
N = 11;
M = (N - 1)/2;
SF_Syn= zeros(1, num_t);

for idx = 1:num_t   
    theta_rad = pi*theta(idx)/180;
    phi = kd*cos(theta_rad);
    for m = -M : M
        am = sinc(m/2)/2;
        SF_Syn(1, idx) = SF_Syn(1, idx) + am*exp(1i*m*phi);
    end
end

figure(1)
plot(theta, SF_Desired, '--', theta, abs(SF_Syn))
title('Array Synthesis via Fourier Series Method')
legend('Desired Space Factor', 'Synthesized Array Factor')
xlabel('Elevation Angle \theta (degrees)')
ylabel('Absolute Value of Factor')
axis([0 180 -0.1 1.1])