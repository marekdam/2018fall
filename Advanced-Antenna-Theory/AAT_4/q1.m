clear;
close all;
%Q1 Source
num_t = 201;
theta = linspace(0, 180, num_t);

SF_Desired = zeros(1, num_t);

%Calculate desired SF
for i = 1:num_t
    
    if theta(i) >= 60.0 && theta(i) < 90.0
        SF_Desired(i) = theta(i)/30 - 2;
    elseif theta(i) >= 90.0 && theta(i) <= 120.0
        SF_Desired(i) = -theta(i)/30 +4;   
    end
end

%Calculate synthesized array factor
kd = pi;
N = 7;
M = 1;
cos_thetam = [-2/7 0 2/7];
bm = [0.44662 1 0.44662];
SF_Syn= zeros(1, num_t);
for i = 1:num_t   
    for m = -M : M
    SF_Syn(i) = SF_Syn(i) + bm(m+M+1)*sin((N*kd/2)*(cos(pi*theta(i)/180) - cos_thetam(m+M+1))) ...
        /(N*sin((kd/2)*(cos(pi*theta(i)/180) - cos_thetam(m+M+1))));
    
    end
end



figure(1)
plot(theta, SF_Desired, '--', theta, abs(SF_Syn))
title('Array Synthesis via Woodward-Lawson sampling method')
legend('Desired Space Factor', 'Synthesized Array Factor')
xlabel('Elevation Angle \theta (degrees)')
ylabel('Absolute Value of Factor')
axis([0 180 -0.1 1.1])