function [p, t] = loadgmsh(filename)
% LOADGMSH  Load Gmsh mesh file.
%   [P, T] = LOADGMSH(FILENAME) loads a mesh file created by Gmsh and
%   returns the node coordinates in P and node-sets forming triangle mesh
%   elements in T.

% Author: Sean Victor Hum
% Date: 2 December 2009
% Copyright (C) 2010 SVH

dat = dlmread(filename);
numnodes = dat(1);
p = dat(2:numnodes+1, :).';
numtris = dat(2+numnodes);
t = dat(3+numnodes:2+numnodes+numtris, :).';
t(4, :) = 1;
