%RWG5 Visualizes the surface current magnitude 
%   Uses the mesh file from RWG2, mesh2.mat, and
%   the file containing surface current coefficients,
%   current.mat, from RWG4 as inputs.
%
%   Copyright 2002 AEMM. Revision 2002/03/05 
%   Chapter 2
%
%   Modified by Sean Victor Hum, December 2009.
%   Modified by Damian Marek 2018.

function [y_coords, J_y] = rwg5_func()

%load the data
load('mesh2');
load('current');

Index=find(t(4,:)<=1);
Triangles=length(Index);

% Work out current distribution
CurrentNorm = sqrt(sum(abs(J).^2));
Jmax=max(CurrentNorm);
%MaxCurrent=strcat(num2str(Jmax),'[A/m]');
CurrentNorm1=CurrentNorm/max(CurrentNorm);
for m=1:Triangles
    N=t(1:3,m);
    X(1:3,m)=[p(1,N)]';
    Y(1:3,m)=[p(2,N)]';
    Z(1:3,m)=[p(3,N)]';      
end
C=repmat(CurrentNorm1,3,1);

%Place currents into correct vector corresponding
%to dipole
%Z positions of each dipole
z1 = 1.125000e-01;
z2 = 0.03749999999999999;
z3 = -0.03749999999999999;
z4 = -1.125000e-01;

Center_z1 = (Center(3, :) == z1);
Center_z2 = (Center(3, :) == z2);
Center_z3 = (Center(3, :) == z3);
Center_z4 = (Center(3, :) == z4);

[yy1, idx1] = sort(Center(2, Center_z1));
[yy2, idx2] = sort(Center(2, Center_z2));
[yy3, idx3] = sort(Center(2, Center_z3));
[yy4, idx4] = sort(Center(2, Center_z4));

J_y1 = J(2, Center_z1);
J_y2 = J(2, Center_z2);
J_y3 = J(2, Center_z3);
J_y4 = J(2, Center_z4);

y_coords = [yy1; yy2; yy3; yy4;];
J_y = [J_y1(idx1); J_y2(idx2); J_y3(idx3); J_y4(idx4);];

% Sort z-locations for plotting
%[zz, idx] = sort(Center(3, :));
%Instead of plotting just return values and positions
%J_z = J(3, idx);
%zcoords = zz;

% figure(2);
% plot(zz, absJ(3, idx), zz, abs(J(1, idx)));
% xlabel('Dipole length, m')
% ylabel('Surface current density, A/m')
% grid on;
% legend('|J_z|', '|J_x|');
end