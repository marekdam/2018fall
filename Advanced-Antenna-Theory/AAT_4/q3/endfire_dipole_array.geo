lc = 2.1e-3;
w = 2e-3;
l = 0.15;
d = 0.075;

i = 0.0;
For (1:4)
i = i + 1;
pos = i*d - 2.5*d;
Point(1 + (i-1)*6) = {-w/2, -l/2, pos, lc};
Point(2 + (i-1)*6) = {-w/2, 0, pos, lc};
Point(3 + (i-1)*6) = {-w/2, l/2, pos, lc};
Point(4 + (i-1)*6) = {w/2, -l/2, pos, lc};
Point(5 + (i-1)*6) = {w/2, 0, pos, lc};
Point(6 + (i-1)*6) = {w/2, l/2, pos, lc};
Line(1 + (i-1)*6) = {1 + (i-1)*6, 2 + (i-1)*6};
Line(2 + (i-1)*6) = {2 + (i-1)*6, 3 + (i-1)*6};
Line(3 + (i-1)*6) = {3 + (i-1)*6, 6 + (i-1)*6};
Line(4 + (i-1)*6) = {6 + (i-1)*6, 5 + (i-1)*6};
Line(5 + (i-1)*6) = {5 + (i-1)*6, 4 + (i-1)*6};
Line(6 + (i-1)*6) = {4 + (i-1)*6, 1 + (i-1)*6};
Line Loop(7 + (i-1)*6) = {1 + (i-1)*6, 2 + (i-1)*6, 3 + (i-1)*6, 4 + (i-1)*6, 5 + (i-1)*6, 6 + (i-1)*6};
Plane Surface(i) = {7 + (i-1)*6};
EndFor
