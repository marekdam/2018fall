function [Z]=       impmet( EdgesTotal,TrianglesTotal,...
                            EdgeLength,K,...
                            Center,Center_,...
                            TrianglePlus,TriangleMinus,...
                            RHO_P,RHO_M,...
                            RHO__Plus,RHO__Minus,...
                            FactorA,FactorFi);   
%IMPMET Standard impedance matrix (metal surface)
%
%	Returns the complex impedance matrix [EdgesTotal x EdgesTotal]
%	Uses 9 integration points for every triangle 
%   (barycentric subdivision)
%
%   The impedance matrix is calculated as a sum of the contributions
%   due to separate triangles (similar to the "face-pair" method). 
%   See Appendix B for a detailed algorithm.
% 
%   A 9-point quadrature is used for all integrals, including 
%   the self-coupling terms. The alternative source code with 
%   the analytical approximation of the self-coupling terms 
%   is given in Appendix B. The difference between two methods 
%   is not significant. 
%
%   Copyright 2002 AEMM. Revision 2002/03/12 
%   Chapter 2

%Memory allocation
Z   =zeros  (EdgesTotal,EdgesTotal)+j*zeros(EdgesTotal,EdgesTotal);

counter = 0;
% Loop over integration (field) triangles
for p=1:TrianglesTotal
    
    % The following variables identify the edges that have triangle p as 
    % their plus and minus triangles respectively; length = 0 to 3
    Plus     =find(TrianglePlus-p==0);              
    Minus    =find(TriangleMinus-p==0);
    
    % Evaluate free space Green's function for every subtri source point,
    % which is part of the Amn integrand.
    % Position vectors from field tri. center (p) to all subtri source
    % points.
    D=Center_-repmat(Center(:,p),[1 9 TrianglesTotal]); % [3 9 TrianglesTotal]     
    R=sqrt(sum(D.*D));                                  % [1 9 TrianglesTotal]
    g=exp(-K*R)./R;                                     % [1 9 TrianglesTotal]

    % Separate into Green's function produced by (+) and (-) triangles
    gP=g(:,:,TrianglePlus);                         % [1 9 EdgesTotal]
    gM=g(:,:,TriangleMinus);                        % [1 9 EdgesTotal]
    
    % Integrate Green's function as Rao (20) (minus proportionality
    % constant) and evaluate phi portion of (17)
    Fi=sum(gP)-sum(gM);                             % [1 1 EdgesTotal]
    
    % This is now the phi contribution to the impedance matrix.  
    % Reshape so that it can be added to nth row below to the impedance
    % matrix 
    ZF= FactorFi.*reshape(Fi,EdgesTotal,1);         % [EdgesTotal 1]

    % Vector potential contributions more involved since the contributions
    % from (+) and (-) triangles must be handled separately.  The Green's
    % function portion of the integrand in (19) has already been computed.
    % All that remains is to evaluate the dot products in the first part of
    % (17), with the basis function in (19) evaluated over the source
    % subtriangles.
    for k=1:length(Plus)
        n=Plus(k);
        % RP is the rho part of the basis function, and is evaluated over
        % all 9 subtriangles, for all possible field points
        % RHO_P/M are the rhos in (17) at all possible field points.  They
        % have already been repmat'd to be the same size in rwg3
        RP=repmat(RHO__Plus(:,:,n),[1 1 EdgesTotal]);   % [3 9 EdgesTotal]
        A=sum(gP.*sum(RP.*RHO_P))+sum(gM.*sum(RP.*RHO_M));
        Z1= FactorA.*reshape(A,EdgesTotal,1);
        %Z(:,n)=Z(:,n)+EdgeLength(n)*(Z1+ZF);
        Z(:,n)=Z(:,n)+EdgeLength(n)*(Z1+ZF);
        counter = counter+1;
        %keyboard
    end
    
    for k=1:length(Minus)
        n=Minus(k);
        RP=repmat(RHO__Minus(:,:,n),[1 1 EdgesTotal]);  % [3 9 EdgesTotal]
        A=sum(gP.*sum(RP.*RHO_P))+sum(gM.*sum(RP.*RHO_M));
        Z1= FactorA.*reshape(A,EdgesTotal,1);
        % ZF is subtracted this time because the div of the basis function
        % is negative in a (-) source triangle
        %Z(:,n)=Z(:,n)+EdgeLength(n)*(Z1-ZF); 
        Z(:,n)=Z(:,n)+EdgeLength(n)*(Z1-ZF); 
        counter = counter+1;
    end
end

% counter is the number of source triangles that have been evaluated.  It
% should sum to EdgesTotal * 2.

counter