%Creates plots and calculates values used to answer question 3
%Run mesh routines and save in file.
%Only need to do this once
rwg1;
rwg2;
%Get rid of all temp variables
clear;
close all;

impedance_fs = 119.9169832*pi;
c = 3e8;
physical_length = 0.3/4;
radius = 0.5e-3; %The actual strip simulated by rwg code is 4*radius or 2e-3 meters

wavelength = 0.3;
frequency = c./wavelength;
omega = 2*pi.*frequency;
k_0 = 2*pi./wavelength;
kd = physical_length*k_0;

%This is the size of the vector returned by RWG code
rwg_result_length = 1152;
Y_admittance = zeros(4,4);

rwg3_func(frequency);
for i = 1:4
    V_exc = zeros(1,4);
    V_exc(i) = 1.0;
%Calculate Current Distribution Numerically
    gap_currents = rwg4_func(V_exc);
    Y_admittance(:,i) = gap_currents;
    [rwg_ycoords, J_y] = rwg5_func();
    I_numeric = J_y*2e-3; %Multiple current density by width of strip
end

% for i = 1:4
% figure(i);
% plot(rwg_ycoords(i,:), real(I_numeric(i,:)));
% xlabel('Position Along Wire (m)')
% ylabel('Current Magnitude |I| (A)')
% grid on;
% end

clear I_numeric

hw = -(pi/2 + 2.92/4);
V_hansen_excite = [exp(0) exp(1*1i*hw) exp(2*1i*hw) exp(3*1i*hw)].';
I_via_admittance = Y_admittance*V_hansen_excite;

%Calculate Current Distribution from Hansen-Woodyard Excitation
rwg4_func(V_hansen_excite.');
[~, J_y] = rwg5_func();
I_hansen = J_y*2e-3; %Multiple current density by width of strip

num_t = 151;
theta = linspace(0, pi, num_t);

%Calculate theoretical array factors
phi = kd*cos(theta) + hw;
AF_theo = abs(sin(4*phi/2)./(4*sin(phi/2)));
AF_theo = AF_theo/max(AF_theo);
phi = kd*cos(theta);
AF_theo_uniform = abs(sin(4*phi/2)./(4*sin(phi/2)));
AF_theo_uniform = AF_theo_uniform/max(AF_theo_uniform);

%Calculate array factors from gap currents and currents along wires
AF_admittance = zeros(1, num_t);
AF_integrated = zeros(1, num_t);
for i = 1:4
AF_admittance = AF_admittance + I_via_admittance(i).*exp((1i*i)*kd*cos(theta));
AF_integrated = AF_integrated + ...
    trapz(rwg_ycoords(i,:), I_hansen(i,:).*exp(1i*k_0.*rwg_ycoords(i,:))).*exp((1i*i)*kd*cos(theta));
end

%Calculate Current Distribution from Uniform Excitation
V_uniform_excite = [1.0 1.0 1.0 1.0].';
rwg4_func(V_uniform_excite.');
[~, J_y] = rwg5_func();
I_uniform = J_y*2e-3; %Multiple current density by width of strip
AF_uniform = zeros(1, num_t);
for i = 1:4
AF_uniform = AF_uniform + ...
    trapz(rwg_ycoords(i,:), I_uniform(i,:).*exp(1i*k_0.*rwg_ycoords(i,:))).*exp((1i*i)*kd*cos(theta));
end

%Normalize values for plotting
AF_admittance = abs(AF_admittance)/max(abs(AF_admittance));
AF_integrated = abs(AF_integrated)/max(abs(AF_integrated));
AF_uniform = abs(AF_uniform)/max(abs(AF_uniform));

figure(1)
plot(180*theta/pi, AF_theo, '--', 180*theta/pi, AF_admittance)
legend('AF Theoretical', 'AF from Admittance')
xlabel('\theta (degrees)')
ylabel('Normalized Array Factor')

figure(2)
plot(180*theta/pi, AF_theo, '--', 180*theta/pi, AF_admittance, ':', 180*theta/pi, AF_integrated)
legend('AF Theoretical', 'AF from Admittance', 'H-plane Pattern')
xlabel('\theta (degrees)')
ylabel('Normalized Array Factor')

figure(3)
plot(180*theta/pi, abs(AF_theo).^2, '--', 180*theta/pi, abs(AF_admittance).^2, ':', 180*theta/pi, abs(AF_integrated).^2)
legend('AF Theoretical', 'AF from Admittance', 'H-plane Pattern')
xlabel('\theta (degrees)')
ylabel('Squared Normalized Array Factor')


%trapz(theta,((AF_theo_uniform.^2).*sin(theta)))/trapz(theta,((AF_theo.^2).*sin(theta)))
SuperDirectivity = trapz(theta,((AF_uniform.^2).*sin(theta)))/trapz(theta,((AF_integrated.^2).*sin(theta)));


%Used to compare currents and test if radiation powers are equal
% figure(5)
% plot(180*theta/pi, abs(AF_integrated).^2, 180*theta/pi, abs(AF_uniform).^2)
% legend('AF_integrated', 'AF_uniform')
% xlabel('\theta (degrees)')
% ylabel('Squared Normalized Array Factor')
% figure(6)
% plot(rwg_ycoords(1,:), abs(I_hansen(1,:)), rwg_ycoords(1,:), abs(I_uniform(1,:)))
% figure(7)
% plot(rwg_ycoords(2,:), abs(I_hansen(2,:)), rwg_ycoords(2,:), abs(I_uniform(2,:)))
% figure(8)
% plot(rwg_ycoords(3,:), abs(I_hansen(3,:)), rwg_ycoords(3,:), abs(I_uniform(3,:)))
% figure(9)
% plot(rwg_ycoords(4,:), abs(I_hansen(4,:)), rwg_ycoords(4,:), abs(I_uniform(4,:)))
