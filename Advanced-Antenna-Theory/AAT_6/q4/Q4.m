clear 
close all
e_r = 10.2;
f = 2e9;
h = 0.05*2.54*0.01;
c = 3e8;
lambda = c/f;
k0 = 2*pi/lambda;

W = c*sqrt(2/(e_r+1))/(2*f);
er_eff = (e_r+1)/2 + 0.5*(e_r-1)*(1+12*h/W)^-0.5;

delta_L = 0.412*h*(er_eff+0.3)*(W/h+0.264)/((er_eff-0.258)*(W/h+0.8));

L = c/(2*f*sqrt(er_eff))-2*delta_L;
L_e = c/(2*f*sqrt(er_eff));

nTheta = 101;
nPhi = 201;

theta = linspace(0.0001, pi, nTheta);
phi = linspace(-pi/2, pi/2, nPhi);
[theta_grid, phi_grid] = meshgrid(theta, phi);

X = (k0*h/2).*sin(theta_grid).*cos(phi_grid);
Z = (k0*W/2).*cos(theta_grid);

E_phi = sin(theta_grid).*(sinc(X/pi)).*(sinc(Z/pi)).*cos((k0*L_e/2).*sin(theta_grid).*sin(phi_grid));

U = abs(E_phi).^2;

U = U./max(U(:));
UdB = 10*log10(U);
UdB = UdB - max(UdB(:));
UdB(UdB<-40) = -40;

integrand = U.*sin(theta_grid);
P_rad = trapz(phi,trapz(theta, integrand, 2));
D = 4*pi/P_rad;

% E_phi_e_plane = (sin(k0*h/2.*cos(phi))./(k0*h/2.*cos(phi))).*cos((k0*L_e/2).*sin(phi));
% 
% U_e_plane = abs(E_phi_e_plane).^2;
% 
% U_e_plane = U_e_plane./max(U_e_plane);
% UdB_e_plane = 10*log10(U_e_plane);
% UdB_e_plane = UdB_e_plane - max(UdB_e_plane(:));
% UdB_e_plane(UdB_e_plane<-40) = -40;

figure(1)
polarplot(phi, UdB(:,51) + 40)
title('E Plane Pattern')
ax = gca;
ax.ThetaZeroLocation = 'top';
ax.ThetaDir = 'counterclockwise';
thetatickformat('degrees')
rticks([10 20 30 40])
rticklabels({'-30 dB','-20 dB','-10 dB', '0 dB'})

figure(2)
polarplot(theta, UdB(101,:) + 40)
title('H Plane Pattern')
ax = gca;
ax.ThetaZeroLocation = 'right';
ax.ThetaDir = 'counterclockwise';
thetatickformat('degrees')
rticks([10 20 30 40])
rticklabels({'-30 dB','-20 dB','-10 dB', '0 dB'})


