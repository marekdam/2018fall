clear
close all

epsilon_    =8.854e-012;
mu_         =1.257e-006;
impedance_fs = 119.9169832*pi;
c = 299792458.0;

fdr = 0.8;
d = 0.085;
focal = fdr*d;
freq = 35e9;
lambda = c/freq;
k0 = 2*pi/lambda;

theta_0 = atan(abs(0.5*fdr/(fdr.^2-1/16)));

term1 = (1-cos(theta_0)^4)/4 - 2*log(cos(theta_0/2)) - ((1-cos(theta_0))^3)/3 ...
-0.5*sin(theta_0)^2;

e_ap = 18*(term1^2*cot(theta_0/2)^2);


%Aperture approach
nTheta = 301;
nPhi = 301;

%Primed coordinates
theta = linspace(0, theta_0, nTheta);
phi = linspace(0, 2*pi, nPhi);
[theta_g, phi_g] = meshgrid(theta, phi);
r_prime = 2*focal./(1+cos(theta_g));
rho_A = r_prime.*sin(theta_g);

%From Eqn. 15-38
er_x = sin(phi_g).*cos(phi_g).*(1-cos(theta_g))./sqrt(1-sin(theta_g).^2.*sin(phi_g).^2);
er_y = (-sin(phi_g).^2.*cos(theta_g)-cos(phi_g).^2)./sqrt(1-sin(theta_g).^2.*sin(phi_g).^2);

%Feed pattern
%Input power is then 1
G_f = 18*cos(theta_g).^8;
C1 = (mu_/epsilon_)^(1/4)*sqrt(1/(2*pi));

%From Eqn. 15.39
E_ap_x = C1*sqrt(G_f).*exp(-1i*k0.*2*focal)./(r_prime).*er_x;
E_ap_y = C1*sqrt(G_f).*exp(-1i*k0.*2*focal)./(r_prime).*er_y;

%Calculate Cartesian coordinates
%on aperture plane wrt radius and angle
x_p = rho_A.*cos(phi_g);
y_p = rho_A.*sin(phi_g);

%The radiation pattern is symmetric about obs_phi
%so I did not need this grid but I already had it written
%The directivity integration uses this fact later.
nObsTheta = 251;
nObsPhi = 1;

obs_theta = linspace(pi/2, pi, nObsTheta);
obs_phi = linspace(pi/2, pi/2, nObsPhi);

[obs_theta_g, obs_phi_g] = meshgrid(obs_theta, obs_phi);

E_theta = zeros(nObsPhi, nObsTheta);
E_phi = E_theta;

%Calculating far field from field on aperture
for j = 1:nObsTheta
    for i=1:nObsPhi
        
        o_theta = obs_theta(j);
        o_phi = obs_phi(i); 
        
        exp_term = exp(1i*k0*(x_p.*sin(o_theta).*cos(o_phi) + y_p.*sin(o_theta).*sin(o_phi)));
        
        %integrands of Eqns. 15-41a and 15-41b
        e_theta_int = (-E_ap_x.*cos(o_phi)-E_ap_y.*sin(o_phi)).*exp_term.*rho_A;
        e_phi_int = (-E_ap_x.*sin(o_phi)+E_ap_y.*cos(o_phi)).*exp_term.*rho_A;
        
        E_theta(i,j) = trapz(phi,trapz(rho_A(1,:), e_theta_int, 2));
        E_phi(i,j) = trapz(phi,trapz(rho_A(1,:), e_phi_int, 2));
    end
end

E_theta = E_theta.*(1-cos(obs_theta_g))*k0/(4*pi);
E_phi = E_phi.*(1-cos(obs_theta_g))*k0/(4*pi);

U = (abs(E_theta).^2 + abs(E_phi).^2)/(2*impedance_fs);
%U = U./max(U(:));
UdB = 10*log10(U);
UdB = UdB - max(UdB(:));
UdB(UdB<-40) = -40;

integrand = U.*sin(obs_theta_g);
P_rad = 2*pi*trapz(obs_theta, integrand, 2);%No dependence on phi
Gain = 4*pi*max(U);

figure(1)
plot(180*obs_theta/pi, UdB(1,:))
xlabel('\theta (°)')
ylabel('Normalized Radiation Intensity (dB)')
title('Co-Polarized Pattern (E-Plane)')

