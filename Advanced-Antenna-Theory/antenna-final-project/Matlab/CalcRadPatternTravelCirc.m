function [U] = CalcRadPatternTravelCirc(theta_g, phi_g, k0, l, a)

epsilon_    =8.854e-012;
mu_         =1.257e-006;
impedance_fs = 119.9169832*pi;
c = 299792458.0;

K = ((1i)^-l).*mu_*a*exp(-1i*l.*phi_g)./(4);
kasin = k0*a*sin(theta_g);

J_lminus1 = besselj(l-1, kasin);
J_lplus1 = besselj(l+1, kasin);

A_theta = K.*cos(theta_g).*(J_lminus1 + J_lplus1);
A_phi = K.*(-1i*J_lminus1 + 1i*J_lplus1);

E_far_theta = -1i*A_theta;
E_far_phi = -1i*A_phi;

U = abs(abs(E_far_theta).^2+abs(E_far_phi).^2);

end

