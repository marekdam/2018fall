function [F] = CreateHarmonicAnimation(filename, x,y,Z)

v = VideoWriter([filename '.avi']);
v.Quality = 90;
open(v);

surf(x,y,real(Z));
axis tight manual
shading interp
view(0,90);
ax = gca;
ax.NextPlot = 'replaceChildren';

loops = 100;
F(loops) = struct('cdata',[],'colormap',[]);
for j = 1:loops
    tz = real(Z.*exp(1i*8*pi*j/loops));
    surf(x,y,tz);
    shading(gca,'interp');
    view(gca,0,90);
    %drawnow

    F(j) = getframe(gcf);
    writeVideo(v,F(j));
end

close(v);
end

