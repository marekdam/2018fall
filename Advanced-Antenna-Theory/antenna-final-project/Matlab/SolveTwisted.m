clear
close all

epsilon_    =8.854e-012;
mu_         =1.257e-006;
impedance_fs = 119.9169832*pi;
c = 299792458.0;

f = 10e9;
lambda = c/f;
omega = 2*pi*f;
k0=2*pi/lambda;

z_plane = 1e4*lambda;
% nRho = 201;
% rho = linspace(0,300*lambda,nRho);
% theta = asin(rho./(sqrt(rho.^2 + z_plane.^2)));
 
nT = 201;
theta = linspace(0,pi/3,nT);


nP = 201;
phi = linspace(0,2*pi,nP);

% [rho_g, ~] = meshgrid(rho, phi);
[theta_g, phi_g] = meshgrid(theta, phi);

rho_g = tan(theta_g).*z_plane;
r = sqrt(rho_g.^2 + z_plane.^2);

a = lambda;
l = 3;

K = ((1i)^-l).*mu_*a*exp(-1i*k0*r).*exp(-1i*l.*phi_g)./(4*r);

kasin = k0*a*sin(theta_g);

J_lminus1 = besselj(l-1, kasin);
J_lplus1 = besselj(l+1, kasin);

A_theta = K.*cos(theta_g).*(J_lminus1 + J_lplus1);
A_phi = K.*(-1i*J_lminus1 + 1i*J_lplus1);

phase_front = exp(-1i*k0*r).*exp(-1i*l.*phi_g);

E_far_theta = -1i*omega.*A_theta;
E_far_phi = -1i*omega.*A_phi;

max_field = max(abs([E_far_theta(:); E_far_phi(:)]));
n_E_far_theta = E_far_theta./max_field;
n_E_far_phi = E_far_phi./max_field;

% [x,y] = pol2cart(phi_g,rho_g); 
% figure(1)
% surf(x,y, angle(phase_front))
% xlabel('\rho')
% colorbar
% shading interp
% view(0,90)

ratio = E_far_phi./E_far_theta;

plot(rho_g(1,:), abs(ratio(1,:)))
plot(rho_g(1,:), angle(ratio(1,:)))

% nT = 201;
% theta_g = linspace(0,2*pi,nT);
% phi_g = 0;
% 
% a = 0.6*lambda;
% l = 0;
% 
% U = CalcRadPatternTravelCirc(theta_g, phi_g, k0, l, a);
% U_n1 = U./max(U(:));
% UdB1 = 10*log10(U_n1);
% UdB1 = UdB1 - max(UdB1(:));
% UdB1(UdB1<-40) = -40;
% 
% a = 0.6*lambda;
% l = 3;
% U = CalcRadPatternTravelCirc(theta_g, phi_g, k0, l, a);
% U_n2 = U./max(U(:));
% UdB2 = 10*log10(U_n2);
% UdB2 = UdB2 - max(UdB2(:));
% UdB2(UdB2<-40) = -40;
% 
% a = 2*lambda;
% l = 3;
% 
% U = CalcRadPatternTravelCirc(theta_g, phi_g, k0, l, a);
% U_n3 = U./max(U(:));
% UdB3 = 10*log10(U_n3);
% UdB3 = UdB3 - max(UdB3(:));
% UdB3(UdB3<-40) = -40;
% 
% a = 2*lambda;
% l = 12;
% 
% U = CalcRadPatternTravelCirc(theta_g, phi_g, k0, l, a);
% U_n4 = U./max(U(:));
% UdB4 = 10*log10(U_n4);
% UdB4 = UdB4 - max(UdB4(:));
% UdB4(UdB4<-40) = -40;
% 
% 
% 
% figure(3)
% subplot(2,2,1)
% polarplot(theta_g, UdB1 + 40)
% title('a = 0.6\lambda, l = 0')
% ax = gca;
% ax.ThetaZeroLocation = 'top';
% ax.ThetaDir = 'counterclockwise';
% thetatickformat('degrees')
% rticks([10 20 30 40])
% %rticklabels({'-30 dB','-20 dB','-10 dB', '0 dB'})
% subplot(2,2,2)
% polarplot(theta_g, UdB2 + 40)
% title('a = 0.6\lambda, l = 3')
% ax = gca;
% ax.ThetaZeroLocation = 'top';
% ax.ThetaDir = 'counterclockwise';
% thetatickformat('degrees')
% rticks([10 20 30 40])
% 
% subplot(2,2,3)
% polarplot(theta_g, UdB3 + 40)
% title('a = 2\lambda, l = 3')
% ax = gca;
% ax.ThetaZeroLocation = 'top';
% ax.ThetaDir = 'counterclockwise';
% thetatickformat('degrees')
% rticks([10 20 30 40])
% 
% subplot(2,2,4)
% polarplot(theta_g, UdB4 + 40)
% title('a = 2\lambda, l = 12')
% ax = gca;
% ax.ThetaZeroLocation = 'top';
% ax.ThetaDir = 'counterclockwise';
% thetatickformat('degrees')
% rticks([10 20 30 40])


% Z = phase_front;
% 
% [F] = CreateHarmonicAnimation('test',x,y,Z);
% 
% fig = figure;
% shading interp
% view(0,90)
%movie(fig,F,2)

