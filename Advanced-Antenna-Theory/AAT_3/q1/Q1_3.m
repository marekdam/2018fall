
theta = linspace(0, pi, 10000);
array_factor = 2*cos(3*pi*cos(theta));
field_proportional = sin(theta).*array_factor;

figure(1)
plot(180*theta/pi, array_factor, 180*theta/pi, field_proportional);
legend('array factor', 'Factor')