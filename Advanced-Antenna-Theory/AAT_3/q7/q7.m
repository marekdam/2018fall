%Polar Plot of Radiation Intensity
%with Numeric Integration
close all
clear

N = 6;
pid = pi/2;

theta = linspace(-pi,pi,200);
u = pid.*cos(theta);
AF_normalized = (10*cos(u) + 5*cos(3*u) + cos(5*u))/16;

figure(1)
polarplot(theta, AF_normalized)
title('Normalized Array Factor')
ax = gca;
ax.ThetaZeroLocation = 'right';
ax.ThetaDir = 'counterclockwise';
thetatickformat('degrees')
rticks([0.25 0.5 0.75 1.0])

E_squared = (cos((pi/4).*cos(theta)) - cos(pi/4)).^2./(sin(theta)).^2;
RadiationPattern = AF_normalized.*E_squared./max(E_squared);

figure(2)
polarplot(theta, RadiationPattern)
title('Normalized Radiation Pattern')
ax = gca;
ax.ThetaZeroLocation = 'right';
ax.ThetaDir = 'counterclockwise';
thetatickformat('degrees')
rticks([0.25 0.5 0.75 1.0])