%Polar Plot of Radiation Intensity
%with Numeric Integration
close all
clear

N = 6;
pid = 0.761924*pi;

theta = linspace(0,pi,500);
u = pid.*cos(theta);
AF_normalized = abs(15.963*cos(u)+10.916*cos(3*u)+4.721*cos(5*u));

figure(1)
plot(theta, AF_normalized/max(AF_normalized))
title('Normalized Array Factor')
%ax = gca;
%ax.ThetaZeroLocation = 'right';
%ax.ThetaDir = 'counterclockwise';
%thetatickformat('degrees')
%rticks([0.25 0.5 0.75 1.0])


