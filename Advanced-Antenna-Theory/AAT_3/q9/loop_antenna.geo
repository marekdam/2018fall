lc = 2.5e-3;
w = 2e-3;
r = 8.25e-2;

//Bottom Circle
Point(1) = {0, 0, -w/2, lc};
Point(2) = {0, -r, -w/2, lc};
Point(3) = {r, 0, -w/2, lc};
Point(4) = {0, r, -w/2, lc};
Point(5) = {-r, 0, -w/2, lc};

//Top Circle
Point(6) = {0, 0, w/2, lc};
Point(7) = {0, -r, w/2, lc};
Point(8) = {r, 0, w/2, lc};
Point(9) = {0, r, w/2, lc};
Point(10) = {-r, 0, w/2, lc};

Circle(1) = {2, 1, 3};
Circle(2) = {3, 1, 4};
Circle(3) = {4, 1, 5};
Circle(4) = {5, 1, 2};

Circle(5) = {7, 6, 8};
Circle(6) = {8, 6, 9};
Circle(7) = {9, 6, 10};
Circle(8) = {10, 6, 7};

Line(9) = {2, 7};
Line(10) = {3, 8};
Line(11) = {4, 9};
Line(12) = {5, 10};





//+
//Curve Loop(1) = {8, 5, -10, -1, -4, 9};
//+
//Surface(1) = {1};
//+
//Surface(1) = {1};
//+
Curve Loop(2) = {5, -10, -1, 9};
//+
Surface(1) = {2};
//+
Curve Loop(3) = {8, -9, -4, 12};
//+
Surface(2) = {3};
//+
Curve Loop(4) = {7, -12, -3, 11};
//+
Surface(3) = {4};
//+
Curve Loop(5) = {6, -11, -2, 10};
//+
Surface(4) = {5};
