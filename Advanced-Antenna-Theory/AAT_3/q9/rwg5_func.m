%RWG5 Visualizes the surface current magnitude 
%   Uses the mesh file from RWG2, mesh2.mat, and
%   the file containing surface current coefficients,
%   current.mat, from RWG4 as inputs.
%
%   Copyright 2002 AEMM. Revision 2002/03/05 
%   Chapter 2
%
%   Modified by Sean Victor Hum, December 2009.
%   Modified by Damian Marek 2018.

function [phi_coords, J_phi] = rwg5_func()

%load the data
load('mesh2');
load('current');

Index=find(t(4,:)<=1);
Triangles=length(Index);

% Work out current distribution
CurrentNorm = sqrt(sum(abs(J).^2));
Jmax=max(CurrentNorm);
MaxCurrent=strcat(num2str(Jmax),'[A/m]');
CurrentNorm1=CurrentNorm/max(CurrentNorm);
for m=1:Triangles
    N=t(1:3,m);
    X(1:3,m)=[p(1,N)]';
    Y(1:3,m)=[p(2,N)]';
    Z(1:3,m)=[p(3,N)]';      
end
C=repmat(CurrentNorm1,3,1);

% figure(1);
% h=fill3(X, Y, Z, C); %linear scale
% colormap gray;
% axis('equal');
% rotate3d
% ylim([-0.075 0.075])

%absJ=abs(J);

phi_coords_unsort = zeros(1, length(Center));
J_phi_unsort = zeros(1, length(Center));
for i=1:length(Center)
   x = Center(1,i);
   y = Center(2, i);
   r = sqrt(x.^2+y.^2);
   sin_phi = y./r;
   cos_phi = x./r;
   phi_coords_unsort(1,i) = atan2(y,x);
   J_phi_unsort(1,i) = -sin_phi*J(1, i) + cos_phi*J(2,i);
    
end

% Sort z-locations for plotting
%[zz, idx] = sort(Center(3, :));
%Instead of plotting just return values and positions
%J_z = J(3, idx);
%zcoords = zz;

%Sort by phi
[phi_coords, idx] = sort(phi_coords_unsort);
J_phi = J_phi_unsort(idx);

%Append one copy of the first element to close the loop
phi_coords(length(Center) + 1) = phi_coords(1)+2*pi;
J_phi(length(Center) + 1) = J_phi(1);

% figure(2);
% plot(zz, absJ(3, idx), zz, abs(J(1, idx)));
% xlabel('Dipole length, m')
% ylabel('Surface current density, A/m')
% grid on;
% legend('|J_z|', '|J_x|');
end