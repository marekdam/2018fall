%This script calculates the current distribution of a loop antenna

%Run mesh routines and save in file.
%Only need to do this once
%rwg1;
%rwg2;
%Get rid of all temp variables
clear;
close all;

impedance_fs = 119.9169832*pi;
c = 3e8;
electrical_length = [0.01 0.1 0.2 1.0]; % Length in normalized wavelength
num_freqs = size(electrical_length, 2);
radius_loop = 8.25e-2;
physical_length = 2*pi*radius_loop;
radius = 0.5e-3; %The actual strip simulated by rwg code is 4*radius or 2e-3 meters


wavelength = physical_length./electrical_length;
frequency = c./wavelength;
omega = 2*pi.*frequency;
k_0 = 2*pi./wavelength;

%This is the size of the vector returned by RWG code
rwg_result_length = 417;
Impedance = zeros(num_freqs, 1);
I_numeric = zeros(num_freqs, rwg_result_length);
rwg_phi_coords = zeros(1, rwg_result_length);
I_normalized = I_numeric;
%Calculate Current Distribution Numerically
for i = 1:num_freqs
    %Keep the data encapsulated in these functions
    rwg3_func(frequency(i));
    Impedance(i) = rwg4_func();
    [rwg_phi_coords(:), J_phi] = rwg5_func();
    I_numeric(i,:) = J_phi*2e-3; %Multiple current density by width of strip
    I_normalized(i,:) = I_numeric(i,:)/max(abs(I_numeric(i,:)));
end

labels = strings(num_freqs,1);
for i = 1:num_freqs
    labels(i) = ['C = ', num2str(electrical_length(i)),' \lambda'];
end

figure(1);
plot(rwg_phi_coords, abs(I_normalized));
xlabel('Position on Wire, \phi (radians)')
ylabel('Normalized Current Magnitude |I| (A)')
ylim([0.1 1.1])
grid on;
legend(labels,'Location','southeast')