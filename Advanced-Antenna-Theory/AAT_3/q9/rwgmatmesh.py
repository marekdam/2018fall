#!/usr/bin/env python

# rwgmatmesh.py
#
# Author: Sean Victor Hum
# Date: 2 December 2009
#
# Processes a .msh file from gmsh and save node and triangle indices in a 
# text file for loading into MATLAB.
#
# Copyright (C) 2010 SVH
#Modified by Damian Marek to work with Python 3.6.6
import sys

def readmshfile(mshfname):
    # Open mesh file
    try:
        mshfile = open(mshfname, 'r')
    except IOError:
        sys.stderr.write('rwgmesh: unable to open mesh file\n')
        sys.exit(1)

    # Parse mesh file header
    line = mshfile.readline().strip()
    assert (line == '$MeshFormat')
    field = mshfile.readline().split()
    #version = int(field[0])
    #assert(version == 2)
    filetype = int(field[1])
    assert(filetype == 0)
    datasize = int(field[2])
    assert(datasize == 8)
    line = mshfile.readline().strip()
    assert(line == '$EndMeshFormat')

    # Process section $Nodes / $NOD
    line = mshfile.readline().strip()
    assert(line == '$Nodes')
    numnodes = int(mshfile.readline())
    print ('Number of nodes:', numnodes)

    # Load nodes into memory
    node_idx, node_coords = [], []
    count = 0
    for node in range(numnodes):
        field = mshfile.readline().split()
        node_idx.append(int(field[0]))
        node_coords.append([float(field[1]), float(field[2]), float(field[3])])
        count += 1
    #node_coords = transpose(array(node_coords))
    assert(count == numnodes)

    line = mshfile.readline().strip()
    assert(line == '$EndNodes')
    
    # Process section $Elements / $ELM
    line = mshfile.readline().strip()
    assert(line == '$Elements')

    numelms = int(mshfile.readline())
    print ('Number of elements:', numelms)

    # Load elements into memory

    # Indices associated with element (unused)
    linidx, tridx = [], []        
    elmtype = []
    # Indices of nodes composing elements (based on node_idx read in above)
    linodes, trinodes = [], [] 
    numlines, numtris = 0, 0

    for element in range(numelms):
        field = mshfile.readline().split()
        elmnum = int(field[0])     # Element number
        numtags = int(field[2])    # Number of tags
        elt = int(field[1])        # Element type
        elmtype.append(elt)
        if elt == 1:        # Line element
            linidx.append(elmnum)
            linodes.append([int(field[2+numtags+1]), \
                                int(field[2+numtags+2])])
            numlines += 1
        elif elt == 2:      # Triangle element
            tridx.append(elmnum)
            trinodes.append([node_idx.index(int(field[2+numtags+1])), \
                                 node_idx.index(int(field[2+numtags+2])), \
                                 node_idx.index(int(field[2+numtags+3]))])
            numtris += 1
        elif elt == 15:               # Point element, not used
            pass
        else:
            sys.stderr.write('rwgmesh: non-triangular meshes not supported\n')
            sys.exit(1)

    print ('Number of lines:', numlines)
    print ('Number of triangles:', numtris)

    # Parse file for end of element block
    line = mshfile.readline().strip()
    assert(line == '$EndElements')

    mshfile.close()

    return numnodes, node_coords, numtris, trinodes

def main(mshfname, outfname):
    print ('Using mesh file', mshfname)

    # Determine nodes and elements from mesh file
    [numnodes, node_coords, numtris, trinodes] = readmshfile(mshfname)
   
    try:
        fid = open(outfname, 'w')
    except IOError:
        sys.stderr.write('rwgmatmesh: unable to open output file\n')
        sys.exit(1)

    # Write nodes to file
    fid.write('%d\n' % numnodes)
    for k in range(0, numnodes):
        fid.write('%e\t%e\t%e\n' % (node_coords[k][0], \
                                        node_coords[k][1], \
                                        node_coords[k][2]))
    # Write triangles to file; note +1 used for MATLAB indexing
    fid.write('%d\n' % numtris)
    for k in range(0, numtris):
        fid.write('%d\t%d\t%d\n' % (trinodes[k][0]+1, \
                                        trinodes[k][1]+1, \
                                        trinodes[k][2]+1))
    fid.close()
    print ('Mesh data written to', outfname)
    return

# CALL

# Check and parse arguments

if len(sys.argv) != 3:
    sys.stderr.write('rwgmesh: usage: rwgmesh <input file> <output file>\n')
    sys.exit(1)
else:
    main(sys.argv[1], sys.argv[2])
