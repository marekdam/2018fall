%This script calculates the input impedance of a loop antenna

%Run mesh routines and save in file.
%Only need to do this once
%rwg1;
%rwg2;
%Get rid of all temp variables
clear;
close all;

impedance_fs = 119.9169832*pi;
c = 3e8;

electrical_length = linspace(0.01, 2.0, 100);
num_freqs = size(electrical_length, 2);
radius_loop = 8.25e-2;
physical_length = 2*pi*radius_loop;
radius = 0.5e-3; %The actual strip simulated by rwg code is 4*radius or 2e-3 meters


wavelength = physical_length./electrical_length;
frequency = c./wavelength;
omega = 2*pi.*frequency;
k_0 = 2*pi./wavelength;

%This is the size of the vector returned by RWG code
rwg_result_length = 417;
Impedance = zeros(num_freqs, 1);
I_numeric = zeros(num_freqs, rwg_result_length);
rwg_phi_coords = zeros(1, rwg_result_length);

%Calculate Current Distribution Numerically
for i = 1:num_freqs
    %Keep the data encapsulated in these functions
    rwg3_func(frequency(i));
    Impedance(i) = rwg4_func();
    [rwg_phi_coords(:), J_phi] = rwg5_func();
    I_numeric(i,:) = J_phi*2e-3; %Multiple current density by width of strip
end

R_in_theoretical = 20*pi*pi*(electrical_length).^4;

u_0 = 4*pi*1e-7;
X_in_theoretical = 2*pi*frequency*u_0*radius_loop*(log(8*radius_loop/radius)-2);

figure(1)
plot(electrical_length, real(Impedance), '--')
xlabel('Circumference of loop (\lambda)')
ylabel('Resistance (\Omega)')
axis([0 max(electrical_length) 0 2000])
grid on;
legend('Numerical R_{in}','Location','northeast')

figure(2)
plot(electrical_length, imag(Impedance), '--')
xlabel('Circumference of loop (\lambda)')
ylabel('Reactance (\Omega)')
axis([0 max(electrical_length) -1000 600])
grid on;
legend('Numerical X_{in}','Location','northeast')

figure(3)
plot(electrical_length, real(Impedance), '--', electrical_length, R_in_theoretical, ':')
xlabel('Circumference of loop (\lambda)')
ylabel('Resistance (\Omega)')
axis([0 0.4 0 2])
grid on;
legend('Numerical R_{in}','Theoretical R_{in}','Location','northwest')

figure(4)
plot(electrical_length, imag(Impedance), '--', electrical_length, X_in_theoretical, ':')
xlabel('Circumference of loop (\lambda)')
ylabel('Reactance (\Omega)')
axis([0 0.4 -10 500])
grid on;
legend('Numerical X_{in}','Theoretical X_{in}','Location','northeast')
