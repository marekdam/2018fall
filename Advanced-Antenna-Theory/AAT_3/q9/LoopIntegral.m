function [SpaceFactor] = LoopIntegral(k_0a, theta_coords, phi1_coords, I_theta_current)
%Calculates the space factor integral for far field of loop current
%Integrates in spherical coordinates so dl' = a*dphi

num_points = length(theta_coords);
SpaceFactor = zeros(1, num_points);

for i=1:num_points
    
    integrand = I_theta_current.*cos(phi1_coords).*exp(1i.*k_0a.*cos(phi1_coords).*sin(theta_coords(i)));
    SpaceFactor(i) = trapz(phi1_coords, integrand);
end

end

