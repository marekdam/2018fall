%This script calculates the radiation pattern of a loop antenna and is
%used to find the circumference which gives the largest directivity in the 
%endfire configuration.
%Run mesh routines and save in file.
%Only need to do this once
%rwg1;
%rwg2;
%Get rid of all temp variables
clear;
close all;

impedance_fs = 119.9169832*pi;
c = 3e8;

electrical_length = [0.63 linspace(1.0, 2.0, 21)]; % Length in normalized wavelength
num_freqs = size(electrical_length, 2);
radius_loop = 8.25e-2;
physical_length = 2*pi*radius_loop;
radius = 0.5e-3; %The actual strip simulated by rwg code is 4*radius or 2e-3 meters


wavelength = physical_length./electrical_length;
frequency = c./wavelength;
omega = 2*pi.*frequency;
k_0 = 2*pi./wavelength;

%This is the size of the vector returned by RWG code
rwg_result_length = 417;
Impedance = zeros(num_freqs, 1);
I_numeric = zeros(num_freqs, rwg_result_length);
rwg_phi_coords = zeros(1, rwg_result_length);

%Calculate Current Distribution Numerically
for i = 1:num_freqs
    %Keep the data encapsulated in these functions
    rwg3_func(frequency(i));
    Impedance(i) = rwg4_func();
    [rwg_phi_coords(:), J_phi] = rwg5_func();
    I_numeric(i,:) = J_phi*2e-3; %Multiple current density by width of strip
end

num_coords_observe = 500;
theta_coords = linspace(0, pi, num_coords_observe);
U_rad = zeros(num_freqs, num_coords_observe);
U_rad_theo = U_rad;
U_dB_theo = U_rad;
Udb_numeric = U_rad;
D = zeros(num_freqs, 1);
%Calculate Current Distribution Numerically
for i = 1:num_freqs
	space_factor = LoopIntegral(k_0(i)*radius_loop, theta_coords, rwg_phi_coords, I_numeric(i,:));
    U_rad(i,:) = space_factor.*conj(space_factor);
    average_U_rad = 2*pi*trapz(theta_coords, U_rad(i,:).*sin(theta_coords));
    D(i) = 4*pi*U_rad(i,1)/average_U_rad; %Directivity along pi/2
    
    U_rad_theo(i,:) = besselj(1,k_0(i)*radius_loop*sin(theta_coords)).^2;
    
    temp_udB = 10*log10(U_rad(i,:));
    temp_udB = temp_udB - max(temp_udB);
    temp_udB(temp_udB<-40) = -40;
    Udb_numeric(i, :) = temp_udB;
    
    temp_udB = 10*log10(U_rad_theo(i,:));
    temp_udB = temp_udB - max(temp_udB);
    temp_udB(temp_udB<-40) = -40;
    U_dB_theo(i, :) = temp_udB;
    
end

theta_mirrored = [flip(-1.0*theta_coords) theta_coords];
UdB_mirrored = [flip(Udb_numeric, 2) Udb_numeric];
UdB_theo_mirrored = [flip(U_dB_theo, 2) U_dB_theo];

figure(1)
polarplot(theta_mirrored, UdB_mirrored(1,:) + 40, theta_mirrored, UdB_theo_mirrored(1,:) + 40)
title(['C = ', num2str(electrical_length(1)),' \lambda'])
legend('Numerical Pattern', 'Theoretical Pattern')
ax = gca;
ax.ThetaZeroLocation = 'top';
ax.ThetaDir = 'clockwise';
thetatickformat('degrees')
rticks([10 20 30 40])
rticklabels({'-30 dB','-20 dB','-10 dB', '0 dB'})

[~, idx] = max(D);

figure(2)
polarplot(theta_mirrored, UdB_mirrored(idx,:) + 40, theta_mirrored, UdB_theo_mirrored(idx,:) + 40)
title(['C = ', num2str(electrical_length(idx)),' \lambda'])
legend('Numerical Pattern', 'Theoretical Pattern')
ax = gca;
ax.ThetaZeroLocation = 'top';
ax.ThetaDir = 'clockwise';
thetatickformat('degrees')
rticks([10 20 30 40])
rticklabels({'-30 dB','-20 dB','-10 dB', '0 dB'})

figure(3)
plot(electrical_length, D)
title('Plot of Endfire Directivity')
ylabel('Directivity')
xlabel('Circumference of Antenna (\lambda)')