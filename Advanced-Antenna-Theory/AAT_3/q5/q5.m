%Polar Plot of Array Factor
%with Numeric Integration
close all
clear


N = 6;
kd = pi/2;
alpha = -2.0575;
theta = linspace(-pi,pi,1000);
psi = kd.*cos(theta)+alpha;
AF_normalized = abs(sin(N*psi/2)./sin(psi/2))/N;

figure(1)
polarplot(theta, AF_normalized)
title('Normalized Array Factor')
ax = gca;
ax.ThetaZeroLocation = 'right';
ax.ThetaDir = 'counterclockwise';
thetatickformat('degrees')
rticks([0.25 0.5 0.75 1.0])

%Numeric Integration
dthet = 1e-6;
theta = linspace(0 + dthet,pi-dthet,1000);
psi = kd.*cos(theta)+alpha;
AF_normalized = sin(N*psi/2)./(N*sin(psi/2));
u = AF_normalized.*AF_normalized;
integrand = u.*sin(theta);
u_0 = 0.5*trapz(theta,integrand);
D = u(1)/u_0; %Directivity along theta = 0