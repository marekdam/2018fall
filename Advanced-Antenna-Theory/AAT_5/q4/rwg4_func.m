%RWG4 Solves MoM equations for the antenna radiation problem
%   Uses the mesh file from RWG2, mesh2.mat, and
%   the impedance file from RWG3, impedance.mat,
%   as inputs.
%   
%   Also calculates the "voltage" vector V (the right-
%   hand side of moment equations)         
%                                           V(1:EdgesTotal)
%
%   The following parameters need to be specified:
%
%   The feed point position                 FeedPoint(1:3);
%   Number of feeding edges (one for the dipole; 
%   two for the monopole)                   srcidx(1:2);
%
%   Copyright 2002 AEMM. Revision 2002/03/14 
%   Chapter 4
%
%   Modified by Sean Victor Hum, December 2009.
%   Modified by Damian Marek, 2018.

function [gap_currents] = rwg4_func(V_excite)
Edge_ = [];
%load the data
load('mesh2');
load('impedance');

dipole_1 = [10 13];

for k = 1:length(Edge_)
    %Check dipole excitation positions
    if (isequal(Edge_(:, k)',dipole_1) || isequal(Edge_(:, k)',fliplr(dipole_1))) %DM-Set src location
        srcidx = k;
    end
end

%Define the voltage vector
V = zeros(1, EdgesTotal);
V(srcidx)=V_excite.*EdgeLength(srcidx);    

%Solve system of MoM equations
%tic;
I=Z\V.';
%toc; %elapsed time

Index=find(t(4,:)<=1);
Triangles=length(Index);

%Find the current density for every triangle
for k=1:Triangles
    i=[0 0 0]';
    for m=1:EdgesTotal
        IE=I(m)*EdgeLength(m);
        if(TrianglePlus(m)==k)
            i=i+IE*RHO_Plus(:,m)/(2*Area(TrianglePlus(m)));
        end
        if(TriangleMinus(m)==k)
            i=i+IE*RHO_Minus(:,m)/(2*Area(TriangleMinus(m)));
        end
    end
    J(1:3, k) = i(1:3);
end

% Calculate port quantities
gap_currents  = I(srcidx).*EdgeLength(srcidx).';
%GapVoltage  =mean(V(srcidx)'./EdgeLength(srcidx)',2);
%Impedance   =GapVoltage./GapCurrent
%FeedPower   =1/2*real(GapCurrent.*conj(GapVoltage))

FileName='current.mat'; 
save(FileName, 'f','omega','mu_','epsilon_','c_', 'eta_',...
    'I','V','J', 'srcidx');
end

    

   
