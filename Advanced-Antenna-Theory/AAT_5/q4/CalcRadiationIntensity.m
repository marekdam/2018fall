function [U] = CalcRadiationIntensity(x, y, z, obs_theta, obs_phi, tri_centers, J, tri_areas, k_0, omega)

epsilon_    =8.854e-012;
mu_         =1.257e-006;
impedance_fs = 119.9169832*pi;
c = 299792458.0;

%Calculate vector potential
Ax = zeros(size(x));
Ay = zeros(size(x));
Az = zeros(size(x));

for i=1:length(tri_centers)
        R = sqrt((x - tri_centers(1,i)).^2 + (y - tri_centers(2,i)).^2 + (z - tri_centers(3,i)).^2);
        Ax = Ax + tri_areas(i).*J(1,i).*exp(-1i*k_0*R)./R;
        Ay = Ay + tri_areas(i).*J(2,i).*exp(-1i*k_0*R)./R;
        Az = Az + tri_areas(i).*J(3,i).*exp(-1i*k_0*R)./R;
end

Ax = Ax.*mu_./(4*pi);
Ay = Ay.*mu_./(4*pi);
Az = Az.*mu_./(4*pi);

[A_theta, A_phi] = CartesianVectorToTransverseSpherical(Ax, Ay, Az, obs_theta, obs_phi);

E_theta = -1i*omega*A_theta;
E_phi = -1i*omega*A_phi;

U = (1/(2*impedance_fs)).*(abs(E_theta).^2+abs(E_phi).^2);
end

