function [Vtheta, Vphi] = CartesianVectorToTransverseSpherical(Vx, Vy, Vz, theta, phi)

Vtheta = cos(theta).*cos(phi).*Vx + cos(theta).*sin(phi).*Vy...
    -sin(theta).*Vz;

Vphi = -sin(phi).*Vx + cos(phi).*Vy;

end

