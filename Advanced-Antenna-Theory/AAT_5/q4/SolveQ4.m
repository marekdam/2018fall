%Creates plots and calculates values used to answer question 3
%Run mesh routines and save in file.
%Only need to do this once
rwg1;
rwg2;
%Get rid of all temp variables
clear;
close all;

epsilon_    =8.854e-012;
mu_         =1.257e-006;
impedance_fs = 119.9169832*pi;
c = 299792458.0;

frequency = 5.5e9;
wavelength = c/frequency;
omega = 2*pi.*frequency;
k_0 = 2*pi./wavelength;

rwg3_func(frequency);
V_exc = 1.0;
%Calculate Current Distribution Numerically
gap_currents = rwg4_func(V_exc);
[tri_centers, J, tri_areas] = rwg5_func();

num_obs = 501;
obs_phi = (pi/2)*ones(1, num_obs);
obs_theta = linspace(-pi, pi, num_obs);
r = 1e9; %observation position far away

[x, y, z] = SphericalToCartesian(r, obs_theta, obs_phi);

U = CalcRadiationIntensity(x, y, z, obs_theta, obs_phi,...
    tri_centers, J, tri_areas, k_0, omega);

U = U./max(U(:));
UdB = 10*log10(U);
UdB = UdB - max(UdB(:));
UdB(UdB<-40) = -40;

figure(1)
polarplot(obs_theta, UdB + 40)
title('E Plane Pattern')
ax = gca;
ax.ThetaZeroLocation = 'top';
ax.ThetaDir = 'clockwise';
thetatickformat('degrees')
rticks([10 20 30 40])
rticklabels({'-30 dB','-20 dB','-10 dB', '0 dB'})

num_obs = 501;
obs_phi = (0)*ones(1, num_obs);
obs_theta = linspace(-pi, pi, num_obs);
r = 1e9; %observation position far away

[x, y, z] = SphericalToCartesian(r, obs_theta, obs_phi);

U = CalcRadiationIntensity(x, y, z, obs_theta, obs_phi,...
    tri_centers, J, tri_areas, k_0, omega);

U = U./max(U(:));
UdB = 10*log10(U);
UdB = UdB - max(UdB(:));
UdB(UdB<-40) = -40;

figure(2)
polarplot(obs_theta, UdB + 40)
title('H Plane Pattern')
ax = gca;
ax.ThetaZeroLocation = 'top';
ax.ThetaDir = 'clockwise';
thetatickformat('degrees')
rticks([10 20 30 40])
rticklabels({'-30 dB','-20 dB','-10 dB', '0 dB'})

%Calculate total radiated power when max is normalized to 1
num_obs = 201;
obs_theta = linspace(0, pi, num_obs);
obs_phi = linspace(0, 2*pi, num_obs);
r = 1e9; %observation position far away

[theta_grid, phi_grid] = meshgrid(obs_theta, obs_phi);

[x, y, z] = SphericalToCartesian(r, theta_grid, phi_grid);

U = CalcRadiationIntensity(x, y, z, theta_grid, phi_grid,...
    tri_centers, J, tri_areas, k_0, omega);

U = U./max(U(:));
UdB = 10*log10(U);
UdB = UdB - max(UdB(:));
UdB(UdB<-40) = -40;

integrand = U.*sin(theta_grid);

power = trapz(obs_phi,trapz(obs_theta, integrand, 2));
Directivity = 4*pi/power;

% figure(3)
% polarplot(obs_theta, UdB(25,:) + 40)
% title('H Plane Pattern')
% ax = gca;
% ax.ThetaZeroLocation = 'top';
% ax.ThetaDir = 'clockwise';
% thetatickformat('degrees')
% rticks([10 20 30 40])
% rticklabels({'-30 dB','-20 dB','-10 dB', '0 dB'})