%RWG5 Visualizes the surface current magnitude 
%   Uses the mesh file from RWG2, mesh2.mat, and
%   the file containing surface current coefficients,
%   current.mat, from RWG4 as inputs.
%
%   Copyright 2002 AEMM. Revision 2002/03/05 
%   Chapter 2
%
%   Modified by Sean Victor Hum, December 2009.
%   Modified by Damian Marek 2018.

function [centers, center_J, areas] = rwg5_func()

%load the data
load('mesh2');
load('current');

Index=find(t(4,:)<=1);
Triangles=length(Index);

% Work out current distribution
CurrentNorm = sqrt(sum(abs(J).^2));
Jmax=max(CurrentNorm);
%MaxCurrent=strcat(num2str(Jmax),'[A/m]');
CurrentNorm1=CurrentNorm/max(CurrentNorm);
for m=1:Triangles
    N=t(1:3,m);
    X(1:3,m)=[p(1,N)]';
    Y(1:3,m)=[p(2,N)]';
    Z(1:3,m)=[p(3,N)]';      
end
C=repmat(CurrentNorm1,3,1);

centers = Center;
center_J = J;
areas = Area;
end