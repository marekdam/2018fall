function [x, y ,z] = SphericalToCartesian(radius, theta, phi)
x = radius.*sin(theta).*cos(phi);
y = radius.*sin(theta).*sin(phi);
z = radius.*cos(theta);
end

