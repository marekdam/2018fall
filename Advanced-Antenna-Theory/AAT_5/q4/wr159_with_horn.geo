// Feed dipole
lcp = 2.5e-3;
w = 2e-3;
Ld = 1.25e-2;

// WR-159 section
lc = 10e-3;
Wg = 1.59*25.4e-3;
Hg = 0.795*25.4e-3;
Dg = 67.425e-3;
Dh = 180e-3;
dp = 17e-3;

//Aperture
dA = 0.5;
b1 = 0.243776;

// Feed waveguide points and lines
Point(1) = {-Wg/2, -Hg/2, -Dh, lc};
Point(2) = {-Wg/2, Hg/2, -Dh, lc};
Point(3) = {Wg/2, Hg/2, -Dh, lc};
Point(4) = {Wg/2, -Hg/2, -Dh, lc};

Point(5) = {-Wg/2, -Hg/2, -Dh-Dg, lc};
Point(6) = {-Wg/2, Hg/2, -Dh-Dg, lc};
Point(7) = {Wg/2, Hg/2, -Dh-Dg, lc};
Point(8) = {Wg/2, -Hg/2, -Dh-Dg, lc};

Line(1) = {1, 2};
Line(2) = {2, 3};
Line(3) = {3, 4};
Line(4) = {4, 1};

Line(5) = {5, 6};
Line(6) = {6, 7};
Line(7) = {7, 8};
Line(8) = {8, 5};

Line(9) = {5, 1};
Line(10) = {6, 2};
Line(11) = {7, 3};
Line(12) = {8, 4};

// Probe points
Point(9) = {-w/2, -Ld/2, -Dh-Dg+dp, lcp};
Point(10) = {-w/2, -0, -Dh-Dg+dp, lcp};
Point(11) = {-w/2, Ld/2, -Dh-Dg+dp, lcp};
Point(12) = {w/2, Ld/2, -Dh-Dg+dp, lcp};
Point(13) = {w/2, 0, -Dh-Dg+dp, lcp};
Point(14) = {w/2, -Ld/2, -Dh-Dg+dp, lcp};

// Probe lines
Line(13) = {9, 10}; 
Line(14) = {10, 11};
Line(15) = {11, 12};
Line(16) = {12, 13};
Line(17) = {13, 14};
Line(18) = {14, 9};

//Aperture points
Point(15) = {-Wg/2, -b1/2, -Dh + dA, lc};
Point(16) = {-Wg/2, b1/2, -Dh + dA, lc};
Point(17) = {Wg/2, b1/2, -Dh + dA, lc};
Point(18) = {Wg/2, -b1/2, -Dh + dA, lc};

//Aperture lines
Line(19) = {15, 16};
Line(20) = {16, 17};
Line(21) = {17, 18};
Line(22) = {18, 15};

//Aperture to waveguide
Line(23) = {15, 1};
Line(24) = {16, 2};
Line(25) = {17, 3};
Line(26) = {18, 4};

//Loops: waveguide to Aperture
Line Loop(7) = {23, 1, -24, -19};
Line Loop(8) = {24, 2, -25, -20};
Line Loop(9) = {25, 3, -26, -21};
Line Loop(10) = {26, 4, -23, -22};

// Loops: feed waveguide
Line Loop(1) = {1, -10, -5, 9};
Line Loop(2) = {2, -11, -6, 10};
Line Loop(3) = {3, -12, -7, 11};
Line Loop(4) = {4, -9, -8, 12};
Line Loop(5) = {5, 6, 7, 8};

// Loop: probe
Line Loop(6) = {13, 14, 15, 16, 17, 18};

Plane Surface(1) = {1};
Plane Surface(2) = {2};
Plane Surface(3) = {3};
Plane Surface(4) = {4};
Plane Surface(5) = {5};
Plane Surface(6) = {6};
Plane Surface(7) = {7};
Plane Surface(8) = {8};
Plane Surface(9) = {9};
Plane Surface(10) = {10};

